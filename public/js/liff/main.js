function increaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  value = isNaN(value) ? 0 : value;
  value++;
  document.getElementById('number').value = value;
}

function decreaseValue() {
  var value = parseInt(document.getElementById('number').value, 10);
  // console.log('---1--- :: ' + value);
  value = isNaN(value) ? 0 : value;
  value--;
  // console.log('---2--- :: ' + value);
  value < 1 ? value = 1 : '';
  document.getElementById('number').value = value;
  
}

$(function(){
    $('.amount-option').click(function() {
        $(this).find('input[type="text"]').focus();
    });
});

/*Offcanvas*/
$(function(){
    var w = $(window).width();
    var h = $(window).height();

    $('.offcanvas-page .uk-offcanvas-bar').css('width', w + 'px');
    
    $(window).on("resize", function(){
        var w = $(window).width();
        var h = $(window).height();
        $("#offcanvas-invoice .uk-offcanvas-bar").css("width", w + "px");
        $("#offcanvas-invoice .uk-offcanvas-bar").css("height", h + "px"); 
    });
});

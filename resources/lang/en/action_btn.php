<?php

return [
    'add_btn'               => 'Add',
    'back_btn'              => 'Back',
    'cancel_btn'            => 'Cancel',
    'clear_btn'             => 'Clear',
    'close_btn'             => 'Close',
    'confirm_btn'           => 'Confirm',
    'delete_btn'            => 'Delete',
    'edit_btn'              => 'Edit',
    'no_btn'                => 'No',
    'ok_btn'                => 'OK',
    'product_share_buy_txt' => 'Buy now at',
    'save_btn'              => 'Save',
    'search_btn'            => 'Search',
    'yes_btn'               => 'Yes',
];

<?php

return [
    'pages'     => [
        'banner'            => [
            'detail'    => [
                'back_to_top_btn'   => 'Back to top',
                'share_title'       => 'SHARE',
            ],
        ],
        'blog'              => [
            'detail'    => [
                'back_to_top_btn'   => 'Back to top',
                'share_title'       => 'SHARE',
            ],
            'list'      => [
                'read_more_btn' => 'Read more',
            ],
        ],
        'cart'              => [
            'cart_success'  => [
                'default_text'          => 'You can check your order status from <a href=":href" title=":full_order_code" target="_blank">:full_order_code</a> menu.',
                'default_title'         => 'Thank you for your order',
                'default_transfer_text' => 'You can check your order status from <a href=":href" title=":full_order_code" target="_blank">:full_order_code</a> menu.<br>You can pay for your order by transferring money to the bank accounts listed below.',
                'etc'                   => [
                    'help_txt'  => 'กรุณาอย่าพึ่งปิด Browser ขณะนี้ระบบกำลังพาคุณไปยัง Payment gateway ที่เลือกไว้',
                    'title'     => 'ระบบกำลังพาคุณไปยัง Payment gateway เพื่อทำการชำระเงิน',
                ],
                'line'                  => [
                    'payment_error_2'   => 'Please try again or call us at +662 683 6512',
                ],
                'paypal'                => [
                    'help_txt'  => 'กรุณาอย่าพึ่งปิด Browser ขณะนี้ระบบกำลังพาคุณไปยัง Payment gateway ที่เลือกไว้',
                    'text'      => 'You can check your order status from <a href=":href" title=":full_order_code" target="_blank">:full_order_code</a> menu.',
                    'title'     => 'ระบบกำลังพาคุณไปยัง Payment gateway เพื่อทำการชำระเงิน',
                ],
                'transfer'              => [
                    'text'  => 'You can check your order status from <a href=":href" title=":full_order_code" target="_blank">:full_order_code</a> menu.<br>You can pay for your order by transferring money to the bank accounts listed below.',
                ],
            ],
            'step_1'        => [
                'add_new_address'   => 'Add new address',
                'delete_confirm'    => 'คุณต้องการลบที่อยู่สำหรับการจัดส่งนี้ไหม?',
                'submit_btn'        => 'Next',
                'title'             => '1. Delivery address',
            ],
            'step_2'        => [
                'delivery_fee_txt'      => 'Charges <strong>(:currency) :fee</strong>',
                'delivery_time_txt'     => 'Delivery within <strong>:delivery_time working days</strong>',
                'email_lbl'             => 'Email:',
                'phone_help_txt'        => 'Please verify your phone number, the delivery provider will contact you at this number.',
                'phone_title'           => 'Phone',
                'rest_of_the_world_txt' => 'Rest of the world',
                'shipping_title'        => 'Delivery address',
                'submit_btn'            => 'Next',
                'title'                 => '2. Select delivery method',
                'weight_limit_txt'      => 'Total weight less than: <strong>(Gram) :weight_limit</strong>',
            ],
            'step_3'        => [
                'submit_btn'    => 'Next',
                'title'         => '3. Select payment method',
            ],
            'step_4'        => [
                'available_point_txt'       => ':point points available',
                'delete_attachment_confirm' => 'คุณต้องการที่จะลบไฟล์แนบไหม?',
                'discount_phd'              => 'Enter your discount code',
                'discount_submit_btn'       => 'Use code',
                'discount_title'            => 'Discount code',
                'fineuploader'              => [
                    'file_size_error'   => '{file} ขนาดไฟล์ใหญ่เกินไป, ขนาดไฟล์สูงสุดควรจะไม่เกิน {sizeLimit}',
                ],
                'payment_method_title'      => 'Payment method',
                'point_discount_txt'        => 'Point discount <span id="realtime-point-discount-amount">:discount</span>',
                'point_minimum_txt'         => 'Minimum order value :minimum',
                'point_title'               => 'Loyalty points',
                'point_to_amt_txt'          => ':point_to_amt แต้ม = :amt_earn',
                'shipping_address_title'    => 'Delivery address',
                'shipping_method_title'     => 'Delivery method',
                'special_req_title'         => 'Your special request',
                'title'                     => '4. Review your order',
                'use_point_lbl'             => 'Points to redeem',
                'use_point_submit_btn'      => 'Use points',
            ],
        ],
        'category'          => [
            'index' => [
                'empty_category'    => 'ไม่มีสินค้าในหมวดนี้',
                'sorting'           => [
                    'default'       => 'เรียงตามลำดับ',
                    'high_to_low'   => 'ราคาสูงไปต่ำ',
                    'label'         => 'Sort by:',
                    'latest'        => 'การอัพเดตล่าสุด',
                    'low_to_high'   => 'ราคาต่ำไปสูง',
                ],
            ],
        ],
        'customer'          => [
            'address_setting'   => [
                'add_btn'               => 'Add new address',
                'delete_confirm'        => 'คุณต้องการลบที่อยู่สำหรับการจัดส่งนี้ไหม?',
                'set_default_btn'       => 'Set as default',
                'set_default_confirm'   => 'Would you like to set it as your default delivery address?',
                'tagline'               => 'Add, edit, or delete your address.',
                'title'                 => 'Manage your address book',
            ],
            'index'             => [
                'email_phd'     => 'Enter email address',
                'firstname_lbl' => 'First name',
                'firstname_phd' => 'Enter first name',
                'idcard_lbl'    => 'ID Card',
                'idcard_phd'    => 'กรอกหมายเลขบัตรประชาชน',
                'lang_lbl'      => 'Default Language',
                'lastname_lbl'  => 'Last name',
                'lastname_phd'  => 'Enter last name',
                'phone_phd'     => 'Enter phone number',
                'tagline'       => 'ตั้งค่าข้อมูลส่วนตัวที่ใช้ในการสั่งซื้อ และรายละเอียดต่าง ๆ',
                'title'         => 'Edit profile',
                'validate'      => [
                    'phone' => 'Please enter a valid phone number.',
                ],
            ],
            'loyalty_points'    => [
                'avaiable_point_txt'    => 'Current points',
                'date_th'               => 'Date',
                'description_th'        => 'Details',
                'get_point_value_txt'   => 'Worth',
                'history_title'         => 'Point collection and usage log',
                'loyalty_points_header' => 'Loyalty points',
                'order_th'              => 'Invoice',
                'point'                 => 'Points',
                'use_point_th'          => 'Points spent',
                'use_point_txt'         => 'Points spent',
            ],
            'order'             => [
                'range_help_txt'    => '<strong>:total</strong> orders placed in: <strong>:range</strong>',
                'range_lbl'         => 'Orders placed in:',
                'range_submit_btn'  => 'Go',
                'tagline'           => 'Check, edit, track, or cancel your orders.',
                'title'             => 'Check your order status',
            ],
            'order_view'        => [
                'action_lbl'                => 'You can:',
                'back_url'                  => 'กลับไปหน้าตรวจสอบสถานะการสั่งซื้อ',
                'cancel_order_tagline'      => 'in :day days.',
                'cancel_order_title'        => 'Cancel order',
                'customer_title'            => 'Customer',
                'order_item'                => [
                    'price_th'      => 'Unit price',
                    'product_th'    => 'Product',
                    'qty_th'        => 'Quantity',
                    'total_th'      => 'Total',
                ],
                'payment_method_title'      => 'Payment method',
                'shipping_address_title'    => 'Ship to',
                'shipping_method_title'     => 'Delivery method',
                'special_req_title'         => 'Special request',
                'tagline'                   => 'From: <a href=":href" title=":store_name" target="_blank">:store_name</a> at :date_created',
                'title'                     => 'Invoice :full_order_code - :status',
            ],
            'password_setting'  => [
                'cpassword_lbl'     => 'Confirm new password',
                'old_password_lbl'  => 'Current password',
                'password_lbl'      => 'New password',
                'submit_btn'        => 'Save changes',
                'tagline'           => 'Use the form below to change password for your BentoSquare account.',
                'title'             => 'Change password',
            ],
            'store_follow'      => [
                'follower_txt'  => 'Followers:',
                'tagline'       => 'เพื่อให้ลูกค้าสามารถเข้าถึงร้านค้าที่สนใจได้สะดวกมากขึ้น',
                'title'         => 'ร้านค้าที่ลูกค้าติดตาม',
            ],
            'wish_list'         => [
                'tagline'   => 'เพื่อให้ลูกค้าสามารถเข้าถึงสินค้าที่สนใจได้สะดวกมากขึ้น',
                'title'     => 'สินค้าที่ลูกค้าชื่นชอบ',
            ],
        ],
        'delivery'          => [
            'form'  => [
                'date_title'            => 'Delivered',
                'invoice_lbl'           => 'Invoice no.',
                'order_tracking_title'  => 'Delivery status for invoice',
                'scan_type_lbl'         => 'Scan type:',
                'shipping_ems_title'    => 'Search result',
                'shipping_info_title'   => 'Recipient information',
                'signature_lbl'         => 'Name:',
                'status_title'          => 'Status',
                'title'                 => 'ข้อมูลใบสั่งซื้อ',
            ],
        ],
        'home'              => [
            'index' => [
                'empty_category'    => 'ไม่มีสินค้าในหมวดนี้',
                'sorting'           => [
                    'default'       => 'เรียงตามลำดับ',
                    'high_to_low'   => 'ราคาสูงไปต่ำ',
                    'label'         => 'Sort by:',
                    'latest'        => 'การอัพเดตล่าสุด',
                    'low_to_high'   => 'ราคาต่ำไปสูง',
                ],
            ],
        ],
        'invoices'          => [
            'default'           => [
                'date_created_lbl'          => 'Order date:',
                'order_title'               => 'ข้อมูลใบสั่งซื้อ',
                'paid_lbl'                  => 'Grand total:',
                'payment_method_lbl'        => 'Payment method',
                'shipping_address_title'    => 'Ship to',
                'shipping_method_lbl'       => 'Delviery method',
                'title'                     => 'Invoice',
            ],
            'payment_process'   => [
                'gate_way_title'    => 'ระบบกำลังพาคุณไปยัง Payment gateway เพื่อทำการชำระเงิน',
                'text'              => 'กรุณาอย่าพึ่งปิด Browser ขณะนี้ระบบกำลังพาคุณไปยัง Payment gateway ที่เลือกไว้',
                'title'             => 'Payment method',
            ],
        ],
        'login'             => [
            'forgot_password'   => [
                'email_help_txt'    => 'Please enter your registered email address. We are going to send you a password reset email.',
                'submit_btn'        => 'Verify',
                'title'             => 'Forgot password',
            ],
            'form'              => [
                'email_btn'                 => 'Log in',
                'email_title'               => 'Use your email address to log in',
                'fb_btn'                    => 'Log in with Facebook',
                'fb_help_txt'               => 'We\'re not going to post on your timeline',
                'fb_title'                  => 'คุณสามารถเข้าสู่ระบบด้วย Facebook',
                'forgot_password_anchor'    => 'Forgot password',
                'or_txt'                    => 'or',
                'password_phd'              => 'Password',
                'register_anchor'           => 'Create account',
                'title'                     => 'Log in',
            ],
        ],
        'notify_payment'    => [
            'form'      => [
                'attachment_help_txt'   => 'The maximum size for images (.jpg, .png) is 4 MB.',
                'attachment_lbl'        => 'Attachment',
                'customer_lbl'          => 'Customer',
                'error'                 => [
                    'canvas'    => 'Oops, Something went wrong.',
                    'file_type' => 'Invalid file extension (allow .jpg, .png only).',
                ],
                'from_bank_lbl'         => 'From bank',
                'from_bank_phd'         => '-- Select your invoice numnber --',
                'full_order_code_lbl'   => 'Invoice no.',
                'grand_total_lbl'       => 'Grand total',
                'invoice_lbl'           => 'Invoice no.',
                'payment_method_lbl'    => 'Payment Method',
                'submit_btn'            => 'Notify payment',
                'title'                 => 'Ordering Information',
                'transfer_amount_lbl'   => 'Transferred amount',
                'transfer_amount_phd'   => 'How much money did you transfer?',
                'transfer_date_lbl'     => 'Transferred date',
                'transfer_time_lbl'     => 'Time',
                'transfer_title'        => 'Transferred Information',
                'transfer_to_lbl'       => 'To bank account',
                'transfer_to_phd'       => '-- Please select bank account --',
            ],
            'no_order'  => [
                'back_url'  => 'Back to home',
                'text'      => 'You do not have any order that needs payment notification with our store at the moment.',
            ],
        ],
        'product'           => [
            'customer_review'   => [
                'back_url'  => 'Back',
            ],
            'index'             => [
                'back_url'          => 'Back',
                'category_lbl'      => 'Category',
                'from_store_lbl'    => 'From',
            ],
            'review_order_list' => [
                'back_url'      => 'Back to product list',
                'order_lbl'     => 'Invoice no.',
                'order_title'   => 'ใบสั่งซื้อสินค้า (:full_order_code) มีสินค้าดังนี้',
                'title'         => 'ข้อมูลใบสั่งซื้อ',
            ],
        ],
        'rating'            => [
            'index' => [
                'from_store_txt'    => 'Order from: <a href=":href" title=":store_name">:store_name</a> Order date :date',
                'title'             => 'Store rating and product review',
            ],
        ],
        'register'          => [
            'form'  => [
                'cemail_lbl'    => 'Confirm email',
                'cpassword_lbl' => 'Confirm password',
                'firstname_lbl' => 'First name',
                'lastname_lbl'  => 'Last name',
                'password_lbl'  => 'Password',
                'submit_btn'    => 'Register',
                'title'         => 'Create your account',
            ],
        ],
        'search'            => [
            'not_found_txt'     => 'Your search ":term" did not match any documents.',
            'suggestion_title'  => 'Suggestions :',
            'suggestion_txt'    => <<<'TEXT'
<li>Use simpler search term</li>
	                            <li>Correct any typo</li>
TEXT
,
            'title'             => 'Search result',
        ],
        'store'             => [
            'order' => [
                'range_help_txt'    => '<strong>:total</strong> orders placed in <strong>:range</strong>',
                'range_lbl'         => 'Orders placed in:',
                'range_submit_btn'  => 'Search',
            ],
        ],
        'store_info'        => [
            'contactus'         => [
                'message_lbl'   => 'Comments',
                'message_phd'   => 'Comments...',
                'name_lbl'      => 'Name',
                'order_lbl'     => 'Invoice no.',
                'order_phd'     => 'ไม่ระบุใบสั่งซื้อ',
                'phone_phd'     => 'Enter phone number',
                'subject_lbl'   => 'Subject',
                'submit_btn'    => 'Send message',
            ],
            'customer_review'   => [
                'country_lbl'       => 'From:',
                'date_do_lbl'       => 'Review date:',
                'date_order_lbl'    => 'Order date:',
                'range_lbl'         => 'Customer feedback:',
                'rank_title'        => 'Rank Grade',
                'rank_txt'          => 'average from :person people',
                'title'             => 'Average score',
            ],
            'description'       => [
                'cancel_order'          => [
                    'text'  => 'Order will be cancelled in  :cancel_order days.',
                    'title' => 'Order cancellation policy',
                ],
                'payment_fee_lbl'       => 'Charges',
                'payment_method_title'  => 'Payment Method',
                'payment_minimum_lbl'   => 'Minimum payment accepted',
                'paypal'                => [
                    'name'  => 'PayPal',
                    'title' => 'Credit/debit card through PayPal',
                ],
                'promptpay_title'       => 'การโอนเงินผ่านพร้อมเพย์',
                'shipping'              => [
                    'delivery_time_txt'     => 'Delivery within :delivery_time working days',
                    'fee_txt'               => 'Charges (:currency) :shipping_fee',
                    'rest_of_the_world_txt' => 'Rest of the world',
                    'title'                 => 'Delivery fees',
                    'weight_limit_txt'      => 'Total weight less than (Gram) :weight_limit',
                ],
                'transfer_title'        => 'Bank/money transfer',
            ],
        ],
    ],
    'partials'  => [
        'blog'                      => [
            'all_blog_btn'  => 'All',
            'load_more_btn' => 'Load more',
            'title'         => 'Recommended',
        ],
        'blog_sidebar'              => [
            'all_category_txt'      => 'All articles',
            'category_title'        => 'Category',
            'store_banner_title'    => 'News',
        ],
        'breadcrumb'                => [
            'home_anchor'   => 'Home',
        ],
        'cart'                      => [
            'cart_content'  => [
                'cart_empty'    => 'Your shopping cart is empty.',
            ],
            'cart_jsrender' => [
                'cart_empty'        => 'Your shopping cart is empty.',
                'cart_title'        => 'Shopping cart',
                'exit_btn'          => 'OR CONTINUE SHOPPING',
                'submit_btn_full'   => 'PROCEED TO CHECKOUT',
                'submit_btn_short'  => 'PLACE ORDER',
            ],
            'cart_sidebar'  => [
                'cart_title'    => 'Shopping cart',
                'submit_btn'    => 'PLACE ORDER',
            ],
            'cart_summary'  => [
                'submit_btn'    => 'PLACE ORDER',
            ],
        ],
        'cart_step_menu'            => [
            'step_1_anchor' => 'Delivery address',
            'step_2_anchor' => 'Delivery method',
            'step_3_anchor' => 'Payment method',
            'step_4_anchor' => 'Place order',
        ],
        'customer_address_example'  => [
            'title' => 'Preview address',
        ],
        'customer_address_form'     => [
            'add_submit_btn'        => 'Add new address',
            'add_title'             => 'Add new delivery address',
            'address_1_other_phd'   => 'Street address, P.O. box, company name, c/o',
            'address_1_phd'         => 'Address line 1: Street address, P.O. box, company name, c/o',
            'address_2_other_phd'   => 'Apartment, suite, unit, building, floor, etc.',
            'address_2_phd'         => 'Address line 2: Apartment, suite, unit, building, floor, etc.',
            'address_lbl'           => 'Delivery address',
            'country_lbl'           => 'Country',
            'district_lbl'          => 'City',
            'edit_submit_btn'       => 'Edit address',
            'edit_title'            => 'Edit your address',
            'firstname_phd'         => 'Recipient first name',
            'lastname_phd'          => 'Recipient last nam',
            'name_lbl'              => 'Recipient name',
            'phone_help_txt'        => 'For product delivery only',
            'phone_lbl'             => 'Phone',
            'province_lbl'          => 'State/Province/Region',
            'required_address_txt'  => 'Please enter your delivery address',
            'search_address_lbl'    => 'Specify ZIP Code or Subdistrict',
            'set_default'           => 'Set as your default address',
            'zipcode_lbl'           => 'ZIP',
        ],
        'customer_header'           => [
            'favorite_product_txt'  => 'My wishlist',
            'follow_store_txt'      => 'Followed stores',
            'nav_item'              => [
                'address_setting'   => [
                    'address_setting_anchor'    => 'Manage address book',
                ],
                'favorite_product'  => [
                    'anchor'    => 'Your wishlist',
                ],
                'followed_store'    => [
                    'followed_store_anchor' => 'Followed stores',
                ],
                'order'             => [
                    'loyalty_point_anchor'  => 'Loyalty Points',
                    'order_anchor'          => 'Order detail',
                    'order_list_anchor'     => 'My orders',
                ],
                'profile'           => [
                    'info_anchor'       => 'Edit profile',
                    'password_anchor'   => 'Change password',
                    'profile_anchor'    => 'Profile',
                ],
            ],
            'order_count_txt'       => 'Numbers of orders',
        ],
        'customer_nav_header'       => [
            'dropdowm_menu' => [
                'edit_profile'      => 'Edit profile',
                'edit_shipping'     => 'Edit delivery address',
                'favorite_product'  => 'Wishlist',
                'followed_store'    => 'Followed stores',
                'logout'            => 'Log Out',
                'order_list'        => 'My orders',
            ],
        ],
        'default_title_bar'         => [
            'store_updated' => 'Latest Update :updated',
        ],
        'faq_card'                  => [
            'answer_lbl'    => 'Answer:',
            'question_lbl'  => 'Question:',
        ],
        'fineuploader_default'      => [
            'drop_area_txt'         => 'Drop files here',
            'drop_processing_txt'   => 'Processing dropped files...',
            'upload_btn'            => 'Attach a file',
            'upload_help_txt'       => '(ขนาดไฟล์ไม่เกิน: 10MB | รองรับไฟล์: .jpg, .jpeg, .png, .ai, .zip, .rar, .doc, .docx, .rtf, .csv, .xls, .xlsx, .ppt, .pptx, .pdf)',
        ],
        'home_product_menu'         => [
            'all_product'   => 'All products',
            'hot'           => 'Hot',
            'new'           => 'New',
            'recommend'     => 'Recommend',
            'sale'          => 'Sale',
        ],
        'invoices'                  => [
            'notify_payment'    => [
                'error' => [
                    'file_type' => 'Invalid file extension (allow .jpg, .png only).',
                    'load_fail' => 'Oops, Something went wrong.',
                ],
                'form'  => [
                    'from_bank_lbl'         => 'From bank',
                    'from_bank_phd'         => '-- Please select your bank --',
                    'image_help_txt'        => 'The maximum size for images (.jpg, .png) is 4 MB.',
                    'image_lbl'             => 'Attachment',
                    'submit_btn'            => 'Notify payment',
                    'title'                 => 'Transferred Information',
                    'transfer_amount_lbl'   => 'Transferred amount',
                    'transfer_amount_phd'   => 'How much money did you transfer?',
                    'transfer_date'         => 'Transferred date',
                    'transfer_time'         => 'Time',
                    'transfer_to_lbl'       => 'To bank account',
                    'transfer_to_phd'       => '-- Please select bank account --',
                ],
                'title' => 'Notify payment',
            ],
            'order_content'     => [
                'empty_item'    => 'ไม่มีสินค้าในใบสั่งซื้อ',
            ],
            'order_item'        => [
                'qty_txt'   => 'Qty:',
                'total_txt' => 'Total:',
            ],
            'order_sidebar'     => [
                'title' => 'Order Detail',
            ],
            'order_status_1'    => [
                'make_payment_btn'  => 'Make payment',
                'title'             => 'Payment method',
            ],
            'order_status_7'    => [
                'order_tracking'    => [
                    'date_title'            => 'Delivered',
                    'na_txt'                => 'N/A',
                    'shipping_ems_title'    => 'Search result',
                    'status_title'          => 'Status',
                    'title'                 => 'Check delivery status',
                ],
            ],
        ],
        'loader'                    => [
            'load_more' => 'Load more',
        ],
        'nav_footer'                => [
            'about_title'           => 'About us',
            'customer_review'       => 'Check customer feedback',
            'menu_title'            => 'Menu',
            'notify_payment'        => 'Notify payment',
            'store_about'           => 'Store Description',
            'store_address_title'   => 'Store\'s Address',
            'store_contact'         => [
                'fb'    => 'Facebook',
                'gplus' => 'Google Plus',
                'ig'    => 'Instagram',
                'line'  => 'LINE',
                'pin'   => 'Pinterest',
                'title' => 'Contact us',
                'tw'    => 'Twitter',
                'web'   => 'Website',
                'yt'    => 'Youtube',
            ],
            'store_description'     => 'How to buy',
        ],
        'nav_header'                => [
            'category_txt'      => 'Category',
            'order_dropdown'    => [
                'delivery'      => 'Check delivery status',
                'loyalty_point' => 'Loyalty points',
                'notify_payment'=> 'Notify payment',
                'store_order'   => 'My orders',
                'title'         => 'Order detail',
            ],
            'profile_dropdown'  => [
                'address_setting'   => 'Edit your address',
                'index'             => 'Edit profile',
                'logout'            => 'Log Out',
                'order'             => 'My orders',
                'store_follow'      => 'Followed stores',
                'wish_list'         => 'Your wishlist',
            ],
        ],
        'order_attachment'          => [
            'title' => 'Attachment',
        ],
        'order_card'                => [
            'buy_from'              => 'From',
            'date_created_lbl'      => 'Order on:',
            'grand_total_lbl'       => 'Grand total:',
            'order_code_lbl'        => 'Invoice no:',
            'review_btn'            => 'User Review',
            'shipping_fullname_lbl' => 'Recipient name:',
            'store_name_lbl'        => 'From store:',
        ],
        'order_history'             => [
            'title' => 'Invoice log',
        ],
        'order_history_log'         => [
            'order_status_2'    => [
                'transaction_lbl'   => 'Transaction ID:',
            ],
            'order_status_7'    => [
                'tracking_lbl'  => 'Tracking ID:',
            ],
        ],
        'order_view_action'         => [
            'contact_store_btn' => 'Contact us',
            'delivery_btn'      => 'Check delivery status',
            'make_payment_btn'  => 'Make payment',
            'notify_payment_btn'=> 'Notify payment',
            'print_invoice_btn' => 'Print invoice',
            'print_receipt_btn' => 'Print receipt',
            'review_btn'        => 'User Review',
        ],
        'product_add_to_cart_form'  => [
            'add_to_cart_btn'           => 'BUY NOW',
            'error'                     => [
                'fail_txt'          => 'ไม่สามารถทำการบันทึกได้ โปรดลองใหม่อีกครั้ง',
                'out_of_stock_txt'  => 'สินค้าไม่เพียงพอต่อการสั่งซื้อ กรุณาติดต่อร้านเพื่อสอบถามข้อมูลเพิ่มเติม',
                'title'             => 'Fail',
            ],
            'get_point_lbl'             => 'สินค้าแต้มสะสมที่ได้ครั้งนี้',
            'out_of_stock_modal'        => [
                'email_help_txt'    => 'Enter your email address to be notified when the item is available',
                'email_phd'         => 'Email address',
                'error_title'       => 'Fail',
                'error_txt'         => 'ไม่สามารถทำการบันทึกได้ โปรดลองใหม่อีกครั้ง',
                'submit_btn'        => 'Confirm email address',
                'success_title'     => 'Success',
                'success_txt'       => 'Thank you for your interest, we will keep you informed when the item is available.',
                'title'             => 'Enter your email address to be notified',
            ],
            'out_of_stock_notify_btn'   => 'NOTIFY ME',
            'price_lbl'                 => 'Price',
            'product_variant_lbl'       => 'Product options',
            'qty_lbl'                   => 'Quantity',
            'remain_col'                => 'Remaining items',
        ],
        'product_card'              => [
            'sold_out'  => 'Sold out',
        ],
        'product_customer_review'   => [
            'customer_review_all_btn'   => 'See all customer reviews',
            'title'                     => 'Customer review (:total reviews)',
            'write_review_btn'          => 'Write a review',
        ],
        'product_relate'            => [
            'title' => 'Related products',
        ],
        'product_review_card'       => [
            'placeholder'   => 'What do you like about this product?',
        ],
        'product_share'             => [
            'fb'            => 'Facebook',
            'gplus'         => 'Google Plus',
            'share_buy_txt' => 'Order at:',
            'share_lbl'     => 'Share:',
            'tw'            => 'Twitter',
        ],
        'product_status'            => [
            'collapse_btn'  => 'Product status',
            'point_th'      => 'Earn',
            'remain_th'     => 'Remain',
        ],
        'rating_product'            => [
            'date_created_lbl'  => 'Date:',
            'from_store_lbl'    => 'From:',
            'title'             => 'รีวิวสินค้า ใบสั่งซื้อ :full_order_code',
        ],
        'rating_store'              => [
            'done_title'    => 'Thank you for your comments',
            'done_txt'      => 'Your opinion will help the store to improve on their competitiveness.',
            'feeling_lbl'   => 'Please choose your feeling.',
            'service_lbl'   => 'How would you rate the store for each of the following criteria?',
            'title'         => 'How do you feel about this purchase?',
        ],
        'search_menu'               => [
            'blog'      => 'Blog',
            'faq'       => 'FAQ',
            'product'   => 'Products',
        ],
        'store_header_logo'         => [
            'follow_btn'    => 'Follow +',
            'following_btn' => 'Following',
        ],
        'store_header_menu'         => [
            'blog'                  => 'All articles',
            'contactus'             => 'Contact us',
            'faq'                   => 'FAQ',
            'follower_lbl'          => 'Followers:',
            'grade_lbl'             => 'Level:',
            'notify_payment'        => 'Notify payment',
            'store_about'           => 'Store information',
            'store_customer_review' => 'Check customer feedback',
            'store_description'     => 'How to buy',
            'store_dropdown'        => 'About us',
        ],
        'store_info_sidebar'        => [
            'created_lbl'           => 'เปิดร้านเมื่อ:',
            'store_contact_title'   => 'ข้อมูลติดต่อร้าน',
            'store_title'           => 'ข้อมูลร้าน',
            'visitor_lbl'           => 'จำนวนคนเข้าร้าน:',
        ],
        'write_review_title'        => [
            'title' => 'Your Reviews',
        ],
    ],
    'title'     => [
        'cart'      => [
            'index' => 'Shopping cart',
        ],
        'customer'  => [
            'add_address'       => 'Add new shipping address',
            'address_setting'   => 'Manage address book',
            'edit_address'      => 'Edit address',
            'index'             => 'Edit profile',
            'loyalty_points'    => [
                'index' => 'Loyalty points',
                'view'  => 'Loyalty points of :store_name',
            ],
            'order'             => [
                'index' => 'My orders',
            ],
        ],
        'invoices'  => [
            'index' => 'Order :full_order_code for :customer_name',
        ],
        'register'  => [
            'index' => 'Register',
        ],
    ],
];

<?php

return [
    'default_variant_name'  => 'Normal',
    'email_txt'             => 'Email',
    'gender_female_label'   => 'Female',
    'gender_male_label'     => 'Male',
    'gender_na_label'       => 'Unspecified',
    'na_txt'                => 'N/A',
    'person_txt'            => 'persons',
    'phone_no_txt'          => 'Phone number',
    'point_txt'             => 'points',
    'qty_txt'               => 'Qty',
    'sold_out_txt'          => 'Sold out',
    'status_txt'            => 'Status',
    'tel_txt'               => 'Tel',
    'website_txt'           => 'Website',
    'wholesale'             => 'Wholesale',
    'year_txt'              => 'Year (s)',
];

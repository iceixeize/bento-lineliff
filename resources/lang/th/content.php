<?php

return [
    'default_variant_name'  => 'ธรรมดา',
    'email_txt'             => 'อีเมล',
    'gender_female_label'   => 'หญิง',
    'gender_male_label'     => 'ชาย',
    'gender_na_label'       => 'ไม่ระบุ',
    'na_txt'                => 'N/A',
    'person_txt'            => 'คน',
    'phone_no_txt'          => 'เบอร์โทรศัพท์',
    'point_txt'             => 'แต้ม',
    'qty_txt'               => 'จำนวน',
    'sold_out_txt'          => 'Sold out',
    'status_txt'            => 'สถานะ',
    'tel_txt'               => 'โทร',
    'website_txt'           => 'เว็บไซต์',
    'wholesale'             => 'ขายส่ง',
    'year_txt'              => 'ปี',
];

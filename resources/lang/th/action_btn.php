<?php

return [
    'add_btn'               => 'เพิ่ม',
    'back_btn'              => 'กลับ',
    'cancel_btn'            => 'ยกเลิก',
    'clear_btn'             => 'ล้าง',
    'close_btn'             => 'ปิด',
    'confirm_btn'           => 'ยืนยัน',
    'delete_btn'            => 'ลบ',
    'edit_btn'              => 'แก้ไข',
    'no_btn'                => 'ไม่',
    'ok_btn'                => 'ตกลง',
    'product_share_buy_txt' => 'สั่งซื้อที่นี่',
    'save_btn'              => 'บันทึก',
    'search_btn'            => 'ค้นหา',
    'yes_btn'               => 'ใช่',
];

<!DOCTYPE html>
<html lang="en">
<head>
	@include('layouts.header')

	<title>
		@yield('title', 'Chat bot')
	</title>

	@yield('header')
</head>

<body class="{{ (isset($bodyClass) ? $bodyClass : '') }}">
	{{-- <div class="container"> --}}

		@yield('content')

		@include('layouts.footer')

	{{-- </div> --}}

	@yield('footer')

	@yield('script')

	@yield('liff')
</body>
</html>
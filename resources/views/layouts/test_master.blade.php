<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />  
  <link href="<?php echo mix('css/app.css'); ?>" rel="stylesheet" type="text/css"/>
  <script src="<?php echo mix('js/app.js'); ?>"></script>

  <title>CHAT BOT</title>

</head>

<style type="text/css">
@font-face {
  font-family: 'kanitregular';
  src: url({{ asset('font/kanit-regular-webfont.woff2') }} format('woff2'),
  url({{ asset('font/kanit-regular-webfont.woff') }} format('woff');
  font-weight: normal;
  font-style: normal;

}

body, html {
  font-family: kanitregular, Tahoma, 'sans-serif';
  height: 100%;
}

</style>

<body>

  <div class="container">
    <!-- Container element -->

    @yield('content')

  </div>



</body>

</html>
<!doctype html>

<html>

<head>

   @include('includes.head')

</head>

<body>

<div class="container">

   <header>

       @include('includes.navbar')

   </header>

   <div id="main" class="row">

           @yield('content')

   </div>

   <footer class="row mg-bt-10">

       @include('includes.footer')

   </footer>

</div>

</body>

</html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href={{ asset("icon/favicon.png", true) }}>
<!-- Bootstrap core CSS -->
<link href ="{{ asset("bootstrap4.3.1/css/bootstrap.min.css", true) }}" rel="stylesheet" type="text/css">
<link href ="{{ asset("css/liff/intlTelInput.css", true) }}" rel="stylesheet" type="text/css">
<link href ="{{ asset("css/liff/loading.css", true) }}" rel="stylesheet" type="text/css">
<link href="{{ asset("css/liff/loading-btn.css", true) }}" rel="stylesheet" type="text/css">
<link href="{{ asset("css/liff/uikit.min.css", true) }}" rel="stylesheet" type="text/css">
<link href="{{ asset("css/liff/styles.css", true) }}" rel="stylesheet" type="text/css">
<link href="{{ asset("css/liff/sweetalert2.min.css", true) }}" rel="stylesheet" type="text/css">

<meta name="csrf-token" content="{{ csrf_token() }}">


<div class="line t_basic b_basic bg-white uk-margin-bottom">

	<div class="uk-container uk-container-small" id="cart-item-container">
		{{-- @dd($cartAll) --}}
		@isset($cartAll['prepare']['data']['cart'])
		@forelse ($cartAll['prepare']['data']['cart'] as $index => $item)
		@if(is_array($item) && strlen($index) == 32)
		@include('partials.cart-item')
		@endif
		
		@empty
		<figure class="item -horizon uk-grid-small uk-child-width-expand uk-margin-remove-top" uk-grid>
			<div>
				<p></p>
			</div>
		</figure>
		@endforelse
		@endisset
	</div>
</div>
 <div class="uk-container uk-container-small">
            <h3 class="text -gray -size14 uk-text-bold mb_5">วิธีการชำระเงิน:</h3> 
        </div> 
        <div class="line t_basic b_basic bg-white uk-margin-bottom">
            <div class="uk-container uk-container-small">
                <ul class="uk-list uk-list-divider pt_15 pb_15">
                    @if(isset($paymentMethod))
                    @foreach($paymentMethod['payment_key'] as $pmIndex => $pmKey)
                    <li>
                        <label class="uk-display-block uk-text-bold controls-checkboxes">
                            <input class="uk-radio select-payment" name="select-payment" type="radio" data-store_id="{{ $storeId }}" data-cart_id="{{ $cartId }}" data-payment_key="{{ $pmKey }}" name="sekect-payment" @isset($cartAll['prepare']['data']['payment_method']) @if($cartAll['prepare']['data']['payment_method'] == $pmKey) checked @endif @endisset> {{ $paymentMethod['payment'][$pmKey] }}
                        </label>
                    </li>
                    @endforeach
                    @endisset
                </ul>
            </div>
        </div><!-- /line -->
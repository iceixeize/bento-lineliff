        <div class="uk-container uk-container-small">
            <h3 class="text -gray -size14 uk-text-bold mb_5">วิธีการจัดส่ง:</h3> 
        </div> 
        <div class="line t_basic b_basic bg-white uk-margin-bottom">
            <div class="uk-container uk-container-small">
                <ul class="uk-list uk-list-divider pt_15 pb_15 text -req">
                    @isset($shippingMethod)
                    {{-- @dd($shippingMethod) --}}
                    @foreach($shippingMethod as $spIndex => $spMethod)
                    {{-- @dd($spMethod['shipping_type']) --}}
                    <li>
                        <label class="uk-display-block uk-text-bold controls-checkboxes">
                            <input class="uk-radio select-shipping" type="radio" name="select-shipping" data-store_id="{{ $storeId }}" data-cart_id="{{ $cartId }}" id="select-shipping" class="select-shipping" data-shipping_key={{$spMethod['shipping_key']}} @isset($cartAll['prepare']['data']['shipping_method']) @if($cartAll['prepare']['data']['shipping_method'] == $spMethod) checked @endif @endisset> {{$spMethod['shipping_name']}}
                        </label>
                    </li>
                    @endforeach
                    @endisset
                </ul>
            </div>
        </div><!-- /line -->
<div class="line b_basic bg-white uk-margin-bottom" id="button-container">
            <div class="uk-container uk-container-small">  
            	<button class="uk-button btn btn-cta uk-width-1-1" id="btn-paid" type="submit" @if($validBtn == false) disabled @endif> สั่งซื้อสินค้า</button>

            	<a href="javascript:close_window();" class="uk-button uk-button-link uk-text-center uk-width-1-1 uk-padding-small uk-margin-small-top">หรือซื้อสินค้าเพิ่มเติม</a>

            	{{-- <button id="close" type="button" class="uk-button-success">ปิด</button> --}}
            	
            </div>
</div>
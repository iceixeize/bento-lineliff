<script type="text/x-jsrender" id="cartItemTmp">
    @{{if cartCal}}
    @{{for cartCal}}


    <figure data-rowid="@{{>rowid}}" data-itemcode="@{{>item_code}}" class="item -horizon uk-grid-small uk-child-width-expand uk-margin-remove-top" uk-grid>
        <div>
            <a href="javascript:void(0)" class="img-wrapper" title="@{{>name}}">
                <img src="@{{>image}}" class="img-responsive" alt="@{{>name}}">
            </a>
        </div>
        
        <figcaption class="item-info uk-width-3-4">
            <h3 itemprop="name"><a href="javascript:void(0);" class="title-link">@{{>name}}</a></h3>
            <div class="variant">
                @{{>pv_name}}
                @{{if sku != ''}} 
                - @{{>sku}} 
                @{{/if}}
                @{{if has_wholesale == true}}
                {{-- @php 
                {{ __('content.wholesale') }} 
                @endphp --}}
                
                {{-- @elseif($prepareCart[$item['rowid']]['has_member_price'])) --}}
                {{-- ({{$prepareCart[$item['rowid']]['customer_group_name']}}) --}}
                @{{/if}}
            </div>
            <div class="amount-option uk-margin-small-bottom">
                <label for="amount-option" class="text -size13">@lang('content.qty_txt')</label>
                <input type="number" class="uk-input uk-form-width-xsmall uk-form-small uk-text-center uk-margin-small-right uk-margin-small-left" autocomplete="off" name="qty_@{{>item_code}}" id="qty_@{{>item_code}}" data-old="@{{>qty}}" value="@{{>qty}}" min="1" max="9999" maxlength="4" 
                {{-- oninput="$(this).keyNotOverMaxLength(event);" onkeypress="$(this).keyAmount(event);"  --}}
                data-role="none">
                <a href="javascript:void(0);" class="update-item uk-button uk-button-link text -size13 btn-update-cart" data-itemcode="@{{>item_code}}" data-rowid="@{{>rowid}}" data-store_id="@{{:~root.storeId}}" data-cartid="@{{:~root.cartId}}" data-action="edit-cart" data-product-variant-id="@{{>product_variant_id}}"><i class="icon-pencil icons"></i> @lang('action_btn.edit_btn')</a>
            </div>
            <div class="list-price" uk-grid>
                <div class="uk-width-auto">
                    <div class="pricetag uk-margin-small-right uk-text-bold">
                        <span>฿ @{{>subtotal}}</span>
                    </div>
                </div>
                <div class="uk-width-expand uk-text-right">
                    <a href="javascript:void(0);" data-itemcode="@{{>item_code}}" data-rowid="@{{>rowid}}" data-cartid="@{{:~root.cartId}}" data-action="delete-cart" data-product-variant-id="@{{>product_variant_id}}" class="remove-item uk-button uk-button-link text -size13">X @lang('action_btn.delete_btn')</a>
                </div>
            </div>
        </figcaption>
    </figure>

    @{{/for}}
    @{{else}}
    <figure class="item -horizon uk-grid-small uk-child-width-expand uk-margin-remove-top" uk-grid>
        <div>
            <p></p>
        </div>
    </figure>
    @{{/if}}
</script>


<script type="text/x-jsrender" id="shippingMethodListItemTmp">

    @{{if shipping}}
    @{{for shipping}}
    <li>
        <label class="uk-display-block uk-text-bold controls-checkboxes">
            <input class="uk-radio select-shipping" type="radio" name="select-shipping" data-store_id="@{{:~root.storeId}}" data-cart_id="@{{:~root.cartId}}" id="select-shipping" class="select-shipping" data-shipping_key="@{{>id}}" @{{if id == ~root.shippingSelected}} checked @{{/if}}> @{{>name}}
        </label>
    </li>
    @{{/for}}
    @{{/if}}

</script>

<script type="text/x-jsrender" id="paymentMethodListItemTmp">
    @{{if payment}}
    @{{for payment}}
    <li>
        <label class="uk-display-block uk-text-bold controls-checkboxes">
            <input class="uk-radio select-payment" type="radio" data-store_id="@{{:~root.storeId}}" data-cart_id="@{{:~root.cartId}}" data-payment_key="@{{>id}}" name="select-payment" @{{if id == ~root.paymentSelected}} checked @{{/if}}> @{{>name}}
        </label>
    </li>

    @{{/for}}
    @{{/if}}
</script>

<script type="text/x-jsrender" id="buttonTmp">
<div class="line b_basic bg-white uk-margin-bottom">
            <div class="uk-container uk-container-small">  
                <button class="uk-button btn btn-cta uk-width-1-1" id="btn-paid" @{{if validBtn == false}} disabled @{{/if}}> สั่งซื้อสินค้า</button>

</div>

</script>
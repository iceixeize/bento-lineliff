
<figure data-rowid="{{ $item['rowid'] }}" data-itemcode="{{ $item['item_code'] }}" class="item -horizon uk-grid-small uk-child-width-expand uk-margin-remove-top" uk-grid>
    <div>
        <a href="javascript:void(0)" class="img-wrapper" title="{{$item['name']}}">
            <img src="{{ $item['image'] }}" class="img-responsive" alt="{{$item['name']}}">
        </a>
    </div>
    <figcaption class="item-info uk-width-3-4">
        <h3 itemprop="name"><a href="javascript:void(0);" class="title-link">{{$item['name']}}</a></h3>
        <div class="variant">
            {{ $item['pv_name'] }}
            {{ (((isset($item['sku'])) && $item['sku'] != '') ? (' - ' . $item['sku']) : '') }}
            @if($item['has_wholesale'])
            ({{ __('content.wholesale') }})
            {{-- @elseif($prepareCart[$item['rowid']]['has_member_price'])) --}}
            {{-- ({{$prepareCart[$item['rowid']]['customer_group_name']}}) --}}
            @endif
        </div>
        <div class="amount-option uk-margin-small-bottom">
            <label for="amount-option" class="text -size13">@lang('content.qty_txt')</label>
            <input type="number" class="uk-input uk-form-width-xsmall uk-form-small uk-text-center uk-margin-small-right uk-margin-small-left" autocomplete="off" name="qty_{{ $item['rowid'] }}" id="qty_{{ $item['item_code'] }}" data-old="{{$item['qty']}}" value="{{$item['qty']}}" min="1" max="9999" maxlength="4" 
            {{-- oninput="$(this).keyNotOverMaxLength(event);" onkeypress="$(this).keyAmount(event);"  --}}
            data-role="none">
            <a href="javascript:void(0);" class="update-item uk-button uk-button-link text -size13 btn-update-cart" data-itemcode="{{ $item['item_code'] }}" data-rowid="{{ $item['rowid'] }}" data-store_id="{{ $storeId }}" data-cartid="{{ $cartId }}" data-action="edit-cart" data-product-variant-id="{{ $item['product_variant_id'] }}"><i class="icon-pencil icons"></i> @lang('action_btn.edit_btn')</a>

            @php
                $productId = $item['product_id'];
            @endphp
        </div><!-- /amount-option -->
        <div class="list-price" uk-grid>
        	<div class="uk-width-auto">
            <div class="pricetag uk-margin-small-right uk-text-bold">
                <span>฿ {{number_format($item['subtotal'], 2)}}</span>
            </div>
        </div>
        <div class="uk-width-expand uk-text-right">
            <a href="javascript:void(0);" data-itemcode="{{ $item['item_code'] }}" data-store_id="{{ $storeId }}" data-rowid="{{ $item['rowid'] }}" data-cartid="{{ $cartId }}" data-action="delete-cart-item" data-product-variant-id="{{ $item['product_variant_id'] }}" class="remove-item uk-button uk-button-link text -size13">X @lang('action_btn.delete_btn')</a>
        </div>
        </div>
    </figcaption>
</figure><!-- /item -->
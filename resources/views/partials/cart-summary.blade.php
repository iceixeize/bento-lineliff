<div class="line t_basic b_basic bg-white uk-margin-top">
    <div class="uk-container uk-container-small pt_15 pb_15">
        
        <div id="summaryZone">
            
	        <div class="uk-margin-remove-top" uk-grid>
	            <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.subtotal_txt')</div>
	            <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->sub_total_txt }}</div>
	        </div>

	        @if(! $cartContent->isEmpty())

	            @if($cartCalculate->isVatDocZero())
	            <div class="uk-margin-small-top" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.before_vat_txt')</div>
	                <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->before_vat_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->hasDiscount() && $cartCalculate->hasDiscountAmount())
	            @php
				$discountData = $cart->getDiscount();
				@endphp
	            <div class="uk-margin-small-top text -cta" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.discount_txt') 
	                	@if(isset($discountData->code)){{ $discountData->code }}@endif 
						@if(isset($discountData->type) && $discountData->type == 'p')({{ $discountData->discount }}%)@endif 
	                </div>
	                <div class="uk-width-1-2 uk-text-right">- {{ $cartCalculate->discount_amount_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->hasSuperDiscount() && $cartCalculate->hasSuperDiscountAmount())
	            <div class="uk-margin-small-top text -cta" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.super_discount_txt')</div>
	                <div class="uk-width-1-2 uk-text-right">- {{ $cartCalculate->super_discount_amount_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->hasLoyaltyDiscount() && $cartCalculate->hasLoyaltyDiscountAmount())
	            <div class="uk-margin-small-top text -cta" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.point_discount_txt')</div>
	                <div class="uk-width-1-2 uk-text-right">- {{ $cartCalculate->loyalty_points_discount_amount_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->hasCustomDiscount() && $cartCalculate->hasCustomDiscountAmount())
	            <div class="uk-margin-small-top text -cta" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.custom_discount_txt')</div>
	                <div class="uk-width-1-2 uk-text-right">- {{ $cartCalculate->custom_discount_amount_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->isVatDocZero())
	            <div class="uk-margin-small-top" uk-grid>
	                <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.vat_txt', ['vat' => (string) $cartCalculate->vat_calculate])</div>
	                <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->vat_amount_txt }}</div>
	            </div>
	            @endif

	            @if($cartCalculate->hasShippingMethod())
	                @if($cartCalculate->hasFreeShipping())
	                <div class="uk-margin-small-top text -cta" uk-grid>
	                    <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.shipping_txt')</div>
	                    <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->shipping_amount_txt }}</div>
	                </div>
	                @else
	                <div class="uk-margin-small-top" uk-grid>
	                    <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.shipping_txt')</div>
	                    <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->shipping_amount_txt }}</div>
	                </div>
	                @endif
	            @endif

	            @if($cartCalculate->hasPaymentMethod())
	                @if($cartCalculate->hasPaymentFee() && $cartCalculate->hasPaymentAmount())
	                <div class="uk-margin-small-top" uk-grid>
	                    <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.payment_fee_txt')</div>
	                    <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->payment_amount_txt }}</div>
	                </div>
	                @endif
	            @endif

	        @endif

	        <hr class="uk-margin-remove-bottom">
	        <div class="uk-margin-small-top text -cta uk-text-bold" uk-grid>
	            <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.grand_total_txt')</div>
	            <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->grand_total_txt }}</div>
	        </div>
	        <hr class="uk-margin-small-top uk-margin-remove-bottom">

		        @if(! $cartContent->isEmpty())

		        @if($cartCalculate->isVatDocOne() || $cartCalculate->isVatDocTwo())
		        <div class="uk-margin-small-top uk-text-bold" uk-grid>
		            <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.before_vat_txt')</div>
		            <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->before_vat_txt }}</div>
		        </div>

		        <div class="uk-margin-small-top" uk-grid>
		            <div class="uk-width-1-2 uk-first-column">@lang('order.order_summary.vat_txt', ['vat' => (string) $cartCalculate->vat_calculate])</div>
		            <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->vat_amount_txt }}</div>
		        </div>

		        @endif

		        @if($cartCalculate->hasGetLoyaltyPoints() && $cartCalculate->hasGetLoyaltyPointsAmount())
		        <div class="uk-margin-small-top" uk-grid>
		            <div class="uk-width-1-2 uk-first-column"><i class="icon-badge icons text -orange"></i>&nbsp;&nbsp;@lang('order.order_summary.get_point_txt')</div>
		            <div class="uk-width-1-2 uk-text-right">{{ $cartCalculate->loyalty_points_sub_total_txt }} @lang('content.point_txt')</div>
		        </div>
		        @endif

	        @endif
        
        </div>

        <div class="uk-margin-large-top" uk-grid>
            <div class="uk-width-1-1">

            	@php
            	$checkoutDisabled = true;
            	if ($cartCalculate->shipping_guest_address &&
		            $cartCalculate->shipping_method &&
		        	$cartCalculate->payment_method) {
            		$checkoutDisabled = false;
            	}
            	@endphp

            	@if (\Auth::check())
                <button type="submit" id="checkout-btn" class="uk-button btn btn-cta uk-width-1-1" {{ ($checkoutDisabled ? 'disabled' : '') }}>@lang('mobilefrontend.partials.cart.cart_summary.submit_btn')</button>
                @else
                <a href="{{ I18n::route('mobilefrontend.login', ['return_uri' => I18n::getCurrentUrl(false, true)]) }}" class="uk-button btn btn-cta uk-width-1-1">@lang('mobilefrontend.pages.login.form.login_btn')</a>
                @endif
				

				@if(! empty($category))
                <a href="{{ I18n::route('mobilefrontend.category.index', ['categoryId' => $category->category_id, 'categorySeoUrl' => $category->seo_url]) }}" class="uk-button uk-button-link uk-text-center uk-width-1-1 uk-padding-small uk-margin-small-top">@lang('mobilefrontend.partials.cart.cart_summary.back_url')</a>
                @else
                <a href="{{ I18n::route('mobilefrontend.home.index') }}" class="uk-button uk-button-link uk-text-center uk-width-1-1 uk-padding-small uk-margin-small-top">@lang('mobilefrontend.partials.cart.cart_summary.back_url')</a>
                @endif
            </div>
        </div>
    </div>
</div>
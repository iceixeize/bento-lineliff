
<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="{{ asset('css/liff/uikit.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/liff/styles.css') }}">
</head>

<body>
    <div class="main-page" class="pb_30">
        <div class="uk-container uk-container-small uk-padding-small uk-position-center uk-text-center">
            
            <img src="{{ asset('img/404.svg') }}" width="130">
            <h2 class="text -gray -size18">ไม่พบหน้านี้ในระบบ</h2>
        </div>            
    </div>

    <script src="{{ asset('js/liff/uikit.min.js') }}"></script>

</body>

</html>

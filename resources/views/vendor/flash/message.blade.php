@foreach (session('flash_notification', collect())->toArray() as $message)
    @if ($message['overlay'])
        @include('flash::modal', [
            'modalClass' => 'flash-modal',
            'title'      => $message['title'],
            'body'       => $message['message']
        ])
    @else
        <div class="uk-alert uk-alert-{{ $message['level'] }}
                    {{ $message['important'] ? 'alert-important' : '' }}"
                    role="alert">
            @if ($message['important'])
                <!-- <button type="button"
                        class="close"
                        data-dismiss="alert"
                        aria-hidden="true"
                >&times;</button> -->

            @endif
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" disabled=""><span aria-hidden="true">×</span></button>
<!-- <a class="uk-alert-close uk-close uk-icon" uk-close=""><svg width="14" height="14" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg"><line fill="none" stroke="#000" stroke-width="1.1" x1="1" y1="1" x2="13" y2="13"></line><line fill="none" stroke="#000" stroke-width="1.1" x1="13" y1="1" x2="1" y2="13"></line></svg></a> -->
            <p>{!! $message['message'] !!}</p>

        </div>
    @endif
@endforeach

{{ session()->forget('flash_notification') }}

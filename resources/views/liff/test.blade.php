@extends('layouts.master')

@section('header')
@endsection

@section('content')
<div class="container">
	<div id="result"></div>
</div>



@endsection

@section('footer')
<script src="https://d.line-scdn.net/liff/1.0/sdk.js" /></script>
@endsection

@section('script')

<script>
	liff.init(function (data) {
		getProfile();
	});

    // Send message
    function sendMessage() {
    	liff.sendMessages([{
    		type: 'text',
    		text: "Send text message"
    	}, {
    		type: 'sticker',
    		packageId: '2',
    		stickerId: '144'
    	}]).then(function () {
    		window.alert("Sent");
    	}).catch(function (error) {
    		window.alert("Error sending message: " + error);
    	});
    }

	// Get profile and display
	function getProfile(){
	    liff.getProfile().then(function (profile) {
	    	let lineUserId = profile.userId;
	    	// let displayName = profile.displayName;
	    	// let img = profile.pictureUrl;
	    	// let statusMessage = profile.statusMessage;
	    	// alert(JSON.stringify(profile));

	    	// alert('line user id : ' + lineUserId);
	    	// alert('line displayname : ' + displayName);
	    	// alert('line image : ' + img);
	    	// alert('line statusMessage : ' + statusMessage);
	    	// alert('line channel : ' + profile.channelId);

	    }).catch(function (error) {
	    	window.alert("Error getting profile: " + error);
	    });
	}
</script>
@endsection



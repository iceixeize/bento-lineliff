@extends('layouts.master')

@section('header')
<link rel="stylesheet" href="{{ asset('js/liff/jquery.Thailand.js/jquery.Thailand.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/liff/intl-tel-input-12.1.15/css/intlTelInput.min.css') }}">
<link href="{{ asset("css/liff/styles-summary.css", true) }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="header">
    <div class="uk-container uk-container-small">
        <h1>สินค้าในตะกร้า</h1>
        <a href="javascript:close_window();" class="header-btn -left icon-btn" title=""><i class="icon-arrow-left icons"></i></a>
    </div>

</div><!-- /header -->
<div class="main-page">
    @include('flash::message')
    {{-- <form method="POST" action="{{ URL::route('bentoCart.cartCheckOut', ['cart_id' => $cartId]) }}" accept-charset="UTF-8" id="checkoutForm" novalidate="novalidate"> --}}
        <form class="form-custom" action="{{ URL::route('bentoCart.cartCheckOut') }}" id="formCart" method="POST">
            @csrf
            <div class="wrapper-container uk-padding-remove-bottom">
                <div class="uk-container uk-container-small">
                    <h3 class="text -gray -size14 uk-text-bold uk-margin-remove-top mb_5">รายละเอียดการสั่งซื้อในครั้งนี้:</h3> 
                </div>


                @include('partials.cart-content')


                <div class="uk-container uk-container-small">
                    <h3 class="text -gray -size14 uk-text-bold mb_5">จัดส่งมายังที่อยู่นี้:</h3> 
                </div>

                @if(count($customerAddress) > 0)
                <div class="line t_basic b_basic bg-white uk-margin-bottom">
                    <div class="uk-container uk-container-small">
                        <a class="uk-button uk-button-link btn-arrow uk-text-bold uk-padding-small text -black -size16 uk-display-block uk-text-break uk-text-capitalize btnSelectGuest" href="javascript:void(0)">{{ $customerAddress['firstname'] }} {{ $customerAddress['lastname'] }}<br> {{ $customerAddress['full_address'] }}</a>
                    </div>
                </div>
                @else
                <div class="line t_basic b_basic bg-white uk-margin-bottom">
                    <div class="uk-container uk-container-small">
                        <a class="uk-button uk-button-link btn-arrow uk-text-bold uk-padding-small text -black -size16 uk-display-block uk-text-break uk-text-capitalize btnSelectGuest" href="javascript:void(0)">เลือกที่อยู่ในการจัดส่ง</a>
                    </div>
                </div>
                @endif

                <div class="uk-container uk-container-small">
                    <h3 class="text -gray -size14 uk-text-bold mb_5">หมายเลขโทรศัพท์:</h3> 
                </div>
                <div class="line t_basic b_basic bg-white uk-margin-bottom">
                    <div class="uk-container uk-container-small pt_15 pb_15">
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-1">
                                <input class="uk-input" id="phone" type="tel" name="phone" aria-describedby="phone-error" placeholder="กรอกหมายเลขโทรศัพท์" value="@isset($customerAddress['phone']){{ $customerAddress['phone']}}@endisset">
                                <span id="phone-error" class="invalid-feedback"></span>
                                <span class="help-block">ใช้สำหรับติดต่อก่อนจัดส่งสินค้า กรุณาตรวจสอบความถูกต้อง</span>

                            </div>

                        </div>
                    </div>
                </div>

                <!-- SHIPPING METHOD -->

                @include('partials.shipping-list')

                @include('partials.payment-list')

                <span id="summary-container">
                    {!! $cartAll['html'] !!}
                </span>
                
                @include('partials.cart-btn-pay')


                @include('partials.cart-all-jsrender')

                {{-- @include('partials.cart-summary') --}}

            </div><!-- /wrapper-container -->
            <input type="hidden" name="line_user_id" id="line_user_id" value="">
            <input type="hidden" name="store_id" id="store_id" value="{{ $storeId }}">
            <input type="hidden" name="cart_id" id="cart_id" value="{{ $cartId }}">


        </form>
    {{-- </form> --}}
</div><!-- /customer-review -->
@endsection


@section('footer')
<!-- Laravel Javascript Validation -->
{!! JsValidator::formRequest('App\Http\Requests\PhoneRequest', '#formCart') !!}
@endsection

@section('liff')
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
@endsection

@section('script')
<script src="{{ asset('js/liff/jquery.Thailand.js/JQL.min.js') }}"></script>
<script src="{{ asset('js/liff/jquery.Thailand.js/jquery.Thailand.js') }}"></script>
<script src="{{ asset('js/liff/intl-tel-input-12.1.15/js/utils.js') }}"></script>

<script src="{{ asset('js/liff/intl-tel-input-12.1.15/js/intlTelInput.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/liff/libphonenumber/intlTelInput.js') }}"></script>
<script>
    $(function(){
        let guestAddressId = 0;
        var cartId = "{{ $cartId }}";
        var storeId = "{{ $storeId }}";
        var uId = "";

        function getLineCookies() {
            return Cookies.getJSON('lineUserData');
        }

        $('#close').click(function(){
            alert('close');
            window.close();
        });

        function getLineCookiesUId() {
            var lineCookiesData = getLineCookies();
            if (typeof lineCookiesData !== "undefined") {
                return lineCookiesData.data.userId;
            } else {
                return null;
            }
        }

        function validUIdBeforeSubmit() {
            if(uId == null) {
                Swal.fire('ไม่พบข้อมูลผู้ใช้ กรุณาออกจากหน้านี้แล้วลองอีกครั้ง');
                return false;
            } else {
                return true;
            }
        }

        
        var uId = getLineCookiesUId();
        $('#line_user_id').val(uId);

        // $("#phone").intlTelInput({
        // });

        // $('[name="phone"]').intlTelInput({
        //     formatOnDisplay: false,
        //     initialCountry: '{{ $defaultCountry }}',
        //     preferredCountries: ['th'],
        //     hiddenInput: 'full_phone',
        // });    

        $('#formCart [name="phone"]').intlTelInput({
            formatOnDisplay: false,
            initialCountry: '{{ $defaultCountry }}',
            preferredCountries: ['th'],
            hiddenInput: 'full_phone',
        });

        jQuery.validator.addMethod('validPhone', function(value, element) {
            console.log(value);
            console.log(element);
            console.log(this.optional(element));
            console.log($(element).intlTelInput('isValidNumber'));
            return this.optional(element) || $(element).intlTelInput('isValidNumber');
        }, 'Please enter a valid phone number.');

        // jQuery.validator.addMethod('validPhone', function(value, element) {
        //     console.log(value);
        //     console.log(element);
        //     console.log(this.optional(element));
        //     console.log($(element).intlTelInput('isValidNumber'));

        //     return this.optional(element) || $(element).intlTelInput('isValidNumber');
        // }, 'Please enter a valid phone number.');

        $('#formCart [name="phone"]').rules('add', {
            validPhone: true,
        });

        $(document).on('click', '.select-shipping', function() {
            if ($(this).is(':checked')) {
                let shippingKey = $(this).data('shipping_key');
                let storeId = $(this).data('store_id');
                let cartId = $(this).data('cart_id');
                let action = 'shipping';
                let guestAddressId = {{ $guestAddressId }};
                var sendData = {shipping_key: shippingKey, store_id: storeId, cart_id: cartId, guest_address_id: guestAddressId, social_id: uId};

                // alert(JSON.stringify(sendData));
                // alert(uId);


                if(uId === null) {
                    Swal.fire('ไม่พบข้อมูลผู้ใช้ กรุณาออกจากหน้านี้แล้วลองอีกครั้ง').then((result) => {
                        if(result.value == true) {
                            $('input[name="select-shipping"]').prop('checked', false);
                        }
                    });
                } else {
                    ajaxCart(action, sendData);
                }
            }
        });

        $(document).on('click', '.select-payment', function() {
            if ($(this).is(':checked')) {
                let paymentKey = $(this).data('payment_key');
                let storeId = $(this).data('store_id');
                let cartId = $(this).data('cart_id');
                let action = 'payment';
                let guestAddressId = {{ $guestAddressId }};

                var sendData = {payment_key: paymentKey, store_id: storeId, cart_id: cartId, guest_address_id: guestAddressId, social_id: uId};
                if(uId === null) {
                    Swal.fire('ไม่พบข้อมูลผู้ใช้ กรุณาออกจากหน้านี้แล้วลองอีกครั้ง').then((result) => {
                        if(result.value == true) {
                            $('input[name="select-payment"]').prop('checked', false);
                        }
                    });
                } else {
                    ajaxCart(action, sendData);
                }
            }
        });

        $('.btn-cta').click(function(){
            var $this = $(this);
            $('.btn-cta').empty().append('<span>กำลังส่งข้อมูล...</span><div class="ld ld-ring ld-spin-fast"></div>');
            $this.addClass("running");
        });




        $(document).on('click', '.remove-item', function() {
            let itemCode = $(this).data('itemcode');
            let cartId = $(this).data('cartid');
            let storeId = $(this).data('store_id');
            let action = 'delete-cart-item';

            var sendData = {item_code: itemCode, cart_id: cartId, store_id: storeId, social_id: uId};
            console.log(sendData);
            Swal.fire({
              title: 'ยืนยันการลบสินค้า ?',
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'ใช่',
              cancelButtonText: 'ไม่ใช่',
          }).then((result) => {
              if (result.value) {
                ajaxCart(action, sendData);

            }
        });

      });


        $(document).on('click', '.update-item', function() {
            let itemCode = $(this).data('itemcode');
            let action = $(this).data('action');
            let cartId = $(this).data('cartid');
            let storeId = $(this).data('store_id');

            let qty = $('#qty_' + itemCode).val();

            if(qty == 0) {
                action = 'delete-cart-item';
            }
            let productVariantId = $(this).data('product-variant-id');

            var sendData = {item_code: itemCode, social_id: uId, qty: qty, cart_id: cartId, store_id: storeId};
            ajaxCart(action, sendData);
        });



        $(document).ajaxStart(function() {
            $.blockUI({ message: 'กำลังโหลด...' });
        });

        $(document).ajaxStop(function() {
            $.unblockUI();
        });

        $('#inputTel').intlTelInput({
            formatOnDisplay: false,
            initialCountry: '{{ $defaultCountry }}',
            preferredCountries: ['th'],
            hiddenInput: 'full_phone',
        });

        function ajaxCart(action, sendData) {
            var valid = validUIdBeforeSubmit();
            sendData.line_user_id = uId;
            let url = '';
            if(action == 'edit-cart') {
                url = "{!! URL::route('bentoCart.updateCart') !!}";
            } else if(action == 'delete-cart-item') {
                url = "{!! URL::route('bentoCart.deleteCartItem') !!}";
            } else if(action == 'shipping') {
                url = "{!! URL::route('bentoCart.addShippingToCart') !!}";
            } else if(action == 'payment') {
                url = "{!! URL::route('bentoCart.addPaymentToCart') !!}";
            } else if(action == 'get_cart_info') {
                url = "{!! URL::route('bentoCart.getCartInfo') !!}";
            }

            if(valid === true) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    data: sendData,
                    success: function(data) {
                        if(action == 'delete-cart-item' || action == 'edit-cart') {
                        // console.log(data.cart);
                        if(data.cart.cartAll.prepare.data.cart.total_items == 0) {
                            window.location="{{ URL::route('bentoCart.emptyCart', ['channel_id' => $channelId, 'store_id' => $storeId]) }}";
                        }
                    }


                    if(data.success != 1) {
                        if(typeof(data.cart) != "undefined" && data.cart !== null) {
                            renderCart(data.cart);
                        }
                        Swal.fire(data.message);
                    } else {
                        $.unblockUI();
                        // Swal.fire(data.message);
                        if(typeof(data.cart) != "undefined" && data.cart !== null) {
                            renderCart(data.cart);
                        }
                    }
                }
            });
            }
        }

        function renderCart(data) {
            console.log('----- render cart -----');
            var cartItemTmpl = $.templates("#cartItemTmp");
            var paymentMethodTmpl = $.templates("#paymentMethodListItemTmp");
            var shippingMethodTmpl = $.templates("#shippingMethodListItemTmp");
            var buttonTmpl = $.templates("#buttonTmp");

            var dataCart = {cartCal: data['cartCal'], cartId: data.cartId, storeId: data.storeId};
            var dataPayment = {payment: data.paymentMethod, paymentSelected: data['cartAll']['prepare']['data']['payment_method'], storeId: data.storeId, cartId: data.cartId};
            var dataShipping = {shipping: data['shippingMethod'], shippingSelected: data['cartAll']['prepare']['data']['shipping_method'], storeId: data.storeId, cartId: data.cartId};

            // console.log(dataShipping);

            var cartItemHtml = cartItemTmpl.render(dataCart);
            var paymentHtml = paymentMethodTmpl.render(dataPayment);      
            var shippingHtml = shippingMethodTmpl.render(dataShipping);

            var buttonHtml = buttonTmpl.render({validBtn: data.validBtn});      

            $("#cart-item-container").html(cartItemHtml);
            $('#payment-list-container').html('');
            $("#payment-list-container").html(paymentHtml);
            $('#summary-container').html(data.cartAll.html);
            $("#shipping-list-container").html(shippingHtml);
            // console.log({validBtn: data.validBtn});
            $("#button-container").html("");
            $("#button-container").html(buttonHtml);



        }

        $(document).on('click', '#btn-paid', function() {
            $.blockUI({message: "กำลังโหลด..."});
        //     console.log('click btn paid');

        //     liff.openWindow({
        //       url:"",
        //     // external:true
        // });
    });

        // $(document).on('submit', '#formCart', function(e) {
        //     e.preventDefault();

        //     var url = "{{ URL::route('bentoCart.cartCheckOut') }}";
        //     url = url + "?cart_id=" + cartId + 
        //     alert(url);
        // });

        $(document).on('click', '.btnSelectGuest', function() {
            var url = "{{ URL::route('customerAddress') }}";
            window.location = url + '?store_id=' + storeId + '&line_user_id=' + uId;
        });
    });
</script>

<!-- Initialize Swiper -->
@endsection


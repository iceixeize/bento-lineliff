@extends('layouts.master')

@section('header')
<link href="{{ asset("css/liff/styles-summary.css", true) }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="header">
    <div class="uk-container uk-container-small">
        <h1>สินค้าในตะกร้า</h1>
    </div>

</div><!-- /header -->
<div class="main-page">
        <div class="wrapper-container uk-padding-remove-bottom">
            <div class="uk-position-medium uk-position-cover uk-flex uk-flex-center uk-flex-middle uk-text-center">
                <div><img src="{{ asset('img/online-shop-icon.svg')}}" width="200">
                <p>รถเข็นสินค้าคุณว่าง</p>
                <a class="uk-button btn-primary text -size16" onClick="window.close()">กลับไปที่หน้าสินค้า</a></div>
            </div>
        </div><!-- /wrapper-container -->
    {{-- </form> --}}
</div><!-- /customer-review -->
@endsection


@section('footer')
@endsection

@section('liff')
@endsection

@section('script')
@endsection


@extends('layouts.master')

@section('header')
@endsection

@section('content')

<div class="header">
    <div class="uk-container uk-container-small">
        <h1>จัดการที่อยู่สำหรับการจัดส่ง</h1>
        <a href="javascript: history.go(-1)" class="header-btn -left icon-btn" title=""><i class="icon-arrow-left icons"></i></a>
        <a href="javascript:void(0)" class="header-btn -left icon-btn" title="" id="btnBack"><i class="icon-arrow-left icons"></i></a>
    </div>

</div><!-- /header -->
<div class="main-page">
    @include('flash::message')
    <div class="wrapper-container uk-padding-remove-top">

        <div class="bg-white line t_basic b_basic pt_15 pb_15 uk-margin-bottom">
            <div class="uk-container uk-container-small">
                <div class="uk-grid-small" uk-grid>
                    <div class="uk-width-1-1">
                        <a class="uk-button uk-button-default uk-width-1-1 uk-text-left" id="addNewAddress" href="javascript:void(0)">เพิ่มที่อยู่ใหม่  <i class="text -size16 uk-position-center-right uk-position-small fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-container uk-container-small">
            <h3 class="text -gray -size14 uk-text-bold uk-margin-small-bottom">สถานที่จัดส่งที่บันทึกไว้:</h3> 
        </div>

        <div class="line t_basic b_basic bg-white customer-address">
            <ul class="uk-list uk-list-divider uk-margin-remove-bottom" id="customer-address-list" uk-switcher>
                @isset($customerAddressList)
                @foreach($customerAddressList as $index => $key)
                <li>
                    <div class="uk-container uk-container-small pt_15 pb_10">
                        <div class="uk-grid-small address-info" uk-grid>
                            <div class="uk-width-1-1">
                                <div class="pl_25 uk-position-relative">
                                    <span class="uk-radio uk-position-center-left"></span>
                                    <h3 class="text -size14 uk-text-bold uk-margin-remove">{{ $key['firstname'] }} {{ $key['lastname'] }}</h3>
                                    <p class="uk-text-small uk-margin-remove text -gray">
                                        {{ $key['phone'] }}<br>
                                    {{ $key['full_address'] }}</p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid-small customer-address-action" uk-grid>
                            <div class="uk-width-1-1"><button data-guest_id="{{ $key["guest_id"] }}" data-guest_address_id="{{ $key["guest_address_id"] }}" data-store_id="{{ $storeId }}" class="uk-button btn btn-success uk-width-1-1 btnSetDefaultAddress" type="button">จัดส่งมายังที่นี่</button></div><!-- /Deliver to this Address -->
                            <div class="uk-width-1-2 uk-grid-margin">

                                <a href="javascript:void(0)" data-type="edit" data-guest_address_id="{{ $key['guest_address_id'] }}" data-store_id="{{ $storeId }}" data-guest_id="{{ $key['guest_id'] }}" class="uk-button uk-button-default uk-width-1-1 btn-edit-guest">แก้ไข</a></div>
                                <div class="uk-width-1-2 uk-grid-margin">
                                    <a href="javascript:void(0)" data-guest_address_id="{{ $key['guest_address_id'] }}" data-store_id="{{ $storeId }}" class="uk-button uk-button-default uk-width-1-1 btn-del-guest">ลบ</a>
                                </div>
                            </div><!-- /customer-address-action -->
                        </div>
                    </li>
                    @endforeach
                    @endisset

                    {{-- <li class="uk-padding-remove-top">
                        <div class="uk-container uk-container-small pt_15 pb_10">
                            <div class="uk-grid-small address-info" uk-grid>
                                <div class="uk-width-1-1">
                                    <div class="pl_25 uk-position-relative">
                                        <span class="uk-radio uk-position-center-left"></span>
                                        <h3 class="text -size14 uk-text-bold uk-margin-remove">Nutthaseth Sirinanthananon</h3>
                                        <p class="uk-text-small uk-margin-remove text -gray">
                                        (+66)00012345<br>
                                        549/18 นันทนันท์ลิฟวิ่ง ห้อง 506
                                        ถ.สาธุประดิษฐ์ ซ.สาธุประดิษฐ์ 34 
                                        เขตยานนาวา แขวงบางโพงพาง
                                        กรุงเทพมหานคร 10400</p>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-grid-small customer-address-action" uk-grid>
                                <div class="uk-width-1-1"><button onclick="window.location.href='add-new-address.html'" class="uk-button btn btn-success uk-width-1-1" type="button">จัดส่งมายังที่นี่</button></div><!-- /Deliver to this Address -->
                                <div class="uk-width-1-2 uk-grid-margin"><a href="add-new-address.html" class="uk-button uk-button-default uk-width-1-1">แก้ไข</a></div>
                                <div class="uk-width-1-2 uk-grid-margin"><a href="add-new-address.html" class="uk-button uk-button-default uk-width-1-1">ลบ</a></div>
                            </div><!-- /customer-address-action -->
                        </div>
                    </li> --}}
                </ul>
            </div><!-- /customer-address" -->
        </div><!-- /wrapper-container -->
    </div><!-- /customer-review -->
    @endsection

    @section('script')
    <script type="text/javascript" src="{{ asset('js/liff/line-liff.js') }}"></script>


    <script>
        $(function() {
            var storeId = "{{ $storeId }}";
            function getLineCookies() {
                return Cookies.getJSON('lineUserData');
            }

            function getLineCookiesUId() {
                var lineCookiesData = getLineCookies();
                if (typeof lineCookiesData !== "undefined") {
                    return lineCookiesData.data.userId;
                } else {
                    return null;
                }
            }

            function validUIdBeforeSubmit() {
                if(uId == null) {
                    Swal.fire('ไม่พบข้อมูลผู้ใช้ กรุณาออกจากหน้านี้แล้วลองอีกครั้ง');
                } else {
                    return true;
                }
            }

            var uId = getLineCookiesUId();
            // alert('uId: ' + uId);

            $(document).on('click', '.btn-del-guest', function() {
                var guestAddressId = $(this).data('guest_address_id');
                // var storeId = $(this).data('store_id');

                var href = '{{ URL::route("bentoCart.deleteGuestAddress") }}';
                href = href + '?guest_address_id=' + guestAddressId + '&store_id=' + storeId + '&line_user_id=' + uId;

                Swal.fire({
                  title: "คุณมั่นใจว่าจะลบที่อยู่นี้ ?",
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                  if (result.value) {
                    $.blockUI({ message: 'กำลังโหลด...' });
                    window.location = href;
                }
            })
          });

            $(document).on('click', '.btn-edit-guest', function() {
                var type = $(this).data('type');
                var guestAddressId = $(this).data('guest_address_id');
                var guestId = $(this).data('guest_id');
                // var storeId = $(this).data('store_id');
                var url = "{{ URL::route('bentoCart.editAddress') }}";
                url = url + '?type=' + type + '&guest_address_id=' + guestAddressId + '&guest_id=' + guestId + '&store_id=' + storeId + '&line_user_id=' + uId;
                // alert(url);
                $.blockUI({ message: 'กำลังโหลด...' });

                window.location = url;
            });

            $(document).on('click', '#btnBack', function() {
                var urlCart = "{{ URL::route('bentocart.cart') }}";
                var url = urlCart + '?store_id=' + storeId + '&line_user_id=' + uId;
                // alert('url: ' + url);
                $.blockUI({ message: 'กำลังโหลด...' });
                
                window.location = url;
            });

            $(document).on('click', '.btnSetDefaultAddress', function() {
                var guestId = $(this).data('guest_id');
                var guestAddressId = $(this).data('guest_address_id');
                var url = '{{ URL::route("setDefaultAddress") }}';
                url = url + '?guest_id=' + guestId + '&guest_address_id=' + guestAddressId + '&store_id=' + storeId + '&line_user_id=' + uId;
                $.blockUI({ message: 'กำลังโหลด...' });
                window.location = url;
            });

            $(document).on('click', '#addNewAddress', function() {
                var url = "{{ URL::route('addAddress') }}";
                // var storeId = storeId;
                url = url + '?store_id=' + storeId + '&line_user_id=' + uId;
                // alert(url);
                $.blockUI({ message: 'กำลังโหลด...' });

                window.location = url;

            });

        });
    </script>
    @endsection


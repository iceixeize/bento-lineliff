@extends('layouts.master')

@section('header')
<link rel="stylesheet" href="{{ asset('js/liff/jquery.Thailand.js/jquery.Thailand.min.css') }}">
<link rel="stylesheet" href="{{ asset('js/liff/intl-tel-input-12.1.15/css/intlTelInput.min.css') }}">


<style>
  .tt-menu{font-size: 12px;}
  .twitter-typeahead{
    display:block !important;
  }
  .iti-container {
    z-index: 35000 !important;
  }
  .tt-hint[disabled], .tt-hint[readonly] {
    background-color: #fff !important;
  }
</style>
@endsection

@section('content')
<div class="header">
  <div class="uk-container uk-container-small">
    <h1>{{ $textHead }}</h1>
    <a href="javascript:void(0)" id="btnBack" class="header-btn -left icon-btn" title=""><i class="icon-arrow-left icons"></i></a>
  </div>

</div><!-- /header -->
<div class="main-page">
  @include('flash::message')
  
  {{-- flash data --}}
  @isset($error)
  <div class="uk-alert-danger" uk-alert>
    <a class="uk-alert-close" uk-close></a>
    <p>{{ $error }}</p>
  </div>
  @endisset


  <div class="wrapper-container">
    <form class="form-custom" action="{{ $href }}" id="customerAddressForm" method="POST">
      <div class="uk-container uk-container-small">
        <h3 class="text -gray -size14 uk-text-bold uk-margin-remove-top mb_5">ข้อมูลผู้รับสินค้า:</h3>
      </div>
      <div class="bg-white uk-margin-bottom line t_basic b_basic">
        <div class="uk-container uk-container-small pt_15 pb_15">
          <div class="uk-grid-small" uk-grid>
            <div class="uk-width-1-1">
              <input class="uk-input"  id="firstname" name="firstname" type="text" placeholder="กรอกชื่อ" value="@isset($customerAddress['firstname']){{ $customerAddress['firstname']}}@endisset">
            </div>
            <div class="uk-width-1-1">
              <input class="uk-input" id="lastname" name="lastname" type="text" placeholder="กรอกนามสกุล" value="@isset($customerAddress['lastname']){{ $customerAddress['lastname']}}@endisset">
            </div>
            <div class="uk-width-1-1">
              <input class="uk-input" id="email" name="email" type="email" placeholder="กรอกอีเมล์" value="@isset($customerAddress['email']){{ $customerAddress['email']}}@endisset">
            </div>
            <div class="uk-width-1-1">
              <input class="uk-input" id="phone" name="phone" type="tel" aria-describedby="phone-error" placeholder="กรอกหมายเลขโทรศัพท์" value="@isset($customerAddress['phone']){{ $customerAddress['phone']}}@endisset">
              <span id="phone-error" class="invalid-feedback"></span>
            </div>
            <input name="store_id" type="hidden" value="{{ $storeId }}">

            {{-- <span id="phone-error" class="help-block error-help-block"></span> --}}

          </div>
        </div>
      </div><!--- bg-white -->
      <div class="bg-white uk-margin-bottom line t_basic b_basic">
        <div class="uk-container uk-container-small pt_15 pb_15">

          <div id="thailand-block" class="uk-width-1-1">
            <input type="hidden" name="sub_dist_id" id="sub_dist_id" value="@isset($customerAddress['sub_dist_id']){{ $customerAddress['sub_dist_id']}}@endisset">
            <input type="hidden" name="subdistrict" id="subdistrict" value="@isset($customerAddress['subdistrict']){{ $customerAddress['subdistrict']}}@endisset">
            <input type="hidden" name="dist_id" id="dist_id" value="@isset($customerAddress['dist_id']){{ $customerAddress['dist_id']}}@endisset">
            <input type="hidden" name="district" id="district" value="@isset($customerAddress['district']){{ $customerAddress['district']}}@endisset">
            <input type="hidden" name="prov_id" id="prov_id" value="@isset($customerAddress['prov_id']){{ $customerAddress['prov_id']}}@endisset">
            <input type="hidden" name="province" id="province" value="@isset($customerAddress['province']){{ $customerAddress['province']}}@endisset">
            <input type="hidden" name="zipcode" id="zipcode" value="@isset($customerAddress['zipcode']){{ $customerAddress['zipcode']}}@endisset">
          </div>

          <div class="uk-grid-small uk-grid uk-grid-stack" uk-grid>
            <div class="uk-width-1-1 uk-first-column">
              <select name="country" id="country" class="uk-select country mt_5 mb_5" placeholder="ประเทศ">
                @isset($countryDom)
                @foreach($countryDom as $index => $name)
                @isset($customerAddress->country)
                <option value="{{ $index }}" @if($index == $customerAddress->country) selected @endif>{{ $name }}</option>
                @endisset
                <option value="{{ $index }}" @if($index == 5) selected @endif>{{ $name }}</option>
                @endforeach
                @endisset
              </select>
              <span id="country-error" class="help-block error-help-block uk-text-danger"></span>
              {{-- <input class="uk-input" id="inputCountry" type="text" placeholder="ประเทศ"> --}}
            </div>
            <div class="uk-width-1-1" id="sec-zipcode">
              <input class="uk-input uk-text-left" name="st_search" id="st-search" type="text" maxlength="200" placeholder="ตำบล, อำเภอ, จังหวัด, รหัสไปรษณีย์" value="@isset($customerAddress['zipcode']){{ $customerAddress['subdistrict'] . ', ' . $customerAddress['district'] . ', ' . $customerAddress['province'] . ', ' . $customerAddress['zipcode']}}@endisset">
            </div>
            <div class="uk-width-1-1">
              <input class="uk-input uk-text-left" name="address_1" id="address_1" type="text" maxlength="150" placeholder="ที่อยู่บรรทัดที่ 1: บ้านเลขที่, หมู่ที่, ชื่ออาคาร" value="@isset($customerAddress['address_1']){{ $customerAddress['address_1'] }}@endisset">
            </div>
            <div class="uk-width-1-1">
              <input class="uk-input uk-text-left" name="address_2" id="address_2" type="text" maxlength="150" placeholder="ที่อยู่บรรทัดที่ 2: ซอย ถนน">
            </div>

            <div id="world-block" class="uk-width-1-1 uk-grid-small uk-padding-bottom" uk-grid>
              <div class="uk-width-1-1 uk-first-column">
                <input class="uk-input uk-text-left" name="district" id="district" maxlength="50" type="text" placeholder="เขต/อำเภอ">
              </div>
              <div class="uk-width-1-1 uk-grid-margin uk-first-column">
                <input class="uk-input uk-text-left" name="province" id="province" maxlength="50" type="text" placeholder="จังหวัด">
              </div>

              <div class="uk-width-1-1 uk-grid-margin uk-first-column">
                <input class="uk-input uk-text-left" name="zipcode" id="zipcode" maxlength="9" type="text" placeholder="รหัสไปรษณีย์">
              </div>
            </div>

          </div>
          <div class="uk-width-1-1 uk-grid-margin uk-first-column">
            <h3 class="uk-text-bold text -size16 uk-margin-remove-top uk-margin-small-bottom">แสดงตัวอย่างที่อยู่ของคุณ</h3>
            <p class="uk-margin-remove line-height-small" id="customerAddressExample">
              <!-- Example Address -->  
            </p>
            <div class="postal-code uk-margin-small-top">
              <span class="square-box">1</span>
              <span class="square-box">0</span>
              <span class="square-box">4</span>
              <span class="square-box">0</span>
              <span class="square-box">0</span>
            </div>
          </div>
          <div class="uk-width-1-1">
            <input type="hidden" id="line_user_id" name="line_user_id" value="">
            <button type="submit" class="uk-button btn btn-success uk-width-1-1">บันทึกที่อยู่ใหม่</button> 

            {{-- <button onclick="window.location.href='customer-address.html'" class="uk-button btn btn-success uk-width-1-1">บันทึกที่อยู่ใหม่</button>  --}}
          </div>

        </div>
      </div><!--- bg-white -->
    </form>
  </div><!-- /wrapper-container -->
</div><!-- /customer-review -->

@endsection


@section('footer')
<script src="{{ asset('js/liff/jquery.Thailand.js/JQL.min.js') }}"></script>
<script src="{{ asset('js/liff/jquery.Thailand.js/typeahead.bundle.js') }}"></script>
<script src="{{ asset('js/liff/jquery.Thailand.js/jquery.Thailand.js') }}"></script>

<script src="{{ asset('js/liff/intl-tel-input-12.1.15/js/intlTelInput.min.js') }}"></script>
<script src="{{ asset('js/liff/intl-tel-input-12.1.15/js/utils.js') }}"></script>


<!-- Laravel Javascript Validation -->
{!! JsValidator::formRequest('App\Http\Requests\CustomerAddAddressRequest', '#customerAddressForm') !!}
@endsection

@section('liff')
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
@endsection

@section('script')
<script>
  var countryShortNameList = {!! $countryDomShortName->toJson() !!};
  $(function() {

    function getLineCookies() {
      return Cookies.getJSON('lineUserData');
    }

    function getLineCookiesUId() {
      var lineCookiesData = getLineCookies();
      if (typeof lineCookiesData !== "undefined") {
        return lineCookiesData.data.userId;
      } else {
        return null;
      }
    }

    function validUIdBeforeSubmit() {
      if(uId == null) {
        Swal.fire('ไม่พบข้อมูลผู้ใช้ กรุณาออกจากหน้านี้แล้วลองอีกครั้ง');
      } else {
        return true;
      }
    }

    var uId = getLineCookiesUId();
    $('#line_user_id').val(uId);

    jQuery.validator.addMethod('validPhone', function(value, element) {
     console.log(value);
     console.log(element);
     console.log(this.optional(element));
     console.log($(element).intlTelInput('isValidNumber'));

     return this.optional(element) || $(element).intlTelInput('isValidNumber');
     return this.optional(element) || $(element).intlTelInput('isValidNumber');
   }, 'Please enter a valid phone number.');

    $('#customerAddressForm [name="phone"]').rules('add', {
      validPhone: true,
    });
    $.fn.clearStSearch = function() {
      setTimeout(function() {
        $('#st-search').val('');
      }, 30);

      $('#st-search').typeahead('val', '');

      $('#sub_dist_id').val('');
      $('#subdistrict').val('');
      $('#dist_id').val('');
      $('#district').val('');
      $('#prov_id').val('');
      $('#province').val('');
      $('#zipcode').val('');

      $.fn.updateCustomerAddressExample();
    };

    $.fn.switchForm = function() {
      var countryId = $('[name="country"]').val();
      // alert('country id : ' + countryId);
      if (countryId == 5) {
        $('#thailand-block #st-search').prop('disabled', false);
        $('#thailand-block #sub_dist_id').prop('disabled', false);
        $('#thailand-block #subdistrict').prop('disabled', false);
        $('#thailand-block #dist_id').prop('disabled', false);
        $('#thailand-block #district').prop('disabled', false);
        $('#thailand-block #prov_id').prop('disabled', false);
        $('#thailand-block #province').prop('disabled', false);
        $('#thailand-block #zipcode').prop('disabled', false);

        $('#world-block [name="district"]').prop('disabled', true);
        $('#world-block [name="province"]').prop('disabled', true);
        $('#world-block [name="zipcode"]').prop('disabled', true);

        $('[name="address_1"]').attr('placeholder', 'ที่อยู่บรรทัดที่ 1: บ้านเลขที่, หมู่ที่, ชื่ออาคาร');
        $('[name="address_2"]').attr('placeholder', 'ที่อยู่บรรทัดที่ 2: ซอย ถนน');

        $('#sec-zipcode').show();
        $('#thailand-block').show();
        $('#world-block').hide();
      } else {
        $('#thailand-block #st-search').prop('disabled', true);
        $('#thailand-block #sub_dist_id').prop('disabled', true);
        $('#thailand-block #subdistrict').prop('disabled', true);
        $('#thailand-block #dist_id').prop('disabled', true);
        $('#thailand-block #district').prop('disabled', true);
        $('#thailand-block #prov_id').prop('disabled', true);
        $('#thailand-block #province').prop('disabled', true);
        $('#thailand-block #zipcode').prop('disabled', true);

        $('#world-block [name="district"]').prop('disabled', false);
        $('#world-block [name="province"]').prop('disabled', false);
        $('#world-block [name="zipcode"]').prop('disabled', false);

        $('[name="address_1"]').attr('placeholder', 'ที่อยู่บรรทัดที่ 1: บ้านเลขที่, หมู่ที่, ชื่ออาคาร');
        $('[name="address_2"]').attr('placeholder', 'ที่อยู่บรรทัดที่ 2: ถนน ตำบล');

        $('#sec-zipcode').hide();
        $('#thailand-block').hide();
        $('#world-block').show();
      }
    };

    $.fn.customerAddressFormat = function() {
      var temp = '';
      var line_1 = line_2 = line_3 = line_4 = '';
      var data = {};
      var line = '<br>';
      var $form = $('#customerAddressForm');
      var $example = $('#customerAddressExample');

      if ($form.find('#country').val() != '') {
        var country_id = $form.find('[name="country"]').val();

        if (country_id == '5') {
          data = {
            firstname: $form.find('[name="firstname"]').val(),
            lastname: $form.find('[name="lastname"]').val(),
            address_1: $form.find('[name="address_1"]').val(),
            address_2: $form.find('[name="address_2"]').val(),
            sub_dist_id: $form.find('#thailand-block #sub_dist_id').val(),
            subdistrict: $form.find('#thailand-block #subdistrict').val(),
            dist_id: $form.find('#thailand-block #dist_id').val(),
            district: $form.find('#thailand-block #district').val(),
            prov_id: $form.find('#thailand-block #prov_id').val(),
            province: $form.find('#thailand-block #province').val(),
            zipcode: $form.find('#thailand-block #zipcode').val(),
            country_id: $form.find('[name="country"]').val(),
            country_name: ($form.find('[name="country"]').val() != '' ? $form.find('[name="country"] option:selected').text() : ''),
          };

        } else {
          data = {
            firstname: $form.find('[name="firstname"]').val(),
            lastname: $form.find('[name="lastname"]').val(),
            address_1: $form.find('[name="address_1"]').val(),
            address_2: $form.find('[name="address_2"]').val(),
            district: $form.find('#world-block [name="district"]').val(),
            province: $form.find('#world-block [name="province"]').val(),
            zipcode: $form.find('#world-block [name="zipcode"]').val(),
            country_id: $form.find('[name="country"]').val(),
            country_name: ($form.find('[name="country"]').val() != '' ? $form.find('[name="country"] option:selected').text() : ''),
          };
        }
      } else {
        return '<h5 class="text-center text -basic">@lang('mobilefrontend.partials.customer_address_form.validate.required')</h5>';
      }

        // Line 1
        if (data.hasOwnProperty('firstname') && data.firstname != '') {
          line_1 += data.firstname;
        }
        if (data.hasOwnProperty('lastname') && data.lastname != '') {
          line_1 += (line_1 != '' ? ' ' : '') + data.lastname;
        }
        if (line_1 != '') {
          line_1 = '<h5 class="mb_10">' + line_1 + '</h5>';
        }

        // Line 2
        if (data.hasOwnProperty('address_1') && data.address_1 != '') {
          line_2 += data.address_1 + line;
        }
        if (data.hasOwnProperty('address_2') && data.address_2 != '') {
          line_2 += data.address_2 + line;
        }

        // Line 3
        if (data.hasOwnProperty('subdistrict') && data.subdistrict != '') {
          line_3 += data.subdistrict;
        }
        if (data.hasOwnProperty('district') && data.district != '') {
          line_3 += (line_3 != '' ? ', ' : '') + data.district;
        }
        if (line_3 != '') {
          line_3 += line;
        }

        // Line 4
        if (data.hasOwnProperty('province') && data.province != '') {
          line_4 += data.province;
        }
        if (data.hasOwnProperty('zipcode') && data.zipcode != '') {
          var zipcodeArray = Array.from(data.zipcode)
          var zipcodeHtml = '';

          $.each(zipcodeArray, function(i, v){
            zipcodeHtml += '<span class="square-box">'+v+'</span>';
          });

          $('.postal-code').html(zipcodeHtml);
        } else {
          $('.postal-code').html('');
        }
        if (data.hasOwnProperty('country_name') && data.country_name != '') {
          line_4 += (line_4 != '' ? ' ' : '') + data.country_name;
        }
        if (line_4 != '') {
          line_4 += line;
        }

        temp = line_1 + '<p class="mt_0">' + line_2 + line_3 + line_4 + '</p>';
        return temp;
      };

      $.fn.updateCustomerAddressExample = function() {
        // console.log('update customer address');
        var $customerAddressExample = $('#customerAddressExample');
        // console.log('length : ' + $customerAddressExample.length);
        if ($customerAddressExample.length > 0) {
          $('#customerAddressExample').html($.fn.customerAddressFormat());
        }
      };

      $.Thailand({
        database: '{{ config('data.zipcode_json.' . (getLang() == 'th' ? 'th' : 'en')) }}',
        $search: $('#st-search'),
        onDataFill: function(data) { 
    // callback เมื่อเกิดการ auto complete ขึ้น 
    $('#sub_dist_id').val(parseInt(data.sub_dist_id));
    $('#subdistrict').val(data.subdistrict);
    $('#dist_id').val(parseInt(data.dist_id));
    $('#district').val(data.district);
    $('#prov_id').val(parseInt(data.prov_id));
    $('#province').val(data.province);
    $('#zipcode').val(data.zipcode);

    $.fn.updateCustomerAddressExample();
  }
});

      $(document).on('keydown', '#st-search', function(event) {
        switch (event.keyCode) {
          case 8:
          var str = $(this).val();
          var hasComma = str.indexOf(', ');
          if (hasComma > -1) {
            var strArr = str.split(', ');
            if (strArr.length == 4) {
              $.fn.clearStSearch();
            }
          }
          break;
        }
      });

      $(document).on('change, blur', '#st-search', function(event) {
        var zipcode = $('#zipcode').val();
        if (zipcode == '') {
          $.fn.clearStSearch();
        }
      });

      $('#customerAddressForm').on('keyup', '[name="firstname"], [name="lastname"], [name="address_1"], [name="address_2"], [name="district"], [name="province"], [name="zipcode"]', function(event) {
        $.fn.updateCustomerAddressExample();
      });

      $('#customerAddressForm').on('change', '[name="firstname"], [name="lastname"], [name="country"], [name="address_1"], [name="address_2"], [name="district"], [name="province"], [name="zipcode"], [name="st_search"]', function(event) {
        $.fn.switchForm();
        $.fn.updateCustomerAddressExample();
      });

      $('#customerAddressForm').on('change', '[name="country"]', function() {
        var countryId = $(this).val();
        var countryShortName = 'th';

        if (countryShortNameList[countryId] !== 'undefined') {
          countryShortName = countryShortNameList[countryId];
          if (countryShortName.toLowerCase() == 'uk') {
            countryShortName = 'gb';
          }
        }

        $('#customerAddressForm [name="phone"]').intlTelInput('setCountry', countryShortName.toLowerCase());
      });

      $('#customerAddressForm [name="phone"]').intlTelInput({
        formatOnDisplay: false,
        initialCountry: '{!! $defaultCountry !!}',
        preferredCountries: ['th'],
        hiddenInput: 'full_phone',
      });

      $('#customerAddressForm [name="phone"]').on('open:countrydropdown', function(e) {
        $.blockUI();
      });

      $('#customerAddressForm [name="phone"]').on('close:countrydropdown', function(e) {
        $.unblockUI();
      });

      $.fn.updateCustomerAddressExample();
      $.fn.switchForm();

      var iti = $("#phone").intlTelInput({
      });

      // $(document).on('change', '#phone', function() {
      //   console.log(intlTelInput($(this).val()).getNumber());
      // });

      var customerAddress = {!! json_encode($customerAddress) !!};

      if(customerAddress != '') {
        // $.fn.updateCustomerAddressExample(customerAddress);
        // $('#firstname').val(customerAddress.firstname);
        // $('#lastname').val(customerAddress.lastname);
        // $('#phone').val(customerAddress.phone);
        // $('#email').val(customerAddress.email);
        // $('#st-search').typeahead('val', customerAddress.subdistrict + ', ' + customerAddress.district + ', ' + customerAddress.province + ', ' + customerAddress.zipcode).trigger('change');
        // $('#address_1').val(customerAddress.address_1);
        // $('#address_2').val(customerAddress.address_2);
        // $('#customerAddressExample').val(customerAddress.full_address);        

      }

      $(document).on('submit', '#customerAddressForm', function() {
        $.blockUI({ message: 'กำลังโหลด...' });
      });

      $(document).on('click', '#btnBack', function() {
        var url = "{{ URL::route('customerAddress') }}";
        var storeId = "{{ $storeId }}";
        url = url + "&store_id=" + storeId + "&line_user_id=" + uId;
      });
      
      // console.log(customerAddress);
      
    });
  </script>
  @endsection
  <!-- Initialize Swiper -->


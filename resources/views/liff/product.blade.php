@extends('layouts.master')


@section('content')

<style>
    .gallery-thumbs .swiper-slide-active {
        border: 0;
    }
    .gallery-thumbs .swiper-slide-thumb-active {
        border: 1px solid #2293ea;
        background: #2293ea;
    }
</style>

<div class="header" style="z-index: 980;" uk-sticky="show-on-up: true; animation: uk-animation-slide-top; bottom: #bottom">
    <div class="uk-container uk-container-small">
        <h1>{{ $pdDetails['name'] }}</h1>
        {{-- <a href="{{ URL::route('productItem', ['action' => 'get_product_data', 'channel_id' => $channelId, 'product_id' => $pdDetails['product_id']]) }}" class="header-btn -left icon-btn" title=""><i class="icon-arrow-left icons"></i></a> --}}
    </div>
</div><!-- /header -->

<div class="main-page pb_75">  

    @isset($pdImage)
    <div class="uk-margin-bottom uk-clearfix">
        <div class="swiper-container gallery-top">
            <div class="swiper-wrapper">
                @foreach($pdImage as $index => $arr)
                <div class="swiper-slide">
                    <img src="{{ $arr['image'] }}" class="uk-responsive-width">
                    @if($pdDetails['remain_stock'] < 0)
                    <div class="uk-overlay-default uk-position-cover">
                        <div class="uk-position-center">
                            <span class="text -black -size20">Sold out</span>
                        </div>
                    </div>
                    @endif
                </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>

        <div class="swiper-container gallery-thumbs">
            <div class="swiper-wrapper">
                @foreach($pdImage as $index => $arr)
                <div class="swiper-slide">
                    {{--<a href="javascript:void(0);">--}}
                        <img src="{{ $arr['thumb'] }}" class="uk-responsive-width">
                        @if($pdDetails['remain_stock'] < 0)
                        <div class="uk-overlay-default uk-position-cover">
                            <div class="uk-position-center">
                                <span class="text -black -size12">Sold out</span>
                            </div>
                        </div>
                        @endif
                    {{--</a>--}}
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endisset

    <div class="price-block">
        <div class="uk-container uk-container-small">
            <h2 class="product-name uk-text-bold">{{ $pdDetails['name'] }}</h2>
            <div class="uk-grid-small" uk-grid>
                <div class="uk-width-2-3">
                    <div class="product-price">{{ number_format($pdDetails['price'], 2) }}</div>
                    {{-- <div class="product-price"><del>3,200</del> <ins>THB 3,440</ins></div><!-- /product-price --> --}}
                </div>
                {{-- <div class="uk-width-1-3 uk-text-right">
                    <div class="discount-ribbon uk-text-truncate">SALE -19.80%</div><!-- discount-ribbon -->
                </div> --}}
            </div>

            <div id="test"></div>
            
        </div>
    </div><!-- /price-block -->
    
    <div class="feature-block">
        <div class="uk-container uk-container-small">
            <div class="rte">
                {{ !!$pdDetails['description'] }}
            </div><!-- /rte -->
        </div>
    </div><!-- /feature-block -->


    <div class="action-footer uk-position-bottom uk-position-fixed bg-white line t_basic" >
        <div class="uk-container uk-container-small pt_15 pb_15" >
            <a href="#" uk-toggle="target: #offcanvas-quantity" class="uk-button btn btn-cta uk-width-1-1" id="btn-buy">สั่งซื้อสินค้า</a>    
        </div>
    </div><!-- /action-footer -->
</div>


<div id="offcanvas-share" uk-offcanvas="overlay: true">

</div><!-- /#offcanvas-share -->

<div id="offcanvas-quantity" style="z-index: 9999;" class="quantity-page" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar uk-offcanvas-bottom uk-padding">
        <form class="uk-form-stacked form-custom" id="add-to-cart-form" action="{{ route('addToCart') }}" method="GET" role="form" target="_self">
            @csrf
            <button class="uk-offcanvas-close" type="button" uk-close></button>

            <h3 class="text -black line b_basic uk-text-center mt_5 text -size16 pb_10">ระบุจำนวนและตัวเลือกสินค้า</h3>

            <div class="uk-margin">
                <label class="uk-form-label" for="form-stacked-select">ตัวเลือกสินค้า</label>
                <div class="uk-form-controls">
                    <select class="uk-select" id="form-stacked-select">
                        @foreach($pdDetails['product_variant'] as $index => $value)
                        <option value="{{ $value['product_variant_id'] }}" @if($index == 0) selected @endif>
                            {{ $value['name'] }} (฿ {{ number_format($value['price'], 2) }})
                        </option> 
                        @endforeach
                    </select>
                </div>
            </div>
            <hr>
            <div class="uk-margin">
                <label class="uk-form-label" for="input-amount">จำนวนสินค้า</label>
                <div class="uk-form-controls">
                    <div class="uk-text-center uk-grid-match uk-grid-collapse" uk-grid>
                        <div class="uk-width-1-4 uk-first-column">
                            <div class="value-button uk-button" id="decrease" onclick="decreaseValue()" value="Decrease Value"><i class="fa fa-minus"></i></div>
                        </div>
                        <div class="uk-width-1-2">
                            <input type="number" id="number" name="qty" value="1" min="1" class="uk-input uk-text-center input-number" aria-describedby="number-error" />
                        </div>
                        <div class="uk-width-1-4">
                            <div class="value-button uk-button" id="increase" onclick="increaseValue()" value="Increase Value"><i class="fa fa-plus"></i></div>
                        </div>    
                    </div>
                    <span id="number-error" class="invalid-feedback" style="display: inline;"></span>
                </div>
            </div>
            <hr>
            <input type="hidden" name="product_id" id="product_id" value="{{ $pdDetails['product_id'] }}">
            <input type="hidden" name="line_user_id" id="line_user_id">
            <input type="hidden" name="product_variant_id" id="product_variant_id">
            <input type="hidden" name="store_id" id="store_id" value="{{ $storeData->store_id }}">
            <input type="hidden" name="channel_id" id="channel_id" value="{{ $channelId }}">
            <input type="hidden" name="qty" id="qty">

            <div class="uk-margin-top">
                <button class="uk-button btn btn-cta uk-button-primary uk-width-1-1 uk-margin-small-bottom" type="button" id="addToCart">เพิ่มลงตะกร้าสินค้า</button>

                {{-- <button class="uk-button btn btn-cta uk-button-primary uk-width-1-1 uk-margin-small-bottom" type="button" id="openLiff">open liff</button> --}}
            </div>            
        </form>
        @if(count($errors))
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        {{-- <div id="success_message"></div> --}}
    </div>
</div><!-- /#offcanvas-quantity -->
@endsection
@section('footer')
<!-- Laravel Javascript Validation -->
{!! JsValidator::formRequest('App\Http\Requests\AddToCartRequest', '#add-to-cart-form') !!}
@endsection

@section('liff')
<script src="https://d.line-scdn.net/liff/1.0/sdk.js"></script>
@endsection

@section('script')
<!-- Initialize Swiper -->
<script>
    $(function() {
        // var lineUserId = 'Uefc48a3f3b195367ccb46e03164edc0b';
        // var line = new LineLiff();
        var lineUserId = '';
        liff.init(function (data) {
            // alert('liff init');
            // $('#test').html(data);
            getProfile();
        });

        // liff.init(
        //   data => {// Now you can call LIFF API
        //     const userId = data.context.userId;
            
        // },
        //     err => {
        //         // LIFF initialization failed
        // }
        // );

        // Get profile and display
        function getProfile(){
            // alert('get profile');
            liff.getProfile().then(function (profile) {
                // alert(JSON.stringify(profile));
                lineUserId = profile.userId;
                // alert(lineUserId);
                // Cookies.set('lineUserData', profile, { expires: 1, path: '/', domain: '.ngrok.io' });
                Cookies.set('lineUserData', {data: profile}, { expires: 1, path: '/'});

                // alert(Cookies.getJSON('lineUserData'));

            }).catch(function (error) {
                window.alert("Error getting profile: " + error + " please close this tab and open again");
            });
        }

        $(document).on('change', '#number', function() {
            var value = $(this).val();
            console.log('change value : ' + value);

            if(value < 1) {
                $(this).val(1);
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var galleryThumbs = new Swiper('.gallery-thumbs', {
            direction: 'vertical',
            slidesPerView: 4,
            //slideToClickedSlide: true,
            spaceBetween: 15,
            // loop: true,
            //freeMode: true,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });
        var galleryTop = new Swiper('.gallery-top', {
            slidesPerView: 1,  
            loop: true,
            /*pagination: {
                el: '.swiper-pagination',
            },*/
            thumbs: {
                swiper: galleryThumbs
            }
        });

        $("#form-stacked-select").focusout(function(){
            window.scrollBy(0, 1);
        });

        $(document).on('click', '#addToCart', function() {
            //alert('test');
            addCart();
        });

        function addCart() {
            var qty = $("#number").val();
            var uId = '';
            var lineUserData = Cookies.getJSON('lineUserData');
            // alert(JSON.stringify(lineUserData));
            if (typeof lineUserData !== "undefined") {
                uId = lineUserData.data.userId;
            } else {
                uId = '';
            }
            // alert(uId);
            $('#line_user_id').val(uId);
            $('#product_variant_id').val($('#form-stacked-select').val());
            $('#qty').val(parseInt(qty));

            //$('#line_user_id').val("Uefc48a3f3b195367ccb46e03164edc0b");
                //$('#product_variant_id').val(73589);
            //$('#qty').val(1);

            $('#add-to-cart-form').submit();
        }

        function setDefaultPdItem() {
            $("#form-stacked-select option:first").prop('selected', true);
            $("#number").val(1);
        }

        setDefaultPdItem();

        // $(document).on('click', '#openLiff', function() {
        //     var lineUserData = Cookies.getJSON('lineUserData');
        //     alert(lineUserData.data.userId);
            // alert(Cookies.getJSON('lineUserData')['userId']);

          //   liff.openWindow({
          //     // url:'line://app/1555269657-P0n5A77g',
          //     url:'www.google.co.th',

          //     external: false,
          // });
      // });

  });
</script>
@endsection




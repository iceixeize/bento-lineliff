<?php

return [
	'zipcode_json' => [
		'th' => asset('data/jquery.Thailand.js/db.th.json', isSsl()),
		'en' => asset('data/jquery.Thailand.js/db.en.json', isSsl()),
	],
];
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

// Route::group(['middleware' => 'auth.linelogin'], function() {
Route::get('/product-item', 'BentoLineLiff@productItem')->name('productItem');
// });

Route::get('/test', 'BentoLineLiff@testPageLiff');

Route::match(['get', 'post'], '/linewebhook', 'test@lineWebhook');
// Route::get('/login', function () {
// 	return view('line.login', ['navmenu' => 'home']);
// })->name('linebot.login');
// Route::get('/lineauthen', 'BentoLineLiff@lineAuthen');
// Route::post('/validate-add-to-cart', 'BentoLineLiff@addToCart')->name('addToCart');

Route::group(['middleware' => 'lineliff.validBentoGuest'], function() {
	Route::get('/test-line', 'Test@testLine')->name('test.testLine');
	
	Route::match(['get', 'post'], '/validate-add-to-cart', 'BentoCart@addToCart')->name('addToCart');
	Route::get('/cart', 'BentoCart@cart')->name('bentocart.cart');
	
	Route::post('/add-new-address/', 'BentoCart@addNewAddress')->name('addNewAddress');
	Route::get('/edit-address/', 'BentoCart@editAddress')->name('bentoCart.editAddress');
	Route::post('/submit-edit-address/', 'BentoCart@submitEditAddress')->name('bentoCart.submitEditAddress');
	
	Route::post('/get-cart-info/', 'BentoCart@getCartInfo')->name('bentoCart.getCartInfo');
	Route::get('/set-default-address/', 'BentoCart@setDefaultAddress')->name('setDefaultAddress');
	Route::get('/delete-guest-address', 'BentoCart@deleteGuestAddress')->name('bentoCart.deleteGuestAddress');
	Route::get('/customer-address/', 'BentoCart@customerAddress')->name('customerAddress');
	Route::get('/add-address/', 'BentoCart@addAddress')->name('addAddress');
	Route::post('/cart-checkout/', 'BentoCart@cartCheckOut')->name('bentoCart.cartCheckOut');
});

Route::post('/update-cart/', 'BentoCart@updateCart')->name('bentoCart.updateCart');
Route::post('/delete-cart-item/', 'BentoCart@deleteCartItem')->name('bentoCart.deleteCartItem');
Route::post('/add-payment-to-cart/', 'BentoCart@addPaymentToCart')->name('bentoCart.addPaymentToCart');
Route::post('/add-shipping-to-cart/', 'BentoCart@addShippingToCart')->name('bentoCart.addShippingToCart');


// Route::get('/delete-cart/', 'BentoLineLiff@deleteCart')->name('deleteCart');
// Route::post('/update-cart/', 'BentoLineLiff@updateCart')->name('updateCart');
// Route::get('/page-not-found', 'BentoCart@pageNotFound')->name('bentoCart.pageNotFound');

Route::get('/empty-cart', 'BentoCart@emptyCart')->name('bentoCart.emptyCart');
Route::get('/page-not-found', function () {
	return view('page_not_found');
})->name('pageNotFound');

Route::get('/clear-cart-session', 'BentoCart@clearCartSession')->name('bentoCart.clearCartSession');









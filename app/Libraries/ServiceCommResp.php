<?php

namespace App\Libraries;

class ServiceCommResp {

    private $status = 0;
    private $data = [];

    private $errorMsg = '';
            
    function __construct($params = array()) {
        if (is_string($params)) {
            $params = json_decode($params, true);
        }
        if (! is_array($params)) {
            throw new \Exception('Invalid response');
        }
        $this->initialize($params);

        if (! $this->isSuccess()) {
            if (! empty($this->data['message'])) {
                $this->errorMsg = $this->data['message'];
            }
        }
    }

    public function toArray() {
        $arr = [
            'status' => $this->status,
            'data' => $this->data,
        ];

        return $arr;
    }

    public function toJson() {
        return json_encode($this->toArray());
    }

    public function hasError() {
        return ($this->errorMsg != '');
    }

    public function isSuccess() {
        return ($this->status == 1);
    }

    /**
     * Initialize Preferences
     *
     * @access  public
     * @param   array   initialization parameters
     * @return  void
     */
    function initialize($params) {
        if (count($params) > 0) {
            foreach ($params as $key => $value) {
                if (isset($this->$key)) {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     *
     * @return self
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData($index = '')
    {
        if ($index == '') {
            return $this->data;
        }
        if (isset($this->data[$index])) {
            return $this->data[$index];
        }
        return '';
    }

    /**
     * @param mixed $data
     *
     * @return self
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getErrorMsg()
    {

        return $this->errorMsg;
    }

    /**
     * @param mixed $errorMsg
     *
     * @return self
     */
    public function setErrorMsg($errorMsg)
    {
        $this->errorMsg = $errorMsg;

        return $this;
    }
}

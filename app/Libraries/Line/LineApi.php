<?php

namespace App\Libraries\Line;
//================ LINE BOT SDK ====================
use LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use App\Libraries\Line\FlexSampleRestaurantMenuList;
use App\Libraries\Line\FlexSampleRestaurant;
use App\Libraries\Line\FlexSampleShopping;
use App\Libraries\Line\FlexSampleMessage;
use App\Libraries\Line\FlexBentoInvoiceOrder;
//==================================================

class LineApi
{
	const channelAccessToken = '3oxbEgaEwpFBbXGgIulcTyVc7ri4QqAmkXg+QzBsoIBvLN9cThBUxY+aaoL+a65BgagGbyg3AV0utwbWKdavRJusxwgaeochTHO+whCsy2j5L9poCL6t4zE1mwDuXBg4Oe9cxPk1/aOM+J9uttlVnlGUYhWQfeY8sLGRXgo3xvw=';
	const channelSecret = '9ddd44c36e35de0706bcb3dc3d1ea5a4';

	private $bot;
	public function __construct() {
		$this->initLineBot();
	}

	public function initLineBot() {
		$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(self::channelAccessToken);
		$this->bot = new \LINE\LINEBot($httpClient, ['channelSecret' => self::channelSecret]);
	}


	public function sendReceipt(Request $request) {
		echo 'u s e r';
		$user = $this->getGuest();

		if($user) {
			$test = $this->sendPushMessage($user->social_ref_id);
			dd($test);
			// $sendReceipt = $this->sendFlexReceipt($user->social_ref_id, $itemsList, 'bill no. ' . $bill->bill_no);
		}		
	}

	public function sendPushMessage($uId = '') {
		if(!$uId) {
			return false;
		}

		 // create curl resource 
		$ch = curl_init(); 

        // set url 
		$url = 'https://api.line.me/v2/bot/message/push';

		$data = [
		    "to" => "Uefc48a3f3b195367ccb46e03164edc0b",
		    "messages" => [
		        [
		            "type" => "text",
		            "text" => "Hello, world1"
		        ],
		        [
		            "type" => "text",
		            "text" => "Hello, world2"
		        ]
		    ]
		];
		// $data = array(
		//     'username' => 'tecadmin',
		//     'password' => '012345678'
		// );
		 
		$payload = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
			'Authorization: Bearer ' . self::channelAccessToken,
		];

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec($ch);

		curl_close ($ch);

	}

	public function sendFlexBentoInvoice($userId = '', $items = [], $title = 'default title', $datas = []) {
		$flex = new FlexInvoiceBentoOrder();
		if(empty($items)) {
			return false;
		}
		$flexSample = $flex->getFlexMessage($items, $title, $datas);

            // $flexSample = $flex->get($this->flexParams);
            // \Log::info(['flexSample' => $flexSample]);

		$response = $this->bot->pushMessage($userId, $flexSample);
		$status = $response->isSucceeded();

		\Log::info('[/] send receipt to customer ' . json_encode($response));
		if (!$status) {
			\Log::info('[x] Can\'t send flex message : ' . $response->getHTTPStatus() . ' ' . $response->getRawBody());
		}
	}

	public function test($userId = '', $items = [], $title = 'default title') {
		if(empty($items)) {
			return false;
		}

		$response = $this->bot->pushMessage($userId, $items);
		$status = $response->isSucceeded();

		\Log::info('[/] send receipt to customer ' . json_encode($response));
		if (!$status) {
			\Log::info('[x] Can\'t send flex message : ' . $response->getHTTPStatus() . ' ' . $response->getRawBody());
		}
	}

	}

<?php

/**
 * Copyright 2018 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace App\Libraries\Line;

use LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use LINE\LINEBot\Constant\Flex\ComponentButtonStyle;
use LINE\LINEBot\Constant\Flex\ComponentButtonHeight;
use LINE\LINEBot\Constant\Flex\ComponentAlign;
use LINE\LINEBot\Constant\Flex\ComponentFontSize;
use LINE\LINEBot\Constant\Flex\ComponentFontWeight;
use LINE\LINEBot\Constant\Flex\ComponentGravity;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectMode;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectRatio;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use LINE\LINEBot\Constant\Flex\ComponentLayout;
use LINE\LINEBot\Constant\Flex\ComponentMargin;
use LINE\LINEBot\Constant\Flex\ComponentSpacing;
use LINE\LINEBot\Constant\Flex\ComponentIconSize;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\BoxComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ButtonComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\CarouselContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SeparatorComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\IconComponentBuilder;

use App\Libraries\Line\Object\FlexItemList;

class FlexInvoiceBentoOrder
{
    const textHead = 'Bento Invoice';
    public function getFlexMessage($flexData = [], $altText = '', $datas = []) {
        \Log::info(['datas' => $datas]);
        $arrayFlex = [];
        if(empty($flexData)) {
            return FALSE;
        }

        return FlexMessageBuilder::builder()
        ->setAltText($altText)
        ->setContents(
            BubbleContainerBuilder::builder()
            ->setBody($this->createItemBubble($flexData, $altText, $datas))
            ->setFooter(self::createFooterBlock($datas))
        );
        
    }

    private function createItemBubble($flexData, $altText, $datas)
    {
        $array = [];
        $components = [];

        $separate = SeparatorComponentBuilder::builder()->setMargin(ComponentMargin::MD);

        $head = TextComponentBuilder::builder()
        ->setText($altText)
        ->setWeight(ComponentFontWeight::BOLD)
        ->setSize(ComponentFontSize::XL)
        ->setColor('#00AA33');
        array_push($components, $head);

        if(!empty($datas['created_at'])) {
            $date = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText('Date')
                ->setWeight(ComponentFontWeight::BOLD)
                ->setSize(ComponentFontSize::MD),
                TextComponentBuilder::builder()
                ->setText(\Carbon\Carbon::parse($datas['created_at'])->format('d/m/Y H:i'))
                // ->setText($datas['created_at'])
                ->setFlex(4),
                // SeparatorComponentBuilder::builder()
            ]);
            array_push($components, $date);
            array_push($components, $separate);
            

        }

        foreach ($flexData as $index => $item) {
            
            if(!empty($item)) {
                $itemList = 
                 BoxComponentBuilder::builder()
                ->setLayout(ComponentLayout::VERTICAL)
                ->setMargin(ComponentMargin::MD)
                ->setContents([
                    BoxComponentBuilder::builder()
                    ->setLayout(ComponentLayout::BASELINE)
                    ->setContents([
                        IconComponentBuilder::builder()
                        ->setUrl($item['image'])
                        ->setMargin(ComponentMargin::XL)
                        ->setSize(ComponentIconSize::XXL),
                        TextComponentBuilder::builder()
                        ->setText($item['name'] . ' - ' . $item['pv_name'])
                        ->setWeight(ComponentFontWeight::BOLD)
                        ->setFlex(0)
                        ->setMargin(ComponentMargin::MD),
                    ]),
                    // $separate,
                    BoxComponentBuilder::builder()
                    ->setLayout(ComponentLayout::BASELINE)
                    ->setContents([
                        TextComponentBuilder::builder()
                        ->setText($item['price'] . ' x ' . $item['qty'] . ' = ' . $item['subtotal'] . ' THB')
                        ->setSize(ComponentFontSize::SM)
                        ->setColor('#726E6E')
                        ->setAlign(ComponentAlign::END)
                    ]),
                ]);
                array_push($components, $itemList);
            }


            // if(!empty($item)) {
            //     $amt =  
            //     BoxComponentBuilder::builder()
            //     ->setLayout(ComponentLayout::BASELINE)
            //     ->setContents([
            //         TextComponentBuilder::builder()
            //         ->setText($item['price'] . ' x ' . $item['qty'] . ' = ' . $item['subtotal'] . ' THB')
            //         ->setSize(ComponentFontSize::SM)
            //         ->setColor('#726E6E')
            //         ->setAlign(ComponentAlign::END)
            //     ]);

            //     array_push($components, $amt);
            // }

        }

        return BoxComponentBuilder::builder()
        ->setLayout(ComponentLayout::VERTICAL)
        ->setContents($components);
    }

    private static function createItemBodyBlock($item)
    {
        $components = [];
        $separate = SeparatorComponentBuilder::builder();

        //================ item list ================
        if(!empty($item)) {
            $itemList = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                IconComponentBuilder::builder()
                ->setUrl($item['image'])
                ->setMargin(ComponentMargin::SM)
                ->setSize(ComponentIconSize::XXL),
                TextComponentBuilder::builder()
                ->setText($item['name'] . ' - ' . $item['pv_name'])
                ->setWeight(ComponentFontWeight::BOLD)
                ->setFlex(0)
                ->setMargin(ComponentMargin::MD),
            ]);
            array_push($components, $itemList);
        }


        if(!empty($item)) {
            $amt = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText(moneyFormat($item['price']) . ' x ' . $item['qty'] . ' = ' . moneyFormat($item['subtotal']) . ' THB')
                ->setSize(ComponentFontSize::SM)
                ->setColor('#726E6E')
                ->setAlign(ComponentAlign::END)
            ]);

            array_push($components, $amt);
        }

        array_push($components, $separate);

        return BoxComponentBuilder::builder()
        ->setLayout(ComponentLayout::VERTICAL)
        ->setSpacing(ComponentSpacing::SM)
        ->setContents($components);
    }

    private static function createFooterBlock($item)
    {
        $components = [];
        $separate = SeparatorComponentBuilder::builder();
        $beforeVat = [];
        $vat = [];
        $shipping = [];

        //=============== sub total ===============
        if(isset($item['calculate']['sub_total'])) {
            $totalTemplate = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText('รวม')
                // ->setColor('#AAAAAA')
                ->setWeight(ComponentFontWeight::BOLD)
                ->setMargin(ComponentMargin::SM),
                TextComponentBuilder::builder()
                ->setText(moneyFormat($item['calculate']['sub_total'] + $item['calculate']['shipping_amount']) . ' THB')
                ->setSize(ComponentFontSize::SM)
                ->setColor('#726E6E')
                ->setAlign(ComponentAlign::END)
            ]);
            array_push($components, $separate);
            array_push($components, $totalTemplate);

        }

        //=============== before_vat ===============
        // if(isset($item['calculate']['before_vat'])) {
        //     $beforeVat = BoxComponentBuilder::builder()
        //     ->setLayout(ComponentLayout::BASELINE)
        //     ->setContents([
        //         TextComponentBuilder::builder()
        //         ->setText('ราคาสินค้า')
        //         ->setColor('#AAAAAA')
        //         ->setWeight(ComponentFontWeight::BOLD)
        //         ->setMargin(ComponentMargin::SM),
        //         TextComponentBuilder::builder()
        //         ->setText(moneyFormat($item['calculate']['before_vat']) . ' THB')
        //         ->setSize(ComponentFontSize::SM)
        //         ->setColor('#AAAAAA')
        //         ->setAlign(ComponentAlign::END)
        //     ]);
        //     array_push($components, $beforeVat);

        // }

        // //================ vat =================
        // if(isset($item['calculate']['vat_include']) && $item['calculate']['vat_include'] == 1) {
        //     $vat = BoxComponentBuilder::builder()
        //     ->setLayout(ComponentLayout::BASELINE)
        //     ->setContents([
        //         TextComponentBuilder::builder()
        //         ->setText('Vat ' . $item['calculate']['vat_calculate'] . '%')
        //         ->setColor('#AAAAAA')
        //         ->setWeight(ComponentFontWeight::BOLD)
        //         ->setMargin(ComponentMargin::SM),
        //         TextComponentBuilder::builder()
        //         ->setText(moneyFormat($item['calculate']['vat_amount']) . ' THB')
        //         ->setSize(ComponentFontSize::SM)
        //         ->setColor('#AAAAAA')
        //         ->setAlign(ComponentAlign::END)
        //     ]);
        //     array_push($components, $vat);

        // }

        // //================ shipping =================
        // if(isset($item['calculate']['shipping_amount'])) {
        //     $shipping = BoxComponentBuilder::builder()
        //     ->setLayout(ComponentLayout::BASELINE)
        //     ->setContents([
        //         TextComponentBuilder::builder()
        //         ->setText('ค่าจัดส่ง')
        //         ->setColor('#AAAAAA')
        //         ->setWeight(ComponentFontWeight::BOLD)
        //         ->setMargin(ComponentMargin::SM),
        //         TextComponentBuilder::builder()
        //         ->setText(moneyFormat($item['calculate']['shipping_amount']) . ' THB')
        //         ->setSize(ComponentFontSize::SM)
        //         ->setColor('#AAAAAA')
        //         ->setAlign(ComponentAlign::END)
        //     ]);
        //     array_push($components, $shipping);

        // }


        //============== bill no ==============
        // if(!empty($item['invoice_no'])) {
        //     $date = BoxComponentBuilder::builder()
        //     ->setLayout(ComponentLayout::BASELINE)
        //     ->setContents([
        //         TextComponentBuilder::builder()
        //         ->setText('Invoice no.')
        //         ->setColor('#AAAAAA')
        //         ->setSize(ComponentFontSize::SM)
        //         ->setWeight(ComponentFontWeight::BOLD)
        //         ->setFlex(1),

        //         TextComponentBuilder::builder()
        //         ->setText($item['invoice_no'])
        //         ->setColor('#AAAAAA')
        //         ->setSize(ComponentFontSize::SM)
        //         ->setFlex(0)
        //         ->setAlign(ComponentAlign::END)

        //     ]);
        // }

        //============== btn ==============
        if(!empty($item['invoice_url'])) {
            $url = ButtonComponentBuilder::builder()
            ->setStyle(ComponentButtonStyle::PRIMARY)
            ->setHeight(ComponentButtonHeight::SM)
            ->setColor('#02B04D')
            ->setAction(new UriTemplateActionBuilder('แจ้งชำระเงิน', $item['invoice_url']));
            array_push($components, $url);
        }

        return BoxComponentBuilder::builder()
        ->setLayout(ComponentLayout::VERTICAL)
        ->setSpacing(ComponentSpacing::SM)
        ->setFlex(0)
        ->setContents($components);
    }



}

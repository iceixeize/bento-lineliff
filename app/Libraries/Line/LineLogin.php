<?php

namespace App\Libraries\Line;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Socialite;


class LineLogin
{
	const endPoint = '/token';
	private $redirectUri;
	private $viewName = '';
	private $channelId;
	private $channelSecret; 
	private $code = '';
	private $accessToken = '';

	protected $userData;
    protected $socialData;

	public function __construct() {
		self::init();
	}

    public function init() {
      $this->redirectUri = config('services.line.redirect');
      $this->channelId = config('services.line.client_id');
      $this->channelSecret = config('services.line.client_secret'); 
  }

  public function lineLoginRedirectUri() {
      return Socialite::with('line')->redirect();
  }

  public function handleAuthenLineCallback()
  {
      try {
       $users = Socialite::driver('line')->stateless()->user();
   } catch (Exception $e) {
       return Redirect::to('line/getredirecturi');
   }

   $authUser = $this->findOrCreateUser($user);
}

public function findOrCreateUser($users) {
  $lineUserId = $users->getId();
  $line = \App\Models\Social::where('line_user_id', $lineUserId)->first();
  if(!$line) {
   $newUser = new \App\Models\Social();
   $newUser->line_user_id = $lineUserId;
   $newUser->name = $users->getName();
   $newUser->nickname = $users->getNickname();
   $newUser->email = $users->getEmail();
   $newUser->image_url = $users->getAvatar();
   $newUser->token = $users->token;
   $newUser->refresh_token = $users->refreshToken;
   $convertTimeStamp = Carbon::createFromTimestamp($users->expiresIn)->toDateTimeString();
   $newUser->expire_in = Carbon::parse($convertTimeStamp)->format('Y-m-d H:i:s');
			$newUser->customer_id = 1; // mock
			$newUser->company_id = 1; // mock
			$newUser->save();
			$statusUser = 1;
		} else {
			$statusUser = 2;
		}
	}

	public function loggingOutUsers() {

	}

	private function getAccessToken($code) {
		$array = [
			'grant_type' => 'authorization_code',
			'code' => $code,
			'redirect_uri' => $this->redirectUri,
			'client_id' => $this->channelId,
			'client_secret' => $this->channelSecret,
		];


		\Log::info(['body' => $array]);
         // create curl resource 


		$url = 'https://api.line.me/oauth2/v2.1/token';

		$guzzle = new Client();
		$resp = $guzzle->post($url, [
			'headers' => ['Accept' => 'application/json'],
			'form_params' => $array,
		]);

		return $output;
	}


	public function authen(Request $request) {

		if ($request->has('code') && $request->has('state')) { // ได้ callback มาจาก line
			try {
				$users = Socialite::driver('line')->stateless()->user();
				$this->userData = $users;
			} catch (Exception $e) {
				return Redirect::to('web.home');
			}

			$authUser = $this->findOrCreateUser($users);
			if ($users) {
				session(['lineUserData' => $users, 'line_token' => $users->token]);
				return TRUE;
			}
        } else if (session()->has('line_token')) { // มี session line-token เก็บไว้
        	$lineToken = session('line_token');
        	$users = Socialite::driver('line')->userFromToken($lineToken);
        	$this->userData = $users;
        	session(['lineUserData' => $users, 'line_token' => $users->token]);
        	return TRUE;
        } else {
        	return FALSE;
        }  

    }

    public function index() {
    	return Socialite::with('line')->redirect();
    }



    public static function getEndPoint() {
    	return self::endPoint;
    }


    /**
     * @return mixed
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    /**
     * @param mixed $redirectUri
     *
     * @return self
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getViewName()
    {
        return $this->viewName;
    }

    /**
     * @param mixed $viewName
     *
     * @return self
     */
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param mixed $channelId
     *
     * @return self
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelSecret()
    {
        return $this->channelSecret;
    }

    /**
     * @param mixed $channelSecret
     *
     * @return self
     */
    public function setChannelSecret($channelSecret)
    {
        $this->channelSecret = $channelSecret;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     *
     * @return self
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param mixed $accessToken
     *
     * @return self
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserData()
    {
        return $this->userData;
    }

    /**
     * @param mixed $userData
     *
     * @return self
     */
    public function setUserData($userData)
    {
        $this->userData = $userData;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSocialData()
    {
        return $this->socialData;
    }

    /**
     * @param mixed $socialData
     *
     * @return self
     */
    public function setSocialData($socialData)
    {
        $this->socialData = $socialData;

        return $this;
    }
}
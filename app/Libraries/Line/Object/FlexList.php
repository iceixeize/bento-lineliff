<?php

/**
 * Copyright 2018 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace App\Libraries\Line\Object;

class FlexList
{
    public $photo = '';
    public $name = '';
    public $price = '';
    public $id = '';
    public $time = '';
    public $place = '';
    public $link_detail = '';

    public $amount = '';

    public $bill = '';
    public $restaurant = '';
    public $menu = '';
    public $total = '';

    const defaultImage = 'https://66.media.tumblr.com/efe950c1b27b0c1e2cc6dbf81a773afe/tumblr_p53lkfMfac1w9xu55o2_400.jpg';
    public function __construct() 
    {
        // if(!empty($params)) {
        //     $this->setAttribute($params);
        // }
    }

    private function setAttribute($params) {
        foreach ($params as $key => $value) {
                \Log::info('error', ['keys' => $key]);

            if(isset($this->$key)) {
                $this->$key = $value;
            }


        }
    }

    public function getAttributeFlexList($params) {
        \Log::info('error', ['FlexList' => $params]);

        if(!empty($params)) {
            $this->setAttribute($params);
        }

        $params = [
            'photo' => empty($this->photo) ? self::defaultImage : $this->photo,
            'name' => $this->name,
            'price' => $this->price,
            'id' => $this->id,
            'time' => $this->time,
            'place' => $this->place,
            'link_detail' => $this->link_detail,
            'amount' => $this->amount,
            'bill' => $this->bill,
            'restaurant' => $this->restaurant,
            'menu' => $this->menu,
        ];
        \Log::info('error', ['paramsTotal' => $this->total]);
        if(!is_null($this->total)) {
            $params['total'] = $this->total;
        }

        return $params;

        
    }




    /**
     * @return mixed
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param mixed $photo
     *
     * @return self
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     *
     * @return self
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     *
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     *
     * @return self
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLinkDetail()
    {
        return $this->link_detail;
    }

    /**
     * @param mixed $link_detail
     *
     * @return self
     */
    public function setLinkDetail($link_detail)
    {
        $this->link_detail = $link_detail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     *
     * @return self
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * @param mixed $bill
     *
     * @return self
     */
    public function setBill($bill)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * @param mixed $restaurant
     *
     * @return self
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param mixed $menu
     *
     * @return self
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;

        return $this;
    }
}

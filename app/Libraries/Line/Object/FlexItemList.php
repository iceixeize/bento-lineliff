<?php

/**
 * Copyright 2018 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace App\Libraries\Line\Object;

class FlexItemList
{
    public $image = '';
    public $name = '';
    public $pv_name = '';
    public $price = '';
    public $subtotal = '';
    public $cart_total = 0;
    public $qty = '';

    public $bill_no = '';

    const defaultImage = 'https://66.media.tumblr.com/efe950c1b27b0c1e2cc6dbf81a773afe/tumblr_p53lkfMfac1w9xu55o2_400.jpg';
    public function __construct() 
    {
        // if(!empty($params)) {
        //     $this->setAttribute($params);
        // }
    }

    private function setAttribute($params) {
        foreach ($params as $key => $value) {
                // \Log::info('error', ['keys' => $key]);

            if(isset($this->$key)) {
                $this->$key = $value;
            }


        }
    }

    public function getAttributeFlexList($params) {
        // \Log::info('error', ['FlexList' => $params]);
// dd($params);
        if(!empty($params)) {
            $this->setAttribute($params);
        }

        $params = [
            'image' => empty($this->image) ? self::defaultImage : $this->image,
            'name' => $this->name,
            'pv_name' => $this->pv_name,
            'price' => $this->price,
            'subtotal' => $this->subtotal,
            'qty' => $this->qty,
            'sku' => !empty($this->sku) ? '(' . $this->sku . ')' : '',
            'bill_no' => $this->bill_no,

        ];
        // \Log::info('error', ['paramsTotal' => $this->total]);
        if(!is_null($this->cart_total)) {
            $params['cart_total'] = $this->cart_total;
        }

        return $params;

        
    }




  
}

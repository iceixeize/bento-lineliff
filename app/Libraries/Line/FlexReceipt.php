<?php

/**
 * Copyright 2018 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

namespace App\Libraries\Line;

use LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use LINE\LINEBot\Constant\Flex\ComponentButtonStyle;
use LINE\LINEBot\Constant\Flex\ComponentAlign;
use LINE\LINEBot\Constant\Flex\ComponentFontSize;
use LINE\LINEBot\Constant\Flex\ComponentFontWeight;
use LINE\LINEBot\Constant\Flex\ComponentGravity;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectMode;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectRatio;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use LINE\LINEBot\Constant\Flex\ComponentLayout;
use LINE\LINEBot\Constant\Flex\ComponentMargin;
use LINE\LINEBot\Constant\Flex\ComponentSpacing;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\BoxComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ButtonComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\CarouselContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SeparatorComponentBuilder;

use App\Libraries\Line\Object\FlexList;

class FlexReceipt
{
    public function getFlexMessage($flexData = [], $altText = '', $loadMore = '', $id = '', $footerBtnVal = '') {
        $arrayFlex = [];
        $total = 0;
        $sumTotal = 0;
        if(empty($flexData)) {
            return FALSE;
        }
        foreach ($flexData as $index => $value) {
            $flex = new FlexList();
            $total += $value['price'] * $value['amount'];

            if((count($flexData) - 1) == $index) {
                $value['total'] = $total;
            }
            $arrayFlex[] = $this->createItemBubble($flex->getAttributeFlexList($value));
        }

        return FlexMessageBuilder::builder()
        ->setAltText($altText)
        ->setContents(new CarouselContainerBuilder(
         $arrayFlex
     ));
        
    }


    private function createItemBubble($item)
    {
        return BubbleContainerBuilder::builder()
        ->setBody(self::createItemBodyBlock($item));
    }

    private static function createItemBodyBlock($item)
    {
        $components = [];
        $separate = SeparatorComponentBuilder::builder();
        $components[] =  TextComponentBuilder::builder()
        ->setText('Receipt')
        ->setWeight(ComponentFontWeight::BOLD)
        ->setSize(ComponentFontSize::XXL)
        ->setColor('#00AA33');

        if(!empty($item['bill']['created_at'])) {
            $date = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText('Date')
                ->setWeight(ComponentFontWeight::BOLD)
                ->setSize(ComponentFontSize::MD),
                TextComponentBuilder::builder()
                ->setText(\Carbon\Carbon::parse($item['bill']['created_at'])->format('d/m/Y H:i'))
                ->setFlex(4)
            ]);
            array_push($components, $date);
        }

        array_push($components, $separate);

        if(!empty($item)) {
            $menuList = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText($item['restaurant']['restaurant_name'])
                ->setWeight(ComponentFontWeight::BOLD)
                ->setFlex(0)
                ->setMargin(ComponentMargin::SM),
                TextComponentBuilder::builder()
                ->setText($item['menu']['name'])
                ->setMargin(ComponentMargin::MD),
            ]);
            array_push($components, $menuList);
        }



        if(!empty($item)) {
            $amt = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText($item['price'] . ' x ' . $item['amount'] . ' = ' . ($item['price'] * $item['amount']) . ' THB')
                ->setSize(ComponentFontSize::SM)
                ->setColor('#726E6E')
                ->setAlign(ComponentAlign::END)
            ]);

            array_push($components, $amt);
        }

        array_push($components, $separate);

        if(isset($item['total'])) {
            $totalTemplate = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText('รวม')
                ->setWeight(ComponentFontWeight::BOLD)
                ->setMargin(ComponentMargin::SM),
                TextComponentBuilder::builder()
                ->setText($item['total'] . ' THB')
                ->setSize(ComponentFontSize::SM)
                ->setColor('#726E6E')
                ->setAlign(ComponentAlign::END)
            ]);
            array_push($components, $totalTemplate);
            array_push($components, $separate);
        }



        if(!empty($item['bill'])) {
            $date = BoxComponentBuilder::builder()
            ->setLayout(ComponentLayout::BASELINE)
            ->setContents([
                TextComponentBuilder::builder()
                ->setText('Bill no.')
                ->setColor('#AAAAAA')
                ->setSize(ComponentFontSize::SM)
                ->setWeight(ComponentFontWeight::BOLD)
                ->setFlex(1),

                TextComponentBuilder::builder()
                ->setText($item['bill']['bill_no'])
                ->setColor('#AAAAAA')
                ->setSize(ComponentFontSize::SM)
                ->setFlex(0)
                ->setAlign(ComponentAlign::END)

            ]);
            array_push($components, $date);
        }


        return BoxComponentBuilder::builder()
        ->setLayout(ComponentLayout::VERTICAL)
        ->setSpacing(ComponentSpacing::SM)
        ->setContents($components);
    }



}

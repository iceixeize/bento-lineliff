<?php

namespace App\Libraries\Bento;

use Illuminate\Http\Request;

//------------- USE GUZZLE ----------------
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Cache;

class Bento
{
    protected $defaultCurlOption = [
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_SSL_VERIFYHOST => 2,
        CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2,
        CURLOPT_FAILONERROR => false,
    ];

    protected $client;

    public function __construct() {
        $this->initDefaultCurlOption();
        $this->initClient();
    }


    private function initClient() {
        $this->client = new Client([
            'config' => [
                'curl' => $this->defaultCurlOption
            ]
        ]);
    }

    private function initDefaultCurlOption() {
        if (in_array(env('ENV_NAME'), array('development', 'testing'))) {
            $this->defaultCurlOption = [
                CURLOPT_SSL_VERIFYPEER => false,
            ];
            
            switch (env('ENV_NAME')) {
                case 'development':
                    // $this->defaultCurlOption[CURLOPT_CAINFO] = realpath('./cacert.pem');
                break;
                case 'testing':
                $this->defaultCurlOption = [
                    CURLOPT_SSL_VERIFYPEER => true,
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2,
                ];
                default:
                break;
            }
        }
    }
}

<?php 

namespace App\Libraries\Bento;
use Illuminate\Support\Facades\Cache;
use App\Libraries\Servicecomm;
class BentoApi extends BentoAuth {
    private $bentoAccessToken = '';
    private $jwtConfig = '';
    protected $response = [];
    protected $responseData = '';


    protected $errorMessage = '';
    public function __construct($params = []) {
        parent::__construct();
        // $this->initialize($params);
    }

    //==================== GUEST ==========================
    public function getGuest($params = []) {

        if(empty($params['guest_id'])) {
            return false;
        }
        $serviceComm = new ServiceComm(['service' => 'queue']);
        $res = $serviceComm->get('api/guest/' . $params['guest_id']);
        return $this->getResponse($res, 'guest');
    }

    public function getGuestAddress($params = []) {
        if(empty($params['guest_id'])) {
            return false;
        }
        $serviceComm = new ServiceComm(['service' => 'queue']);
        $res = $serviceComm->get('api/guest-address/guest/' . $params['guest_id']);
        return $this->getResponse($res, 'address');
    }

    public function getGuestAddressByGuestAddressId($params = []) {
        if(empty($params['guest_address_id'])) {
            return false;
        }
        $serviceComm = new ServiceComm(['service' => 'queue']);
        $res = $serviceComm->get('api/guest-address/' . $params['guest_address_id']);
        return $this->getResponse($res, 'address');
    }

    public function addGuest($params = []) {
        if(empty($params['firstname']) || empty($params['lastname']) || empty($params['email']) || empty($params['phone']) || empty($params['store_id'])) {
            return false;
        }

        $serviceComm = new ServiceComm(['service' => 'queue']);
        $parameters = [
            'firstname' => $params['firstname'], 
            'lastname' => $params['lastname'], 
            'email' => $params['email'], 
            'phone' => $params['phone'], 
            'store_id' => $params['store_id'],

        ];
        $res = $serviceComm->post('api/guest', $parameters);
        return $this->getResponse($res, 'guest_id');
    }

    public function addGuestAddress($params = []) {
        if(empty($params['firstname']) ||
         empty($params['lastname']) || 
         empty($params['email']) || 
         empty($params['phone']) || 
         empty($params['guest_id']) || 
         empty($params['country']) || 
         empty($params['zipcode']) || 
         empty($params['address_1']) ||
         !isset($params['is_store_create']) ||
         !isset($params['default'])
     ) {
            return false;
    }


    if($params['country'] == 5) {
        if(empty($params['prov_id']) ||
            empty($params['dist_id']) ||
            empty($params['sub_dist_id']) 
        ) {
            return false;
    }
} else {
            // if(empty($params['prov_name'])) ||
}
$serviceComm = new ServiceComm(['service' => 'queue']);
$parameters = [
    'firstname' => $params['firstname'], 
    'lastname' => $params['lastname'], 
    'email' => $params['email'], 
    'phone' => phoneReplaceZero($params['full_phone']), 
    'guest_id' => $params['guest_id'],
    'country_id' => $params['country'],
    'zipcode' => $params['zipcode'],
    'address_1' => $params['address_1'],
    'address_2' => $params['address_2'],
    'prov_id' => $params['prov_id'],
    'dist_id' => $params['dist_id'],
    'sub_dist_id' => $params['sub_dist_id'],
    'is_store_create' => $params['is_store_create'],
    'default' => $params['default'],
];

\Log::info('error', ['paramsAfter' => $parameters]);

$res = $serviceComm->post('api/guest-address', $parameters);
return $this->getResponse($res, 'guest_address_id');
}

public function updateGuestAddress($params = []) {
    if(empty($params['firstname']) ||
     empty($params['lastname']) || 
     empty($params['email']) || 
     empty($params['phone']) || 
     empty($params['guest_id']) || 
     empty($params['country_id']) || 
     empty($params['zipcode']) || 
     empty($params['address_1']) ||
     !isset($params['is_store_create']) ||
     !isset($params['default'])
 ) {
        return false;
}


if($params['country_id'] == 5) {
    if(empty($params['prov_id']) ||
        empty($params['dist_id']) ||
        empty($params['sub_dist_id']) 
    ) {
        return false;
}
} else {
            // if(empty($params['prov_name'])) ||
}
$serviceComm = new ServiceComm(['service' => 'queue']);
$parameters = [
    'firstname' => $params['firstname'], 
    'lastname' => $params['lastname'], 
    'email' => $params['email'], 
    'phone' => $params['phone'], 
    'guest_id' => $params['guest_id'],
    'country_id' => $params['country_id'],
    'zipcode' => $params['zipcode'],
    'address_1' => $params['address_1'],
    'address_2' => $params['address_2'],
    'prov_id' => $params['prov_id'],
    'dist_id' => $params['dist_id'],
    'sub_dist_id' => $params['sub_dist_id'],
    'is_store_create' => $params['is_store_create'],
    'default' => $params['default'],
];

\Log::info('--------------------- u p d a t e G u e s t A d d r e s s ----------------------');
\Log::info(['paramsAfter' => $parameters]);
\Log::info('api/guest-address/' . $params['guest_address_id']);

$res = $serviceComm->put('api/guest-address/' . $params['guest_address_id'], $parameters);
\Log::info(['res' => $res]);
\Log::info('------------------------------------------------------------------------');

return $res->getStatus();
}

    //===================== CART ===========================
public function createCart($params = []) {
    if(empty($params['store_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['store_id' => $params['store_id']];
    $res = $serviceComm->post('cart-api', $parameters);
    return $this->getResponse($res, 'cart_id');
}

public function deleteCartItem($params = []) {
    if(empty($params['item_code']) || empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id' => $params['cart_id'], 'item_code' => $params['item_code']];
    $res = $serviceComm->delete('cart-api/product/' . $params['item_code'], $parameters);
    return $res->getStatus();
}

public function updateCart($params = []) {
    if(empty($params['item_code']) || ($params['qty'] < 0) || empty($params['cart_id'])) {
        $this->errorMessage = 'ข้อมูลไม่ถูกต้อง กรุณาลองอีกครั้ง';
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id' => $params['cart_id'], 'qty' => $params['qty']];
    $res = $serviceComm->put('cart-api/product/' . $params['item_code'], $parameters);

    return $res->getStatus();
}

public function cartCheckOut($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id' => $params['cart_id'], 'langs' => app()->getLocale()];
    $res = $serviceComm->post('cart-api/checkout', $parameters);
    \Log::info(['response_checkout' => $res]);
    $status = $res->getStatus();
    if(!$status) {
        $this->setErrorMessage($res->getErrorMsg());
    } else {
        $this->setResponseData($res->getData());
    }
    return $status;
}

public function addGuestAddressToCart($params = []) {
    if(empty($params['cart_id']) || empty($params['guest_address_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id' => $params['cart_id'], 'guest_address_id' => $params['guest_address_id']];

    $res = $serviceComm->post('cart-api/guest-address', $parameters);
    return $res->getStatus();
}

public function deleteGuestAddress($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id' => $params['cart_id']];
    $res = $serviceComm->delete('/cart-api/guest-address', $parameters);
    return $this->getResponse($res, 'cart_id');
}

public function getCartInfo($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->get('cart-api/' . $params['cart_id']);
    return $this->getResponse($res, 'cart');
}

public function addPaymentMethodToCart($params = []) {
    if(empty($params['cart_id']) || empty($params['store_id']) || empty($params['payment_key']) 
        // || empty($params['guest_address_id'])
) {
        return false;
}

$serviceComm = new ServiceComm(['service' => 'order']);
$parameters = [
    'cart_id' => $params['cart_id'], 
    'store_id' => $params['store_id'], 
    'payment_key' => $params['payment_key'], 
        // 'guest_address_id' => $params['guest_address_id']
];
$res = $serviceComm->post('cart-api/payment', $params);
\Log::info(['addPaymentMethodToCart' => $res]);
return $res->getStatus();
}

public function addShippingMethodToCart($params = []) {
    if(empty($params['cart_id']) || empty($params['store_id']) || empty($params['shipping_key']) || empty($params['guest_address_id'])
) {
        return false;
}
$serviceComm = new ServiceComm(['service' => 'order']);
$parameters = ['cart_id' => $params['cart_id'], 'store_id' => $params['store_id'], 'shipping_key' => $params['shipping_key'], 
'guest_address_id' => $params['guest_address_id']
];

$res = $serviceComm->post('cart-api/shipping', $params);
\Log::info(['addShippingMethodToCart' => $res]);
return $res->getStatus();
}

public function deleteShippingMethod($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }
    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->delete('cart-api/shipping', $params);
}

public function deletePaymentMethod($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }
    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->delete('cart-api/payment', $params);
}

public function calculateCart($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->post('cart-api/calculate', ['cart_id' => $params['cart_id'], 'langs' => app()->getLocale()]);
    return $this->getResponse($res);
}

public function checkOut($params = []) {
    if(empty($params['cart_id'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->post('/cart-api/checkout', ['cart_id' => $params['cart_id']]);
    return $this->getResponse($res, 'cart');
}

public function addPdItemToCart($params = []) {
    if(empty($params['cart_id']) || empty($params['product_variant_id']) || empty($params['qty'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'order']);
    $parameters = ['cart_id'=> $params['cart_id'], 'product_variant_id' => $params['product_variant_id'], 'qty' => $params['qty']];
    $res = $serviceComm->post('cart-api/product', $parameters);

    if($res->getStatus() == 0) {
        \Log::error('------ CART ID : ' . $params['cart_id'] . ' -------');
        \Log::error('[x] Can\'t add pd to cart params: ' . json_encode($parameters) . ' res: ' . json_encode($res->getStatus()) . ' mes: ' . json_encode($res->getErrorMsg()));
        // session()->forget('cart_id_' . $params['cart_id']);
        // \Log::error('------ SESSION CART : ' . $params['cart_id'] . ' -------' . json_encode($request->session()->get('cart_id_' . $params['cart_id'])));
    }

    return $this->getResponse($res, 'item');
}

public function prepareCart($params) {
    if(empty($params['store_id']) || empty($params['langs']) || empty($params['cart'])) {
        return false;
    } 

    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->post('cart/prepare_data', $params);
    return $this->getResponse($res, 'cart');

}

public function calculate($params) {
    if(empty($params['store_id']) || empty($params['langs']) || empty($params['cart'])) {
        return false;
    } 

    $serviceComm = new ServiceComm(['service' => 'order']);
    $res = $serviceComm->post('cart/calculate', $params);
    return $this->getResponse($res);

}

    //===================== CATEGORY =======================
public function getCategoryByStoreId($params) {
    if(empty($params['storeId'])) {
        return false;
    }

    $serviceComm = new ServiceComm(['service' => 'queue']);
    $res = $serviceComm->get('api/category/stores/' . $params['storeId']);
}

    //===================== PRODUCT =========================
public function getProductImage($params) {
    if(empty($params['productId'])) {
        return false;
    } 

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $res = $servicecomm->get('api/product-image/product/' . $params['productId']);
    return $this->getResponse($res, 'product');
}

public function getResponse($resp, $key = '') {
        // check ก่อนว่าเป็น instance object ServiceCommResp มั้ย
    if($resp->getStatus() == true) {
        if(!empty($key)) {
            return $resp->getData($key);
        } else {
            return $resp->getData();
        }
    }
    // \Log::info('error', '[x]-------- Bento Api return false -------- Res. => ' . json_encode($resp));

    return false;
}

public function getProductDetail($params) {
    if(empty($params['productId'])) {
        return false;
    } 

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $res = $servicecomm->get('api/product/' . $params['productId']);
    return $this->getResponse($res, 'product');
}
    //===================== CUSTOMER ==========================
public function getCustomerListByCustomerId($params) {
    if(empty($params['customerId'])) {
        return false;
    }

    $parameters = [];
    if(isset($params['store_id'])) {
        $parameters['store_id'] = $params['store_id'];
    }

    if(isset($params['is_store_create'])) {
        $parameters['is_store_create'] = $params['is_store_create'];
    }

    $serviceComm = new ServiceComm(['service' => 'queue']);
    $res = $serviceComm->get('customer-address/customer/' . $params['customerId']);
}

public function getCustomerByStoreId($params) {
    if(empty($params['storeId'])) {
        return false;
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $data = $servicecomm->get('api/customer/pos/' . $storeId);
}


public function getCustomerByEmail($params) {
        //OAUTH
    if(empty($params['email']) && empty($params['storeId'])) {
        return false;
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $data = $servicecomm->post('api/customer/check-email', $params);
}

    //===================== GUEST ========================
public function createGuest($params) {
    if(empty($params['firstname']) || 
        empty($params['lastname']) ||
        empty($params['email']) ||
        empty($params['phone']) ||
        empty($params['store_id'])
    ) {
        return false;
}
$servicecomm = new ServiceComm(['service' => 'queue']);
$data = $servicecomm->post('api/guest', $params);

}

public function getGuestAddressListByGuestId($params = []) {
    if(empty($params['guestId'])) {
        return false;
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $res = $servicecomm->get('api/guest-address/guest/' . $params['guestId']);
}    

    //===================== CUSTOMER ADDRESS ==========================
public function getCustomerByCustomerId($params) {

    if(empty($params['customerId'])) {
        return false;
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $data = $servicecomm->get('api/customer-address/customer/' . $params['customerId']);
}

public function getCustomerAddressListByCustomerAddressId($params) {
    if(empty($params['customerAddressId'])) {
        return false;
    }

        // lang (opt)

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $data = $servicecomm->get('api/customer-address/', $params['customerAddressId']);
}


    //=================== SHIPPING ====================
public function getStoreShippingByStoreId($params) {
    if(empty($params['store_id'])) {
        return false;
    }

        // เลือกอย่างใดอย่างนึง
        // customer_address_id - (Req)
        // OR

        // guest_address_id = (Req)
        // OR

        // order_id = (Req)

    $parameters = [];

    if(isset($params['weight_total'])) {
        $parameters['weight_total'] = $params['weight_total'];
    }

    if(isset($params['is_anonymous'])) {
        $parameters['is_anonymous'] = $params['is_anonymous'];
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $res = $servicecomm->get('api/store/shipping/' . $params['store_id'], $parameters);
    \Log::info(['storeShipping' => $res]);

    return $this->getResponse($res, 'shipping_method');
}

    //===================== PAYMENT ===================
public function getStorePaymentByStoreId($params) {
    if(empty($params['store_id']) || empty($params['grand_total'])) {
        return false;
    }

    $servicecomm = new ServiceComm(['service' => 'queue']);
    $parameters = [
            'grand_total' => $params['grand_total'], // เพื่อหาว่าจ่าย payment ไหนได้บ้าง
            // 'shipping_key' => //opt (เอาไว้กรอง payment ที่ผูกกับ shipping key)
            'is_anonymous' => 0,
            'by_pass' => 1
        ];
        $res = $servicecomm->get('api/store/payment/' . $params['store_id'], $parameters);
        \Log::info(['storePayment' => $res]);
        return $this->getResponse($res);
    }


    //===================== ADDRESS ========================
    public function getCountry() {
        $serviceComm = new ServiceComm(['service' => 'queue']);
        $parameters = ['langs' => 'th'];
        $res = $serviceComm->get('api/country', $parameters);

        return $this->getResponse($res, 'country_list');
    }

    public function deleteGuestAddressByGuestAddressId($params = []) {
        if(empty($params['guest_address_id'])) {
            return false;
        }

        $serviceComm = new ServiceComm(['service' => 'queue']);
        $res = $serviceComm->delete('api/guest-address/' . $params['guest_address_id']);
        return $res->getStatus();
    }


    /**
     * @return mixed
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param mixed $errorMessage
     *
     * @return self
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseData()
    {
        return $this->responseData;
    }

    /**
     * @param mixed $responseData
     *
     * @return self
     */
    public function setResponseData($responseData)
    {
        $this->responseData = $responseData;

        return $this;
    }
}
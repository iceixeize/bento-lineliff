<?php 

namespace App\Libraries\Bento;

class BentoToken extends Bento {
	private $token_type = '';
	private $expires_in = '';
	private $access_token = '';
	private $refresh_token = '';

	public function __construct($params = []) {
		$this->initialize($params);
	}

	/**
	 * Initialize Preferences
	 *
	 * @access  public
	 * @param   array   initialization parameters
	 * @return  void
	 */
	public function initialize($params) {
		// $this->clear();
		if (count($params) > 0) {
			foreach ($params as $key => $value) {
				$this->$key = $value;
			}
		}
	}

	public function __toString() {
		return $this->getAccessToken();
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken()
	{
		return $this->access_token;
	}

	/**
	 * @param mixed $token
	 *
	 * @return self
	 */
	public function setAccessToken($accessToken)
	{
		$this->accessToken = $accessToken;

		return $this;
	}

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
    	return $this->expires_in;
    }

    /**
     * @param mixed $expires_in
     *
     * @return self
     */
    public function setExpiresIn($expires_in)
    {
    	$this->expires_in = $expires_in;
    	return $this;
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->token_type;
    }

    /**
     * @param mixed $token_type
     *
     * @return self
     */
    public function setTokenType($token_type)
    {
        $this->token_type = $token_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->refresh_token;
    }

    /**
     * @param mixed $refresh_token
     *
     * @return self
     */
    public function setRefreshToken($refresh_token)
    {
        $this->refresh_token = $refresh_token;

        return $this;
    }
}
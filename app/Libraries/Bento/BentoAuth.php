<?php

namespace App\Libraries\Bento;

use Illuminate\Http\Request;

//------------- USE GUZZLE ----------------
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Cache;

class BentoAuth extends Bento
{
    private $grant_type = '';
    private $client_id = '';
    private $client_secret = '';
    private $username = '';
    private $password = '';

    private $accessToken = null;
    const apiUrl = 'https://login.bento-sandbox.com/oauth/token';
    const methodType = 'POST';
    const contentType = 'application/x-www-form-urlencoded';
    public function __construct() {
        parent::__construct();
        $this->initParams();
    }

    public function getAccessTokenObj() {
        if (!(Cache::has('bentoToken'))) {
            $this->authen();
            return $this->accessToken;
        } else {
            $this->accessToken = new BentoToken(Cache::get('bentoToken'));
            return $this->accessToken;
        }
    }

    private function initParams() {
        $this->grantType = env('BENTO_API_GRANTTYPE');
        $this->clientId = env('BENTO_API_CLIENT_ID');
        $this->clientSecret = env('BENTO_API_CLIENT_SECRET');
        $this->username = env('BENTO_API_EMAIL');
        $this->password = env('BENTO_API_PASSWORD');

    }

    private function authen() {
        $params = [
           'grant_type' => $this->grantType,
           'client_id' => $this->clientId,
           'client_secret' => $this->clientSecret,
           'username' => $this->username,
           'password' => $this->password,
       ];
       $res = $this->client->request(self::methodType, self::apiUrl, [
        'headers' => [
            'Content-Type' => self::contentType,
        ],

        'form_params' => $params,
    ]);

       $res = json_decode($res->getBody()->getContents(), true);
       if(!$res) {
        \Log::info('error', '[x] Can\'t authen bento login ' . json_encode($params) . ' res : ' . json_encode($res));
        return false;
    } else {
        Cache::put('bentoToken', $res, ($res['expires_in'] - 180));
        $this->accessToken = new BentoToken($res);
        return true;
    }
}
}

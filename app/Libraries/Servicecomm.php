<?php

namespace App\Libraries;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Client;
use Exception;

use Cache;

class ServiceComm {

    private $client;
    private $service;
    private $clientId = '';
    private $secret = '';
    private $token = '';
    private $enableRetry = true;
    private $maxRetry = 3;
    private $baseUrl = '';
    private $availableService = ['chat', 'es', 'login', 'queue', 'order'];
    private $url = ''; // Customer Url


    const CACHE_TTL = 5;

    function __construct($config = array()) {
        $this->initialize($config);

        if ($this->clientId == '') {
            $this->clientId = env('BENTO_API_CLIENT_ID');
        }

        if ($this->secret == '') {
            $this->secret = env('BENTO_API_CLIENT_SECRET');
        }

        if ($this->baseUrl == '') {
            $this->setBaseUrl(env('BENTO_API_URL'));
            // dd($this->baseUrl);
        }
        $resp = $this->setService($config['service']);
        if ($resp === false) {
            throw new Exception('Service not available');
        }

        if ($this->url != '' && substr($this->url, -1) != '/') {
            $this->url = $this->url . '/';
        }

// dd($this->getService());
        $this->client = new Client(['base_uri' => $this->getService(), 'verify' => false]);
    }

    

    public function setService($service) {
        if (in_array($service, $this->availableService)) {
            $this->service = getHttpProtocol() . '://' . $service . '.' . $this->baseUrl  . '/jwt/';
            return true;
        } else {
            return false;
        }
    }

    public function connect() {
        // \Debugbar::startMeasure('----- ServiceComm connect -----', '');
        /*$key = static::SEVICE_COMM_CACHE_FIND_TAG . $this->clientId;

        $tokenCache = Cache::tags([
            static::SEVICE_COMM_CACHE_ALL_TAG
        ])->get($key);

        if (! is_null($tokenCache)) {
            $this->setToken($tokenCache);
        }*/

        if ($this->token == '') {
            $params['client_id'] = $this->clientId;
            $params['secret'] = $this->secret;
            $response = $this->client->get($this->getService() . $this->getAuthEndpoint($params));
            $response = json_decode($response->getBody(), true);
            if (isset($response['token'])) {
                $this->setToken($response['token']);
                
                /*Cache::tags([
                    static::SEVICE_COMM_CACHE_ALL_TAG
                ])->put($key, $response['token'], static::CACHE_TTL);*/
            }
        }
        // \Debugbar::stopMeasure('----- ServiceComm connect -----', '');

        return $this;
    }

    public function getToken() {
        return $this->token;
    }
    
    public function setToken($token) {
        $this->token = $token;
    }

    private function reconnect() {
        $retry = 0;
        if ($this->enableRetry) {
            while ($this->token == '' && $retry < $this->maxRetry) {
                $this->connect();

                $retry++;
            }
        } else {
            $this->connect();
        }
    }

    public function get($endpoint, $params = array()) {
        $this->reconnect();
        $url = '';
        
        $params['token'] = $this->token;
        
        if ($this->service != '') {
            $url = $this->service . $endpoint;
        } else if ($this->url != '') {
            $url = $this->url;
        }
        // \Debugbar::startMeasure('ServiceComm get: ' . $url, '');
        $option = $this->getCurlOption();
        $request = new Request('GET', $url . '?' . http_build_query($params));
        $config = [
            'config' => [
                'curl' => $option
            ]
        ];
        $response = $this->client->send($request, $config);
        // \Debugbar::stopMeasure('ServiceComm get: ' . $url, '');

        if ($this->isValidHeader($response)) {
            return $this->getResponse($response);
        } else if ($this->enableRetry) {
            return $this->retry($request, $config);
        }

        return false;
    }

    public function post($endpoint, $params) {
        $this->reconnect();
        $url = '';
        
        $option = $this->getCurlOption();
        if ($this->service != '') {
            $url = $this->service . $endpoint . '?token=' . $this->token;
        } else if ($this->url != '') {
            $url = $this->url . $endpoint . '?token=' . $this->token;
        }
        // \Debugbar::startMeasure('ServiceComm post: ' . $url, '');
        $request = new Request('POST', $url, [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json'
                ], http_build_query($params, null, '&'));
        $config = [
            'config' => [
                'curl' => $option
            ]
        ];

        $response = $this->client->send($request, $config);
        // \Debugbar::stopMeasure('ServiceComm post: ' . $url, '');

        if ($this->isValidHeader($response)) {
            return $this->getResponse($response);
        } else if ($this->enableRetry) {
            return $this->retry($request, $config);
        }
        return false;
    }

    public function put($endpoint, $params) {
        $this->reconnect();
        $url = '';
        
        $option = $this->getCurlOption();
        if ($this->service != '') {
            $url = $this->service . $endpoint . '?token=' . $this->token;
        } else if ($this->url != '') {
            $url = $this->url . $endpoint . '?token=' . $this->token;
        }
        
        $request = new Request('PUT', $url, [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json'
                ], http_build_query($params, null, '&'));
        $config = [
            'config' => [
                'curl' => $option
            ]
        ];

        $response = $this->client->send($request, $config);
        if ($this->isValidHeader($response)) {
            return $this->getResponse($response);
        } else if ($this->enableRetry) {
            return $this->retry($request, $config);
        }
        return false;
    }

    public function delete($endpoint, $params = []) {
        $this->reconnect();

        $option = $this->getCurlOption();
        $request = new Request('DELETE', $this->service . $endpoint . '?token=' . $this->token, [
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Accept' => 'application/json'
                ], http_build_query($params, null, '&'));
        $config = [
            'config' => [
                'curl' => $option
            ]
        ];
        $response = $this->client->send($request, $config);
        if ($this->isValidHeader($response)) {
            return $this->getResponse($response);
        } else if ($this->enableRetry) {
            return $this->retry($request, $config);
        }
        return false;
    }

    private function getCurlOption() {
        $option[CURLOPT_TIMEOUT] = 120;
        $option[CURLOPT_SSL_VERIFYPEER] = false;
        $option[CURLOPT_CONNECTTIMEOUT] = 120;
        if (defined('ENVIRONMENT') && in_array(config('app.env'), array('development', 'testing', 'local'))) {
            $option[CURLOPT_FAILONERROR] = false;
        } else {
            $option[CURLOPT_FAILONERROR] = true;
            if (getHttpProtocol() == 'https') {
                $option[CURLOPT_SSL_VERIFYPEER] = true;
                $option[CURLOPT_SSL_VERIFYHOST] = 2;
                $option[CURLOPT_SSLVERSION] = CURL_SSLVERSION_TLSv1_2;
            }
        }
        return $option;
    }

    private function retry($request, $config) {
        $currentRetry = 1;
        while ($currentRetry < $this->maxRetry) {
            $response = $this->client->send($request, $config);
            if ($this->isValidHeader($response)) {
                return json_decode($response->getBody(), true);
            }
            $currentRetry++;
        }

        return false;
    }

    private function isValidHeader($response) {
        if (in_array($response->getStatusCode(), [200, 401, 400])) {
            return true;
        }

        return false;
    }

    // @codeCoverageIgnoreStart
    public function setBaseUrl($baseUrl) {
        $this->baseUrl = $baseUrl;
    }

    public function initialize($params) {
        if (count($params) > 0) {
            foreach ($params as $key => $value) {
                if (isset($this->$key)) {
                    $this->$key = $value;
                }
            }
        }
    }

    public function getService() {
        return $this->service;
    }

    public function getAuthEndpoint($params) {
        return 'apps/auth?client_id=' . $params['client_id'] . '&secret=' . $params['secret'];
    }

    public function enableRetry($enableRetry) {
        $this->enableRetry = $enableRetry;
    }

    public function setMaxRetry(int $maxRetry) {
        $this->maxRetry = $maxRetry;
    }

    private function getResponse($response) {
        try {
            $resp = new ServiceCommResp(json_decode($response->getBody(), true));
        } catch (Exception $e) {
            //Empty response
            $resp = new ServiceCommResp();
        }
        return $resp;
    }

    // @codeCoverageIgnoreEnd
}


    
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Http\Requests\AddToCartRequest;
use App\Libraries\Bento\BentoApi;
class BentoLineLiff extends Controller
{

	private $bentoApi;
	public function __construct() {
		$this->bentoApi = new BentoApi();
	}

	public function testPageLiff() {
		return view('liff.test');
	}

	public function productItem(Request $request) {

		$request = $request->all();
		\Log::info(['request' => $request]);
		if (!empty($request['channel_id']) && !empty($request['product_id'])) {
			//================== check store setting has channel ===================
			$storeSetting = \App\Models\StoreSetting::where('line_channel_id', $request['channel_id'])->first();
			\Log::info(['storeSetting' => $storeSetting]);
			if ($storeSetting) {
				//=============== check product of store ================
				$storeHasPd = \App\Models\Product::where(['store_id' => $storeSetting->store_id, 'product_id' => $request['product_id']])
				->first();
				\Log::info(['storeHasPd' => $storeHasPd]);
				if ($storeHasPd) {
					$store = $storeSetting->store;
					$pdImage = $this->bentoApi->getProductImage(['productId' => $storeHasPd->product_id]);
					$pdDetails = $this->bentoApi->getProductDetail(['productId' => $storeHasPd->product_id]);

					if (empty($store) || empty($pdDetails)) {
							//===== set flash message data ======//
						return view('page_not_found');
					}
					$params = ['pdImage' => $pdImage, 'pdDetails' => $pdDetails, 'storeData' => $store, 'channelId' => $request['channel_id']];
					return view('liff.product', $params);
				} else {
						//===== set flash message data ======//
					return view('page_not_found');
				}
			} else {
					//===== set flash message data ======//
				return view('page_not_found');
			}			
			
		} else {
				//===== set flash message data ======//
			return view('page_not_found');
		}
	}

	


}

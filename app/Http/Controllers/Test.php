<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Libraries\Bento\BentoApi;
//================ LINE BOT SDK ====================
use \LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use \LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use App\Libraries\Line\FlexSampleRestaurantMenuList;
use App\Libraries\Line\FlexSampleRestaurant;
use App\Libraries\Line\FlexSampleShopping;
use App\Libraries\Line\FlexSampleMessage;
use App\Libraries\Line\FlexReceipt;
//==================================================

// use App\Libraries\Line\LineLogin;
// use App\Libraries\Object\UserData;
class Test extends Controller
{
	// private $line;
	// private $userData;
	protected $user = [];
	private $bentoApi;
	const channelAccessToken = '3oxbEgaEwpFBbXGgIulcTyVc7ri4QqAmkXg+QzBsoIBvLN9cThBUxY+aaoL+a65BgagGbyg3AV0utwbWKdavRJusxwgaeochTHO+whCsy2j5L9poCL6t4zE1mwDuXBg4Oe9cxPk1/aOM+J9uttlVnlGUYhWQfeY8sLGRXgo3xvw=';
	const channelSecret = '9ddd44c36e35de0706bcb3dc3d1ea5a4';
	public function __construct() {
		// $this->line = new LineLogin();
		$this->bentoApi = new BentoApi();
	}

	private function connectLineBot($request = []) {
		// \Log::info(['request' => $request]);
		// $this->body = $request->getContent();
		// $this->header = $request->header();

		// $signature = $request->header('x-line-signature');
		$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(self::channelAccessToken);
		$this->bot = new \LINE\LINEBot($httpClient, ['channelSecret' => self::channelSecret]);
	}

	public function initLineBot() {
		$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(self::channelAccessToken);
		$this->bot = new \LINE\LINEBot($httpClient, ['channelSecret' => self::channelSecret]);

	}


	public function testLine(Request $request) {

		// $this->initLineBot();
		echo 'u s e r';
		$user = $this->getGuest();

		if($user) {
			// $test = $this->sendPushMessage($user->social_ref_id);
			$test = $this->getLineUserData($user->social_ref_id);
			// $sendReceipt = $this->sendFlexReceipt($user->social_ref_id, $itemsList, 'bill no. ' . $bill->bill_no);
		}		
	}

	public function checkFriendshipStatus($uId = '') {
		if(!$uId) {
			return false;
		}

		 // create curl resource 
		$ch = curl_init(); 

        // set url 
		$url = 'https://api.line.me/friendship/v1/status';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 0);
		// curl_setopt($ch, CURLOPT_POSTFIELDS,$vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
			'Authorization: Bearer ' . self::channelAccessToken,
		];

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec($ch);

		curl_close ($ch);
	}

	public function sendPushMessage($uId = '') {
		if(!$uId) {
			return false;
		}

		 // create curl resource 
		$ch = curl_init(); 

        // set url 
		$url = 'https://api.line.me/v2/bot/message/push';

		$data = [
		    "to" => "Uefc48a3f3b195367ccb46e03164edc0b",
		    "messages" => [
		        [
		            "type" => "text",
		            "text" => "Hello, world1"
		        ],
		        [
		            "type" => "text",
		            "text" => "Hello, world2"
		        ]
		    ]
		];
		// $data = array(
		//     'username' => 'tecadmin',
		//     'password' => '012345678'
		// );
		 
		$payload = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
			'Authorization: Bearer ' . self::channelAccessToken,
			'Content-Type: application/json',
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec($ch);

		curl_close ($ch);

	}

	public function getLineUserData($uId = '') {
		if(!$uId) {
			return false;
		}

		 // create curl resource 
		$ch = curl_init(); 

        // set url 
		$url = 'https://api.line.me/v2/bot/profile/' . $uId;

		// $data = [
		//     "to" => "Uefc48a3f3b195367ccb46e03164edc0b",
		//     "messages" => [
		//         [
		//             "type" => "text",
		//             "text" => "Hello, world1"
		//         ],
		//         [
		//             "type" => "text",
		//             "text" => "Hello, world2"
		//         ]
		//     ]
		// ];
		// $data = array(
		//     'username' => 'tecadmin',
		//     'password' => '012345678'
		// );
		 
		// $payload = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$headers = [
			'Authorization: Bearer ' . self::channelAccessToken,
			// 'Content-Type: application/json',
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$server_output = curl_exec($ch);

		$userData = json_decode($server_output);

		curl_close ($ch);

	}

	public function sendFlexReceipt($userId = '', $items = [], $title = 'default title', $callbackUri = '') {
		$flex = new FlexReceipt();
		if(empty($items)) {
			return false;
		}
		$flexSample = $flex->getFlexMessage($items, $title, $callbackUri);
            // $flexSample = $flex->get($this->flexParams);
            // \Log::info(['flexSample' => $flexSample]);

		$response = $this->bot->pushMessage($userId, $flexSample);
		$status = $response->isSucceeded();
		if (!$status) {
			\Log::info('[x] Can\'t send flex message : ' . $response->getHTTPStatus() . ' ' . $response->getRawBody());
		}
	}

	public function test() {
		return view('liff.test');
		//==== country ====
		// $cart = $this->bentoApi->getCartInfo(['cart_id' => 'eyJpdiI6Im9FUVh4TWx3aVV1NlJDbEE3U2lWMWc9PSIsInZhbHVlIjoiTk5OcWJmVm5zXC9Sek1wdjNmSlZMb3c9PSIsIm1hYyI6Ijk3MGE5M2M5Y2RlMTczZjNlZDNmODk5M2I4MDEyMjk5YTljMmYyMzEwZGMzMjA3ZWFiMDIzMDc2YmVlYjk4Y2YifQ==']);

		$cart = $this->bentoApi->calculateCart(['cart_id' => 'eyJpdiI6Im9FUVh4TWx3aVV1NlJDbEE3U2lWMWc9PSIsInZhbHVlIjoiTk5OcWJmVm5zXC9Sek1wdjNmSlZMb3c9PSIsIm1hYyI6Ijk3MGE5M2M5Y2RlMTczZjNlZDNmODk5M2I4MDEyMjk5YTljMmYyMzEwZGMzMjA3ZWFiMDIzMDc2YmVlYjk4Y2YifQ==']);
	}

	public function lineAuthen(Request $request) {
		$this->line->authen($request);
		return redirect()->route('linebot.home');
	}

	public function lineWebhook() {
		return 'true';
		// \Log::info('error', 'line web hook');
	}

	public function index() {
		echo 'index';
		// $api = new BentoApi();
		$guest = $this->bentoApi->getGuest(['guest_id' => 179]);
		// $customer = $api->getCustomerByEmail(['storeId' => 10377, 'email' => 'iceixeize@gmail.com']);
		// if($customer) { // เป็น customer
		// $api->getCustomerByCustomerId(['customerId' => 131153]);
		// } else {
		// 	//create guest
		// }
		// $api->getProductImage(['productId' => 47551]);
		// $api->getProductDetail(['productId' => 47551]);

		// $api->getStorePaymentByStoreId(['storeId' => 10377]);
		// $api->createGuest(['firstname' => 'Ratikarn', 'lastname' => 'Kaewwiwat', 'email' => 'ice@gmail.com', 'phone' => '0841452586', 'store_id' => 10377]);


	}

	public function authLogin() {
		return view('login');
	}

	public function lineLiff(Request $request) {
		$request = $request->all();
		switch ($request['action']) {
			case 'get_product_data':
			if(!empty($request['channel_id']) && !empty($request['product_id'])) {
				$storeSetting = \App\Models\StoreSetting::where('line_channel_id', $request['channel_id'])->first();
				$store = $storeSetting->store;

				// $customer = \App\Models\customer::where('email', )
				if($store) {
					// get store_id เพื่อหา customer
					$storeId = $store->store_id;
					$request['storeData'] = $store;
					return view('liff.product', $request);

				}
			} else {
				return 'Empty params.';
			}
			break;

			case 'add-cart':
			/* 
				1. create cart object
				2. ส่ง object ไป prepare data http://order.kotenarok.com/api/cart/prepare_data
				3. นำ cart object ที่ได้ มาอัพเดท cart object เดิม แล้วส่งไปที่ calculate ตามตัวอย่างด้านล่าง http://order.kotenarok.com/api/cart/calculate
				4. checkout http://order.kotenarok.com/api/cart/checkout
			*/
				if(!empty($request['socialId']) && !empty($request['productId']) && !empty($request['id']) && !empty($request['type'])) {

				} else {
					return 'Empty params.';
				}
				break;

				case 'get-customer-address':
				if(!empty($request['socialId']) && !empty($request['id']) && !empty($request['type'])) {

				} else {
					return 'Empty params.';
				}
				break;

				case 'pay':
				break;

				default:
				return 'DEFAULT';
				break;
			}
			return view('login', ['params' => $request]);
		// $api = new BentoApi();
		// $api->test();
		}

		public function productItem(Request $request) {
			$request = $request->all();
			if($request['action'] == 'get_product_data' && !empty($request['channel_id']) && !empty($request['product_id'])) {

				//================== check store setting has channel ===================
				$storeSetting = \App\Models\StoreSetting::where('line_channel_id', $request['channel_id'])->first();
				if($storeSetting) {
					//=============== check product of store ================
					$storeHasPd = \App\Models\Product::where(['store_id' => $storeSetting->store_id, 'product_id' => $request['product_id']])->first();
					if($storeHasPd) {
						$store = $storeSetting->store;
						$pdImage = $this->bentoApi->getProductImage(['productId' => $storeHasPd->product_id]);
						$pdDetails = $this->bentoApi->getProductDetail(['productId' => $storeHasPd->product_id]);
						$params = ['pdImage' => $pdImage, 'pdDetails' => $pdDetails, 'storeData' => $store, 'channelId' => $request['channel_id']];
						return view('liff.product', $params);
					} else {
						return view('page_not_found');
					}
				} else {
					return view('page_not_found');
				}			
				
			} else {
				return view('page_not_found');
			}
		}

		public function addToCart(AddToCartRequest $request) {
			$request = $request->all();

			$storeHasPd = \App\Models\Product::where(['store_id' => $request['store_id'], 'product_id' => $request['product_id']])->first();

			$pv = $storeHasPd->productVariant()->where('product_variant_id', $request['product_variant_id'])->first();

			$pdCategory = \App\Models\ProductCategory::where(['product_id' => $storeHasPd->product_id, 'store_id' => $request['store_id']])->get();

			$arrCategory = $pdCategory->pluck('category_id')->toArray();

			$pdImage = $this->bentoApi->getProductImage(['productId' => $storeHasPd->product_id]);

			$wholeSalePrice = 0;
			if ($storeHasPd->has_wholesale == true) {
				$wholeSalePrice = $storeHasPd->wholesale;
			}

			if ($storeHasPd && $pv) {
				$cart = [
					[
						'product_variant_id' => $pv->product_variant_id, 
						'product_id' => $storeHasPd->product_id, 						
						'category' => $arrCategory, 
						'qty' => $request['qty'], 
						'price' => $pv->price, 
						'name' => $storeHasPd->name, 
						'pv_name' => $pv->name, 
						'sku' => $pv->sku, 
						'weight' => $storeHasPd->weight,
						'image' => $pdImage[0]['image'], 
						'seo_url' => $storeHasPd->seo_url, 
						'loyalty_points' => $storeHasPd->loyalty_points, 
						'buy_from_category_id' => 19663, 
						'customer_group_id' => 0, 
						'rowid' => self::setRowId($pv->product_variant_id), 
						'has_wholesale' => 0, 
						'subtotal' => $request['qty'] * $pv->price, 
						'wholesale_qty' => 0, 
						'wholesale_price' => 0, 
						'wholesale_subtotal' => 0, 
					],
				];

				$addCart = \Cart::session($request['line_user_id'])->add($request['product_variant_id'], $storeHasPd->name . ' - ' . $pv->name, $pv->price, $request['qty'], $cart);

				$cart = \Cart::getContent();
				$subTotal = \Cart::session($request['line_user_id'])->getSubTotal();
				$total = \Cart::session($request['line_user_id'])->getTotalQuantity();

				$cartAll = [];
				if($cart) {
					
					$weight = 0;
					foreach($cart as $key => $val) {
						$cartAll[self::setRowId($key)] = $val['attributes'][0];
						$weight += $val['attributes'][0]['weight'];

					}

					$cartAll['cart_total'] = $subTotal;
					$cartAll['total_products'] = 1;
					$cartAll['total_items'] = $total;
					$cartAll['weight_total'] = $weight;

				}

				$params = ['store_id' => $request['store_id'], 'langs' => $storeHasPd->store->langs, 'cart' => $cartAll];

				$prepareCart = $this->bentoApi->prepareCart($params);
				
				$cal = $this->bentoApi->calculate(['store_id' => $request['store_id'], 'langs' => 'th', 'cart' => $prepareCart]);
				
				$shipping = $this->bentoApi->getStoreShippingByStoreId(['store_id' => $request['store_id'], 'is_anonymous' => 0, 'weight_total' => $weight]);

				$payment = $this->bentoApi->getStorePaymentByStoreId(['store_id' => $request['store_id'], 'grand_total' => $cal['grand_total']]);
				//================== set session =======================
				session(
					[
						$request['line_user_id'] => [
							'prepareCart' => $prepareCart, 
							'calculate' => $cal, 
							'payment' => $payment, 
							'shipping' => $shipping,
							'socialId' => $request['line_user_id'],
							'storeId' => $request['store_id'],
							'product_id' => $request['product_id'],
							'channel_id' => $request['channel_id'],
						]
					]
				);

				return redirect()->route('bentocart.cart');

			}

		}

		public function cart() {

			$lineUserId = 'Uefc48a3f3b195367ccb46e03164edc0b';

			$items = \Cart::session($lineUserId)->getContent();
			$prepareCart = session($lineUserId)['prepareCart'];
			$cartCal = session($lineUserId)['calculate'];
			$paymentMethod = session($lineUserId)['payment'];
			$shippingMethod = session($lineUserId)['shipping'];
			$socialId = $lineUserId;

			$params = [
				'bodyClass' => "gray-theme", 
				'cartContent' => $items, 
				'prepareCart' => $prepareCart, 
				'cartCal' => $cartCal, 
				'paymentMethod' => $paymentMethod,
				'shippingMethod' => $shippingMethod,
				'socialId' => $socialId,
				'channel_id' => session($lineUserId)['channel_id'],
				'product_id' => session($lineUserId)['product_id'],

			];
			return view('liff.cart', $params);
		}

		public function updateCart(Request $request) {
			$req = $request->all();
			if(isset($req['qty']) && isset($req['rowId']) && isset($req['socialId'])) {
				$update = \Cart::session($req['socialId'])->update($req['productVariantId'], array(
					'quantity' => $req['qty'],
				));

				return ['success' => 1];
			} else {
				return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง'];
			}
		}

		public function deleteCart(Request $request) {
			$req = $request->all();
			if(isset($req['qty']) && isset($req['productVariantId']) && isset($req['lineUserId'])) {
				Cart::session($req['lineUserId'])->update($req['productVariantId'], array(
					'quantity' => $req['qty'],
				));
			}
		}

		public function addAddress() {
			$defaultCountry = 'th';
			$params = [
				'bodyClass' => "gray-theme",
				'defaultCountry' => $defaultCountry,
				'countryDomShortName' => \App\Models\Country::getDomShortName(),
				'customerAddress' => '',
				'countryDom' => \App\Models\Country::getDom(),
			];
			return view('liff.add-new-address', $params);
		}

		public function getAddress(Request $request) {
			$req = $request->all();
			$address = [];
			if($req['socialId']) {
				$checkCustomer = \App\Models\Social::where('social_ref_id', $req['socialId'])->first();

				if($checkCustomer) {
					if($checkCustomer->guest_id) {
						$address = $this->bentoApi->getGuestAddressListByGuestId(['guestId' => $checkCustomer->guest_id]);
					}

					if($checkCustomer->customer_id) {
						$address = $this->bentoApi->getCustomerAddressListByCustomerAddressId(['customerId' => $checkCustomer->customer_id]);
					}
				}
				return view('liff.customer-address', ['address' => $address]);
			}
		}

		public static function setRowId($id) {
			return md5($id);
		}

	}

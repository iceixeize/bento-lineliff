<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;


use App\Http\Requests\AddToCartRequest;
use App\Http\Requests\CustomerAddAddressRequest;
use Redirect;

use App\Libraries\Bento\BentoApi;
use App\Libraries\Bento\BentoGuest;
use App\Libraries\Line\LineApi;

class BentoCart extends Controller
{

	private $bentoApi;
	private $lineApi;
	public function __construct() {
		$this->bentoApi = new BentoApi();
		$this->lineApi = new LineApi();
	}

	public static function getSessionCartId($sessionCartIdName = '') {
		if(empty($sessionCartIdName)) {
			return false;
		} else {
			return Session::get($sessionCartIdName);
		}
	}

	public function addToCart(AddToCartRequest $request) {
		$params = $request->all();
		\Log::error(['paramsAddToCart' => $params]);
		// dd($params);
		//+++ 
		// $request['line_user_id'] = 'Uefc48a3f3b195367ccb46e03164edc0b';

		if(!$request->has('store_id') || !$request->has('product_id') || empty($request->store_id) || empty($request->product_id)) {
			return redirect()->route('pageNotFound');
		}

		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}		

		// c h e c k User
		// $social = \App\Models\Social::where(['social_ref_id' => $request->line_user_id, 'store_id' => $request['store_id']])->first();
		// if(!$social) {
		// 	$newSocial = $this->addNewSocial($request->line_user_id, $request['store_id']);
		// 	if(!$newSocial) {
		// 		return redirect()->route('pageNotFound');
		// 	}
		// }

		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;

		$sessionCartId = Session::get($sessionCartIdName);
		if(empty($sessionCartId)) {
			//===== create cart =====
			$cartId = $this->bentoApi->createCart(['store_id' => $request->store_id]);
			\Log::error('[x] Can\'t create cart api :: store_id : ' . $request->store_id);
			if(!$cartId) {
				// ===== set flash message data ======//
				return redirect()->route('bentoCart.emptyCart');
			}
		} else {
			$cartId = $sessionCartId;
		}

		// ===== add pd item to cart =====
		$addItem = ['cart_id' => $cartId, 'product_variant_id' => $request['product_variant_id'], 'qty' => $request['qty']];

		$items = $this->bentoApi->addPdItemToCart($addItem);
		if(empty($items)) {

			\Log::error('[x] Can\'t add pd item to cart res: ' . json_encode($items));
			$request->session()->forget('store_' . $request->store_id . '_cart_id_' . $request->line_user_id);

			//===== create cart =====
			$cartId = $this->bentoApi->createCart(['store_id' => $request->store_id]);
			\Log::error('[x] Can\'t create cart api :: store_id : ' . $request->store_id);
			if(!$cartId) {
				// ===== set flash message data ======//
				return redirect()->route('bentoCart.emptyCart');
			}


		// ===== add pd item to cart =====
			$addItem = ['cart_id' => $cartId, 'product_variant_id' => $request['product_variant_id'], 'qty' => $request['qty']];

			$items = $this->bentoApi->addPdItemToCart($addItem);
			if(empty($items)) {
			//===== set flash message data ======//
				return redirect()->route('bentoCart.emptyCart');
			}
		}

		Session::put('store_' . $request->store_id . '_cart_id_' . $request->line_user_id, $cartId);
		return redirect()->route('bentocart.cart', [
			'store_id' => $request->store_id,  
			'channel_id' => $request->channel_id,
			'line_user_id' => $request->line_user_id,
		]);
	}

	public function emptyCart(Request $request) {
		return view('liff.empty-cart');
	}

	public function cart(Request $request) {
		$payment = [];
		$guest = [];
		$shipping = [];
		$cartCal = [];
		$customerAddress = [];
		$guestAddressId = 0;
		$cusAddress = [];
		
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}		

		$cartId = self::getSessionCartId('store_' . $request->store_id . '_cart_id_' . $request->line_user_id);


		if(empty($cartId)) {
			return redirect()->route('bentoCart.emptyCart');
		}

		// c h e c k GUEST
		$guest = $this->getGuest();

		// เปลี่ยน เช็ค cusAddress จาก cart
		if(!empty($guest) && $guest->guest_id) {
			$cusAddress = $this->bentoApi->getGuestAddress(['guest_id' => $guest->guest_id]);
			\Log::info('------------------------------------');
			\Log::info(['cusAddress' => $cusAddress]);
			if(!empty($cusAddress) && count($cusAddress) > 0) {
				if($cusAddress[0]['default'] == true) {
					$customerAddress = $cusAddress[0];
				}
			}
		}

		// ถ้า รายละเอียดที่อยู่มีแค่ 1 ให้ set default แล้ว add to cart api ให้เลย
		if(isset($cusAddress) && count($cusAddress) == 1 && empty($customerAddress)) {
			\Log::info('[x] cus a d d r e s s');
			$setDefault = [
				'line_user_id' => $request->line_user_id, 
				'guest_address_id' => $cusAddress[0]['guest_address_id'], 
				'guest_id' => $cusAddress[0]['guest_id'], 
				'store_id' => $request->store_id
			];
			\Log::info('[x] ' . json_encode($setDefault));
			\Log::info('--------------------------------------------------------');
			return redirect()->route('setDefaultAddress', $setDefault);
		}

		if(!empty($customerAddress)) { //============== add guest address to cart ===================
			$addGuestToCart = $this->bentoApi->addGuestAddressToCart(['cart_id' => $cartId, 'guest_address_id' => $customerAddress['guest_address_id']]);
			\Log::info('cart_id :: ' . $cartId);
			$guestAddressId = $customerAddress['guest_address_id'];
		} else {
			// เพิ่ม เช็คว่าถ้า ไม่มี customer address
			if(count($cusAddress) > 0) {
				return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			} else {
				\Log::info('----------- addAddress :: 1 --------------');
				return redirect()->route('addAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			}
		}

		// Calculate Cart
		$cartCal = $this->bentoApi->calculateCart(['cart_id' => $cartId]);
		if(!$cartCal || !isset($cartCal['prepare']['data']['cart']['total_products']) || empty($cartCal['prepare']['data']['cart']['total_products'])) {
			return redirect()->route('bentoCart.emptyCart');
		}
		// Payment
		$payment = $this->bentoApi->getStorePaymentByStoreId(['store_id' => $request->store_id, 'grand_total' => $cartCal['calculate']['data']['grand_total']]);
		// Shipping 
		$shipping = $this->bentoApi->getStoreShippingByStoreId(['store_id' => $request->store_id]);

		$params = [
			'bodyClass' => "gray-theme", 
			'customerAddress' => $customerAddress,
			'cartAll' => $cartCal,
			'paymentMethod' => $payment,
			'shippingMethod' => $shipping,
			'storeId' => $request->store_id,
			'cartId' => $cartId,
			'defaultCountry' => isset($customerAddress['country_short_name']) ? strtolower($customerAddress['country_short_name']) : 'th',
			'guestAddressId' => $guestAddressId,
			'channelId' => $channelId,
			'validBtn' => validBtnForJsRender($cartCal['prepare']['data']),
		];
		return view('liff.cart', $params);
	}

	public function clearCartSession(Request $request) {
		if($request->has('line_user_id') && !empty($request->line_user_id)) {
			$request->session()->remove('store_' . $request->store_id . '_cart_id_' . $request->line_user_id);
		}

	}

	public function prepareCartData($cartId = '', $storeId = '') {
		// Calculate Cart
		$cartCal = $this->bentoApi->calculateCart(['cart_id' => $cartId]);

		// Payment
		$payment = $this->bentoApi->getStorePaymentByStoreId(['store_id' => $storeId, 'grand_total' => $cartCal['calculate']['data']['grand_total']]);

		// Shipping 
		$shipping = $this->bentoApi->getStoreShippingByStoreId(['store_id' => $storeId]);

		$params = [
			'cartCal' => cartItemForJsRender($cartCal['prepare']['data']['cart']),
			'cartAll' => $cartCal,
			'paymentMethod' => paymentMethodDomForJsRender($payment),
			'shippingMethod' => shippingMethodDomForPos($shipping),
			'storeId' => $storeId,
			'cartId' => $cartId,
			'validBtn' => validBtnForJsRender($cartCal['prepare']['data']),
		];
		return $params;

	}

	public function deleteGuestAddress(Request $request) { // guest_address_id, store_id, line_user_id

		if(empty($request->has('guest_address_id')) || !($request->has('guest_address_id'))) {
			flash('เกิดข้อผิดพลาด ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// c h e c k GUEST
		$guest = $this->getGuest();

		$valid = $this->bentoApi->getGuestAddressByGuestAddressId(['guest_address_id' => $request->guest_address_id]);

		if(empty($valid['guest_id']) || empty($guest)) {
			flash('ไม่สามารถลบข้อมูลลูกค้าได้ เนื่องจากไม่พบที่อยู่นี้ในระบบ')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		if($valid['guest_id'] != $guest->guest_id) {
			flash('เกิดข้อผิดพลาด ไม่สามารถลบข้อมูลลูกค้าได้ เนื่องจากไม่พบผู้ใช้นี้ในระบบ')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		$del = $this->bentoApi->deleteGuestAddressByGuestAddressId($request);
		if($del) {
			flash('ลบข้อมูลสำเร็จ')->success();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		} else {
			flash('ไม่สามารถลบข้อมูลลูกค้าได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}
	}

	public function updateCart(Request $request) {
		$parameters = $request->all();

		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if(empty($request->line_user_id)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากไม่พบข้อมูลผู้ใช้', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}
		
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(isset($request->qty) && isset($request->cart_id) && isset($request->item_code) && isset($request->store_id)) {
			$updateCart = $this->bentoApi->updateCart($parameters);
			if($updateCart) {
				return ['success' => 1, 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			} else {
				return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			}
		} else {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง'];
		}
	}

	public function deleteCartItem(Request $request) {
		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}


		if(empty($request->line_user_id)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากไม่พบข้อมูลผู้ใช้', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(isset($request->item_code) && isset($request->cart_id) && isset($request->store_id)) {
			$parameters = $request->all();
			$delCartItem = $this->bentoApi->deleteCartItem($parameters);
			if($delCartItem) {
				return ['success' => 1, 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			} else {
				return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			}
		} else {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง'];

		}
	}

	public function addPaymentToCart(Request $request) {
		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(empty($request->line_user_id)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากไม่พบข้อมูลผู้ใช้', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(isset($request->payment_key) && isset($request->store_id) && isset($request->cart_id)) {
			$parameters = $request->all();
			$updateCart = $this->bentoApi->addPaymentMethodToCart($parameters);
			if($updateCart) {
				return ['success' => 1, 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			} else {
				return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			}
		} else {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง'];
		}
	}

	public function addShippingToCart(Request $request) {
		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(empty($request->line_user_id)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากไม่พบข้อมูลผู้ใช้', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}

		if(isset($request->shipping_key) && isset($request->store_id) && isset($request->cart_id) && isset($request->guest_address_id)) {
			$parameters = [
				'shipping_key' => $request->shipping_key, 
				'store_id' => $request->store_id, 
				'cart_id' => $request->cart_id, 
				'guest_address_id' => $request->guest_address_id
			];
			$updateCart = $this->bentoApi->addShippingMethodToCart($parameters);
			if($updateCart) {
				return ['success' => 1, 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			} else {
				return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ กรุณาเลือกที่อยู่ในการจัดส่งก่อน', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
			}
		} else {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง2'];
		}
	}

	public function addNewAddress(CustomerAddAddressRequest $request) {
		$params = $request->all();
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($storeSetting->line_channel_id)) {
			return redirect()->route('pageNotFound');
		}

		$params['is_store_create'] = 0;
		$params['default'] = 0;

		// c h e c k GUEST
		$guest = $this->getGuest();

		if(!empty($guest) && $guest->guest_id) {

			$guestId = $guest->guest_id;
		} else { // add guest
			$guestId = $this->bentoApi->addGuest($request);
			if(!empty($guest) && $guest->guest_id) {
				$updateSocial = \App\Models\Social::where(['social_ref_id' => $request->line_user_id, 'store_id' => $request->store_id])
				->update(['guest_id' => $guestId]);
				\Log::info('update social response :: ' . $updateSocial . ' line_user_id :: ' . $request->line_user_id . ' store_id :: ' . $request->store_id);
			} else { // update guest id to table social
				$delSocial = \App\Models\Social::where(['social_ref_id' => $request->line_user_id, 'store_id' => $request->store_id])
				->delete();
				\Log::info('delete social response :: ' . $delSocial . ' line_user_id :: ' . $request->line_user_id . ' store_id :: ' . $request->store_id);
				
			// } else {
				// add guest id to table social
				$social = new \App\Models\Social();

				$social->type = 1;
				$social->phone = phoneReplaceZero($request->phone);
				$social->email = $request->email;
				$social->social_ref_id = $request->line_user_id;
				$social->store_id = $request->store_id;
				$social->guest_id = $guestId;
				$social->save();
			}
			// }
		}

		if($guestId) {
			$params['guest_id'] = $guestId;
			$addGuestAddress = $this->bentoApi->addGuestAddress($params);

			if($addGuestAddress) {
				flash('เพิ่มข้อมูลสำเร็จ')->success();
				return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			} else {
				flash('เกิดข้อผิดพลาด ไม่สามารถเพิ่มที่อยู่ได้ กรุณาลองอีกครั้ง')->error();
				\Log::info('----------- addAddress :: 2 --------------');
				return redirect()->route('addAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			}

		} else {
			return redirect()->route('pageNotFound');
		}
	}

	public function submitEditAddress(CustomerAddAddressRequest $request) {
		
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการได้')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// c h e c k GUEST
		$guest = $this->getGuest();
		if(!empty($guest) && $guest->guest_id) {
			$customerAddressList = $this->bentoApi->getGuestAddress(['guest_id' => $guest->guest_id]);
		} else {
			flash('ไม่สามารถทำรายการได้ เนื่องจากไม่พบข้อมูลลูกค้าในระบบ')->error();
			return redirect()->route('editAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		//*** check social line_user_id, guest_id, store_id can edit//
		if(empty($request->guest_id) || empty($request->guest_address_id)) {
			flash('ไม่สามารถทำรายการได้')->error();
			return redirect()->route('editAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		$request['is_store_create'] = 0;
		$request['default'] = 0;
		$request['country_id'] = $request['country'];
		$request['phone'] = phoneReplaceZero($request['full_phone']);
		// check guest ก่อน edit ?
		$edit = $this->bentoApi->updateGuestAddress($request);

		if($edit) {
			flash('แก้ไขข้อมูลสำเร็จ')->success();
			return redirect()->route('bentoCart.editAddress', [
				'store_id' => $request['store_id'], 
				'guest_id' => $request['guest_id'], 
				'guest_address_id' => $request['guest_address_id'], 
				'line_user_id' => $request['line_user_id'], 
			]);
		} else {
			flash('เกิดข้อผิดพลาดในการแก้ไขข้อมูล กรุณาลองอีกครั้ง')->error();
			return redirect()->route('bentoCart.editAddress', ['store_id' => $request->store_id, 'guest_id' => $request->guest_id, 'guest_address_id' => $request->guest_address_id, 'line_user_id' => $request->line_user_id]);
		}	
	}

	public function customerAddress(request $request) {
		$customerAddressList = [];
		
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการได้')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}
		
		// c h e c k GUEST
		$guest = $this->getGuest();

		if(!empty($guest) && $guest->guest_id) {
			$customerAddressList = $this->bentoApi->getGuestAddress(['guest_id' => $guest->guest_id]);
		}

		$params = [
			'bodyClass' => 'gray-theme',
			'storeId' => $request->store_id,
			'customerAddressList' => $customerAddressList,
			'channelId' => $storeSetting->line_channel_id,
			'line_user_id' => $request->line_user_id,
		];

		return view('liff.customer-address', $params);
	}

	public function setDefaultAddress(Request $request) {	
		\Log::info('[x] s e t d e f a u l t');
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		if(empty($request->has('guest_address_id')) || empty($request->has('guest_id')) || !($request->has('guest_address_id')) || !($request->has('guest_id'))) {
			flash('เกิดข้อผิดพลาด ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		} else {
			$guest = $this->getGuest();
			\Log::info('------------------------------------------------------------------');
			\Log::info(['guest' => $guest, 'params' => $request->all()]);
			if($guest->guest_id != $request->guest_id) {
				flash('ไม่สามารถทำรายการได้')->error();
				return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			}
			$address = $this->bentoApi->getGuestAddressByGuestAddressId(['guest_address_id' => $request->guest_address_id]);
			if($address) {
				if($request->guest_id != $address['guest_id']) {
					return false;
				}
				$address['default'] = 1;
				$updateDefault = $this->bentoApi->updateGuestAddress($address);
				if($updateDefault) {

					return redirect()->route('bentocart.cart', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
				}

				return redirect()->route('bentocart.cart', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
			}
		}

	}

	public function pageNotFound() {
		return redirect()->route('bentoCart.emptyCart');
	}

	public function editAddress(Request $request) {
		$defaultCountry = 'th';
		$req = $request->all();
		if(empty($request->has('guest_address_id')) || empty($request->has('guest_id')) || !($request->has('guest_address_id')) || !($request->has('guest_id'))) {
			flash('เกิดข้อผิดพลาด ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// c h e c k GUEST
		$guest = $this->getGuest();

		// get customer address
		$customerAddress = $this->bentoApi->getGuestAddressByGuestAddressId(['guest_address_id' => $req['guest_address_id']]);
		// c h e c k CUSADDRESS
		if(empty($customerAddress)) {
			flash('เกิดข้อผิดพลาด ไม่พบที่อยู่นี้ในระบบ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $req['store_id'], 'line_user_id' => $req['line_user_id']]);
		}

		// c h e c k GUEST can update address by uid
		if($customerAddress['guest_id'] != $guest->guest_id) {
			flash('เกิดข้อผิดพลาด ไม่สามารถแก้ไขที่อยู่นี้ได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $req['store_id'], 'line_user_id' => $req['line_user_id']]);
		}

		$defaultCountry = (!empty($customerAddress['country_short_name'])) ? $customerAddress['country_short_name'] : 'th';

		$textHead = 'เพิ่มที่อยู่';
		$guestAddressId = '';
		$guestId = '';

		$textHead = 'แก้ไขที่อยู่';
		$href = route('bentoCart.submitEditAddress', ['store_id' => $req['store_id'], 'guest_address_id' => $req['guest_address_id'], 'guest_id' => $req['guest_id']]);

		if($request->has('guest_address_id')) {
			$guestAddressId = $req['guest_address_id'];
		}

		if($request->has('guest_id')) {
			$guestId = $req['guest_id'];
		}

		$params = [
			'bodyClass' => "gray-theme",
			'storeId' => $req['store_id'],
			'defaultCountry' => $defaultCountry,
			'countryDomShortName' => \App\Models\Country::getDomShortName(),
			'customerAddress' => $customerAddress,
			'countryDom' => \App\Models\Country::getDom(),
			'href' => $href,
			'textHead' => $textHead,
			'guest_address_id' => $guestAddressId,
			'guest_id' => $guestId,
		];

		return view('liff.add-new-address', $params);
	}

	public function addAddress(Request $request) {
		$defaultCountry = '';

		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $req['store_id'], 'line_user_id' => $req['line_user_id']]);
		}

		$textHead = 'เพิ่มที่อยู่';
		$guestAddressId = '';
		$guestId = '';
		$href = route('addNewAddress', ['store_id' => $request->store_id]);
		$type = $request->input('type', '');

		$params = [
			'bodyClass' => "gray-theme",
			'storeId' => $request->store_id,
			'defaultCountry' => $defaultCountry,
			'countryDomShortName' => \App\Models\Country::getDomShortName(),
			'customerAddress' => '',
			'countryDom' => \App\Models\Country::getDom(),
			'type' => $type,
			'href' => $href,
			'textHead' => $textHead,
			'guest_address_id' => $guestAddressId,
			'guest_id' => $guestId,
		];

		return view('liff.add-new-address', $params);
	}

	public function getAddress(Request $request) {
		$address = [];

		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $req['store_id'], 'line_user_id' => $req['line_user_id']]);
		}

		if($request->has('line_user_id') && !empty($request->line_user_id)) {
			$checkCustomer = \App\Models\Social::where('social_ref_id', $request->line_user_id)->first();

			if($checkCustomer) {
				if($checkCustomer->guest_id) {
					$address = $this->bentoApi->getGuestAddressListByGuestId(['guestId' => $checkCustomer->guest_id]);
				}

				if($checkCustomer->customer_id) {
					$address = $this->bentoApi->getCustomerAddressListByCustomerAddressId(['customerId' => $checkCustomer->customer_id]);
				}
			}
			return view('liff.customer-address', ['address' => $address]);
		}
	}

	public function cartCheckOut(Request $request) {
		if(!$request->has('store_id') || !$request->has('line_user_id') || !$request->has('cart_id')) {
			flash('เกิดข้อผิดพลาด ไม่สามารถทำรายการได้ กรุณาลองอีกครั้ง')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}
		
		// c h e c k STORESETTING
		$storeSetting = $this->getStoreSetting();
		if(empty($storeSetting) || empty($channelId = $storeSetting->line_channel_id)) {
			flash('ไม่สามารถทำรายการผ่านทาง line ได้ เนื่องจากร้านค้ายังไม่ได้ตั้งค่าร้าน')->error();
			return redirect()->route('customerAddress', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			flash('ไม่สามารถทำรายการได้ เนื่องจากข้อมูลตะกร้าสินค้าไม่ถูกต้อง กรุณาลองอีกครั้ง')->error();
			return redirect()->route('cart', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}

		// Calculate Cart
		$cartCal = $this->bentoApi->calculateCart(['cart_id' => $request->cart_id]);
		if(!$cartCal || !isset($cartCal['prepare']['data']['cart']['total_products']) || empty($cartCal['prepare']['data']['cart']['total_products'])) {
			return redirect()->route('bentoCart.emptyCart');
		}

		$pdInCart = $cartCal['prepare']['data']['cart'];

		$checkOut = $this->bentoApi->cartCheckOut(['cart_id' => $request->cart_id]);

		if($checkOut) {
			// remove session cart id
			$request->session()->remove('store_' . $request->store_id . '_cart_id_' . $request->line_user_id);
			$data = $this->bentoApi->getResponseData();
// dd($data);
			// //================ PUSH MESSAGE RECEIPT ================
			$bentoReceipt = cartItemForInvoice($cartCal['prepare']['data']['cart'], $data);

			$datas = [];
			$datas['invoice_no'] = isset($data['full_order_code']) ? $data['full_order_code'] : '-';
			$datas['invoice_url'] = isset($data['invoice_url']) ? $data['invoice_url'] : 'https://www.bentoweb.com';
			$datas['created_at'] = isset($data['date_created']['date']) ? $data['date_created']['date'] : date('Y-m-d H:i');
			$datas['cart_total'] = isset($cartCal['prepare']['data']['cart']['cart_total']) ? number_format($cartCal['prepare']['data']['cart']['cart_total'], 2) : '0.00';
			$datas['calculate'] = isset($cartCal['calculate']['data']) ? $cartCal['calculate']['data'] : [];

			$this->lineApi->sendFlexBentoInvoice($request->line_user_id, $bentoReceipt, $datas['invoice_no'], $datas);

			if($data) {
				return \Redirect::away($data['invoice_url']);
			}
		} else {
			flash($this->bentoApi->getErrorMessage())->error();
			return redirect()->route('cart', ['store_id' => $request->store_id, 'line_user_id' => $request->line_user_id]);
		}
	}

	public function getCartData(Request $request) {
		if($request->has('cartId')) {
		// Calculate Cart
			$cartCal = $this->bentoApi->calculateCart(['cart_id' => $request->cartId]);
			if(empty($cartCal)) {
				return response()->json(array('success'=> 0), 200);
			}

			return response()->json(array('success'=> 1, 'data' => $cartCal), 200);
		} else {
			return response()->json(array('success'=> 0), 200);
		}
	}

	public function deleteCart(Request $request) {
		//??? check user can update cart ?
		$sessionCartIdName = 'store_' . $request->store_id . '_cart_id_' . $request->line_user_id;
		$sessionCartId = Session::get($sessionCartIdName);
		if($request->cart_id != $sessionCartId || empty($sessionCartId)) {
			return ['success' => 0, 'message' => 'ไม่สามารถทำรายการได้ เนื่องจากข้อมูลไม่ถูกต้อง', 'cart' => $this->prepareCartData($request->cart_id, $request->store_id)];
		}


		if(isset($request->qty) && isset($request->productVariantId) && isset($request->lineUserId)) {
			Cart::session($request->lineUserId)->update($request->productVariantId, array(
				'quantity' => $request->qty,
			));
		}
	}

	public function addNewSocial($socialId, $storeId) {
		if(!empty($socialId) && !empty($storeId)) {
			$social = new \App\Models\Social();
			$social->type = 1;
			$social->social_ref_id = $socialId;
			$social->store_id = $storeId;
			return $social->save();

		} else {
			return false;
		}
	}



}

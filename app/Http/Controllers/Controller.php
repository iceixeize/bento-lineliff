<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Contracts\Validation\Validator;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function formatValidationErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    protected function getGuest(){
    	try {
            $guest = app()->make('App\Models\Social');
        } catch (\ReflectionException $e) {
             $guest = null;
        } catch (\Exception $e) {
             $guest = null;
        }

        return $guest;
    }

    protected function getStoreSetting(){
    	try {
            $storeSetting = app()->make('App\Models\StoreSetting');
        } catch (\ReflectionException $e) {
             $storeSetting = null;
        } catch (\Exception $e) {
             $storeSetting = null;
        }

        return $storeSetting;
    }
}

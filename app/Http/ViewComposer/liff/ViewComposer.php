<?php

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;

class ViewComposer {
    public function viewAddNewAddress() {
        return view('liff.add-new-address', [
            'countryDomShortName' => \App\Models\Country::getDomShortName()
        ]);
    }
}

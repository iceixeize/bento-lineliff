<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerAddAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:30',
            'country' => 'required|integer',
            'st_search' => 'required',
            'sub_dist_id' => 'required|integer',
            'subdistrict' => 'required|max:50',
            'dist_id' => 'required|integer',
            'district' => 'required|max:50',
            'prov_id' => 'required|integer',
            'province' => 'required|max:50',
            'zipcode' => 'required|max:9',
            'address_1' => 'required|max:150',
            'address_2' => 'max:150',
            //'default' => 'boolean',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'firstname' => __('frontend.partials.customer_address_form.firstname_phd'),
            'lastname' => __('frontend.partials.customer_address_form.lastname_phd'),
            'email' => __('content.email_txt'),
            'phone' => __('frontend.partials.customer_address_form.phone_lbl'),
            'country' => __('frontend.partials.customer_address_form.country_lbl'),
            'st_search' => __('frontend.partials.customer_address_form.search_address_lbl'),
            'address_1' => __('frontend.partials.customer_address_form.address_1_phd'),
            'address_2' => __('frontend.partials.customer_address_form.address_2_phd'),
        ];
    }
}

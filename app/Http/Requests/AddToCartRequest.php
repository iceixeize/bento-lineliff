<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddToCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd('add to cart');
        return [
            'qty' => 'required|numeric|min:1',
            // 'channel_id' => 'required|min:1',
            // 'product_id' => 'required|min:1',
            // 'product_variant_id' => 'required|min:1',
            // 'line_user_id' => 'required|min:1',
        ];
    }

    public function messages()
    {
        return [
            'qty.required' => 'กรุณาระบุจำนวนที่ต้องการ',
            'qty.numeric' => 'กรุณาระบุจำนวนเป็นตัวเลขเท่านั้น',
            'qty.min' => 'จำนวนสินค้าต้องมากกว่า 1',
            // 'qty.max'  => 'จำนวนสินค้าต้องมากกว่า 1',
        ];
    }
}

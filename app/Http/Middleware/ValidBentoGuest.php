<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Social;
use Illuminate\Http\Request;

class ValidBentoGuest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //dd(json_encode($request->all()).' '. $request->fullUrl(). ' ' .$request->method());
        \Log::info('[x] m i d d l e w a r e');
        \Log::info('[x]' . json_encode($request->all()).' '. $request->fullUrl(). ' ' .$request->method());

        if($request->has('line_user_id') && !empty($request->line_user_id) && $request->has('store_id') && !empty($request->store_id)) {

            // c h e c k STORE
            $store = \App\Models\StoreSetting::where('store_id', $request->store_id)->first();
            app()->singleton('App\Models\StoreSetting', function($app) use ($store) {
                return $store;
            });

            // c h e c k GUEST
            $guest = \App\Models\Social::where(['social_ref_id' => $request->line_user_id, 'store_id' => $request->store_id])->first();
            app()->singleton('App\Models\Social', function($app) use ($guest) {
                return $guest;
            });

            return $next($request);
        } else {

            // return $next($request);
            return redirect()->guest('page-not-found');
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Socialite;

class AuthLineLogin
{
	public function __construct() {
		self::init();
	}

	public function init() {
		$this->redirectUri = config('services.line.redirect');
		$this->channelId = config('services.line.client_id');
		$this->channelSecret = config('services.line.client_secret'); 
	}

	public function lineLoginRedirectUri() {
		return Socialite::with('line')->redirect();
	}

	public function lineUser() {
		$user = Socialite::driver('line')->user();
		return $user;
	}



	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		// dd('handle middleware');
		if(!(session()->has('line_token'))) {
			return $this->lineLoginRedirectUri();
		} else {
			dd('test2');
			return $next($request);
		}

		
	}
}

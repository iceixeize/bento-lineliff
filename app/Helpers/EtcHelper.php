<?php

if (!function_exists('checkThailandIdCard')) {

    function checkThailandIdCard($idCard) {
        if (strlen($idCard) != 13)
            return false;
        $sum = 0;
        for ($i = 0; $i < 12; $i++) {
            $sum += (int) ($idCard{$i}) * (13 - $i);
        }
        if ((11 - ($sum % 11)) % 10 == (int) ($idCard{12})) {
            return true;
        }
        return false;
    }

}

if (!function_exists('removeSymbols')) {

    function removeSymbols($string) {
        return preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
    }

}

if (!function_exists('removeEmoji')) {

    function removeEmoji($text) {

        $clean_text = '';

        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $clean_text = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $clean_text = preg_replace($regexSymbols, '', $clean_text);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $clean_text = preg_replace($regexTransport, '', $clean_text);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $clean_text = preg_replace($regexMisc, '', $clean_text);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $clean_text = preg_replace($regexDingbats, '', $clean_text);

        return $clean_text;
    }

}

if (!function_exists('removeInvisibleCharacters')) {

    /**
     * Remove Invisible Characters
     *
     * This prevents sandwiching null characters
     * between ascii characters, like Java\0script.
     *
     * @param	string
     * @param	bool
     * @return	string
     */
    function removeInvisibleCharacters($str, $urlEncoded = true) {
        $nonDisplayables = [];
        // every control character except newline (dec 10),
        // carriage return (dec 13) and horizontal tab (dec 09)
        if ($urlEncoded) {
            $nonDisplayables[] = '/%0[0-8bcef]/i'; // url encoded 00-08, 11, 12, 14, 15
            $nonDisplayables[] = '/%1[0-9a-f]/i'; // url encoded 16-31
            $nonDisplayables[] = '/%7f/i'; // url encoded 127
        }
        $nonDisplayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S'; // 00-08, 11, 12, 14-31, 127
        do {
            $str = preg_replace($nonDisplayables, '', $str, -1, $count);
        } while ($count);
        return $str;
    }

}

if (!function_exists('random_string')) {

    function random_string($type = 'alnum', $len = 8) {
        switch ($type) {
            case 'basic' : return mt_rand();
            break;
            case 'alnum' :
            case 'numeric' :
            case 'nozero' :
            case 'alpha' :

            switch ($type) {
                case 'alpha' : $pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
                case 'alnum' : $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
                case 'numeric' : $pool = '0123456789';
                break;
                case 'nozero' : $pool = '123456789';
                break;
            }

            $str = '';
            for ($i = 0; $i < $len; $i++) {
                $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
            }
            return $str;
            break;
            case 'unique' :
            case 'md5' :

            return md5(uniqid(mt_rand()));
            break;
        }
    }

}

if (!function_exists('filterDescription')) {

    function filterDescription($text) {
        return strip_tags($text, '<strong><b><em><i><u><strike><sup><sub><p><span><ul><ol><li><blockquote><a><img><table><thead><tbody><tfoot><th><tr><td><hr><hr /><iframe><br><br/><br /><h1><h2><h3><h4><h5><h6><pre><address><div>');
    }

}

// Create UID
if (!function_exists('createGuid')) {

    function createGuid() {
        $microTime = microtime();
        list($a_dec, $a_sec) = explode(' ', $microTime);

        $dec_hex = dechex($a_dec * 1000000);
        $sec_hex = dechex($a_sec);

        ensureLength($dec_hex, 5);
        ensureLength($sec_hex, 6);

        $guid = '';
        $guid .= $dec_hex;
        $guid .= createGuidSection(3);
        $guid .= '-';
        $guid .= createGuidSection(4);
        $guid .= '-';
        $guid .= createGuidSection(4);
        $guid .= '-';
        $guid .= createGuidSection(4);
        $guid .= '-';
        $guid .= $sec_hex;
        $guid .= createGuidSection(6);

        return $guid;
    }

}

if (!function_exists('createGuidSection')) {

    function createGuidSection($characters) {
        $return = '';
        for ($i = 0; $i < $characters; $i++) {
            $return .= dechex(mt_rand(0, 15));
        }
        return $return;
    }

}

if (!function_exists('ensureLength')) {

    function ensureLength(&$string, $length) {
        $strlen = strlen($string);
        if ($strlen < $length) {
            $string = str_pad($string, $length, '0');
        } elseif ($strlen > $length) {
            $string = substr($string, 0, $length);
        }
    }

}

if (!function_exists('stringContains')) {

    function stringContains($str, array $arr) {
        foreach ($arr as $a) {
            if (mb_strpos($str, $a) !== false)
                return true;
        }
        return false;
    }

}

//--------------------------------------------------
// String & Text & Language
//--------------------------------------------------

if (!function_exists('dateThai')) {

    function dateThai($datetime, $format = 'word', $langs = '', $includeTime = false) {
        $langs = ($langs != '' ? $langs : \App::getLocale());

        // day
        $day = date('j', strtotime($datetime));
        $day_zero = date('d', strtotime($datetime));

        // month
        $month = date('n', strtotime($datetime));
        $month_zero = date('m', strtotime($datetime));
        $month_eng = date('F', strtotime($datetime));
        $month_thai = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
        $month_thai_short = array('ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');

        // year
        $year = date('Y', strtotime($datetime));

        // format
        if ($langs == 'th') {
            $year = $year + 543;

            // 1 กรกฎาคม 2555
            if ($format == 'word') {
                $datetimeTxt = $day . ' ' . $month_thai[$month - 1] . ' ' . $year;
            }
            // 01/07/2555
            elseif ($format == 'number') {
                $datetimeTxt = $day_zero . '/' . $month_zero . '/' . $year;
            }
            // 2555-07-01
            elseif ($format == 'db') {
                $datetimeTxt = $year . '-' . $month_zero . '-' . $day_zero;
            }
            // 1 ก.ค. 2556
            elseif ($format == 'short') {
                $datetimeTxt = $day . ' ' . $month_thai_short[$month - 1] . ' ' . $year;
            }
        } elseif ($langs == 'jp') {
            // 2012年7月1日
            if ($format == 'word') {
                $datetimeTxt = $year . '年' . $month . '月' . $day . '日';
            }
            // 01/07/2012
            elseif ($format == 'number') {
                $datetimeTxt = $day_zero . '/' . $month_zero . '/' . $year;
            }
            // 2012-07-01
            elseif ($format == 'db') {
                $datetimeTxt = $year . '-' . $month_zero . '-' . $day_zero;
            }
        } else {
            // July 1, 2012
            if ($format == 'word') {
                $datetimeTxt = $month_eng . ' ' . $day . ', ' . $year;
            }
            // 01/07/2012
            elseif ($format == 'number') {
                $datetimeTxt = $day_zero . '/' . $month_zero . '/' . $year;
            }
            // 2012-07-01
            elseif ($format == 'db') {
                $datetimeTxt = $year . '-' . $month_zero . '-' . $day_zero;
            }
        }

        if ($includeTime) {
            $datetimeTxt .= date(' H:i:s', strtotime($datetime));
        }

        return $datetimeTxt;
    }

}

if (!function_exists('convertIPToLong')) {

    function convertIPToLong($ip) {
        return sprintf('%u', ip2long($ip));
    }

}

if (!function_exists('convertLongToIP')) {

    function convertLongToIP($long) {
        $long = 4294967295 - ($long - 1);
        return long2ip(-$long);
    }

}

if (!function_exists('isLangSupport')) {

    function isLangSupport($lang) {
        return (array_key_exists($lang, config('app.locales')));
    }

}

if (!function_exists('dateDiff')) {

    function dateDiff($startDate, $endDate) {
        $dateTime1 = new DateTime($startDate);
        $dateTime2 = new DateTime($endDate);

        $interval = $dateTime1->diff($dateTime2);

        return $interval->format('%a');
    }

}

if (!function_exists('formatNumShorthand')) {

    function formatNumShorthand($n) {
        $s = array("K", "M", "G", "T");
        $out = "";
        while ($n >= 1000 && count($s) > 0) {
            $n = $n / 1000.0;
            $out = array_shift($s);
        }
        return round($n, max(0, 3 - strlen((int) $n))) . " $out";
    }

}

if (!function_exists('arrayRemoveEmpty')) {

    function arrayRemoveEmpty($arr) {
        return array_values(array_filter($arr, function($value) {
            return $value != '';
        }));
    }

}

if (!function_exists('isProduction')) {

    function isProduction() {
        return (config('app.env') == 'production');
    }

}

if (!function_exists('getShippingTable')) {

    function getShippingTable($model, $shippingAddress = '', $weight_type = 'Gram', $isTable = true) {
        $temp = '';
        if ($isTable) {
            $table = '<table class="col-xs-12 text -gray"><tbody>';
        } else {
            $table = '';
        }

        if (is_string($model->content)) {
            $content = json_decode($model->content, true);
        } else {
            $content = $model->content;
        }

        //dump($content);

        $langs = \App::getLocale();

        if (in_array($model->shipping_type, [1])) {
            if ($isTable) {
                $prefix = '<tr><td>';
                $suffix = '</td></tr>';
            } else {
                $prefix = '<li>';
                $suffix = '</li>';
            }

            $table .= $prefix . 'ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($content['price'], 2) . $suffix;
        } elseif (in_array($model->shipping_type, [6]) && $model->country_id != '') {
            if ($isTable) {
                $prefix = '<tr><td width="60%">';
                $suffix = '</td></tr>';
                $seperate = '</td><td>';
            } else {
                $prefix = '<li>';
                $suffix = '</li>';
                $seperate = '';
            }

            $limitZipcodeNum = 1;
            if ($shippingAddress == '') { //Store Description
                for ($i = 0; $i < count($content['price']); $i++) {
                    // ดึงเอาเขตในแต่ละรหัสไปรษณีย์
                    if (count($content['price'][$i]['group']) > 0) {
                        $shippingZipcodeList = \DB::select('SELECT sd.sd_id, sd.zipcode, sp.sp_id, IF(sp.local_iso = "' . $langs . '", sp.name_local, sp.name_en) AS "province_name", IF(sd.local_iso = "' . $langs . '", sd.name_local, sd.name_en) AS "district"
                            FROM (
                            SELECT country_id
                            FROM country
                            WHERE country_id = "' . $model->country_id . '"
                            ) c
                            JOIN shipping_province sp ON sp.country_id = c.country_id
                            JOIN (
                            SELECT *
                            FROM shipping_district
                            WHERE sd_id IN (' . implode(',', $content['price'][$i]['group']) . ')
                            ) sd ON sd.sp_id = sp.sp_id
                            ORDER BY sd.sp_id ASC');

                        if (count($shippingZipcodeList) > 0) {
                            $temp .= $prefix;
                            $tmpZipcodeGroup = array();
                            foreach ($shippingZipcodeList as $shippingZipcode) {
                                $zipcode = json_decode($shippingZipcode->zipcode, true);
                                if (count($zipcode) > $limitZipcodeNum) {
                                    $zipcode = '(' . $zipcode[0] . ',..., ' . $zipcode[count($zipcode) - 1] . ')';
                                } else {
                                    $zipcode = '(' . implode(",", $zipcode) . ')';
                                }
                                $tmpZipcodeGroup[] = $shippingZipcode->district . ' - ' . $zipcode;
                            }
                            $temp .= implode(' | ', $tmpZipcodeGroup);
                            $temp .= $seperate;
                            $temp .= 'ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($content['price'][$i]['amount'], 2);
                            $temp .= $suffix;
                        }
                    }
                }
                if (isset($content['promotion'])) {
                    $temp .= $prefix;
                    switch ($content['promotion']['type']) {
                        case 1:
                        $temp .= 'ซื้อสินค้ามากกว่า ' . $content['promotion']['range'] . ' ชิ้น';
                        break;
                        case 2:
                        $temp .= 'ซื้อสินค้ามากกว่า (' . $model->getCurrency()->symbol . ') ' . number_format($content['promotion']['range'], 2);
                        break;
                    }
                    $temp .= $seperate;
                    $temp .= 'ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($content['promotion']['amount'], 2);
                    $temp .= $suffix;
                }
            } else { // Cart step 2
                for ($i = 0; $i < count($content['price']); $i++) {
                    // ดึงเอาเขตในแต่ละรหัสไปรษณีย์
                    if (count($content['price'][$i]['group']) > 0) {
                        $shippingZipcodeList = \DB::select('SELECT sd.sd_id, sd.zipcode, sp.sp_id, IF(sp.local_iso = "' . $langs . '", sp.name_local, sp.name_en) AS "province_name", IF(sd.local_iso = "' . $langs . '", sd.name_local, sd.name_en) AS "district"
                           FROM (
                           SELECT country_id
                           FROM country
                           WHERE country_id = "' . $model->country_id . '"
                           ) c
                           JOIN shipping_province sp ON sp.country_id = c.country_id
                           JOIN (
                           SELECT *
                           FROM shipping_district
                           WHERE sd_id IN (' . implode(',', $content['price'][$i]['group']) . ')
                           ) sd ON sd.sp_id = sp.sp_id
                           ORDER BY sd.sp_id ASC');

                        if (count($shippingZipcodeList) > 0) {
                            foreach ($shippingZipcodeList as $shippingZipcode) {
                                $zipcode = json_decode($shippingZipcode->zipcode, true);
                                if (in_array($shippingAddress->zipcode, $zipcode)) {
                                    $temp .= $prefix;
                                    $zipcode = json_decode($shippingZipcode->zipcode, true);
                                    if (count($zipcode) > $limitZipcodeNum) {
                                        //$zipcode = array_slice($zipcode, 0, 3);
                                        //$zipcode = '(' . implode(",", $zipcode) . ',...)';
                                        $zipcode = '(' . $zipcode[0] . ',..., ' . $zipcode[count($zipcode) - 1] . ')';
                                    } else {
                                        $zipcode = '(' . implode(",", $zipcode) . ')';
                                    }
                                    $temp .= $shippingZipcode->district . ' - ' . $zipcode;
                                    $temp .= $seperate;
                                    $temp .= 'ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($content['price'][$i]['amount'], 2);
                                    $temp .= $suffix;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (isset($content['promotion'])) {
                    $temp .= $prefix;
                    switch ($content['promotion']['type']) {
                        case 1:
                        $temp .= 'ซื้อสินค้ามากกว่า ' . $content['promotion']['range'] . ' ชิ้น';
                        break;
                        case 2:
                        $temp .= 'ซื้อสินค้ามากกว่า (' . $model->getCurrency()->symbol . ') ' . number_format($content['promotion']['range'], 2);
                        break;
                    }
                    $temp .= $seperate;
                    $temp .= 'ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($content['promotion']['amount'], 2) . "\n";
                    $temp .= $suffix;
                }
            }
            $table .= $temp;
        } elseif (in_array($model->shipping_type, [2, 3, 4, 5])) {
            $start_arr = explode(':', $content['price']['start']);
            $end_arr = explode(':', $content['price']['end']);

            $range_start = $range_end = $start_arr[0];
            $amount_start = $start_arr[1];
            $amount_end = $end_arr[1];

            unset($content['price']['start']);
            unset($content['price']['end']);

            if ($isTable) {
                $prefix = '<td width="60%">';
                $prefix2 = '<td>';
                $suffix = '</td>';
                $suffix2 = '</td>';
                $openTr = '<tr>';
                $closeTr = '</tr>';
            } else {
                $prefix = '';
                $prefix2 = '';
                $suffix = '';
                $suffix2 = '';
                $openTr = '<li>';
                $closeTr = '</li>';
            }

            // Range Start
            $temp .= $openTr . "\n";
            if ($model->shipping_type == '2') {
                $temp .= $prefix . 'ซื้อสินค้าไม่เกิน (' . $model->getCurrency()->symbol . ') ' . number_format($range_start, 2) . $suffix2 . "\n";
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_start, 2) . $suffix . "\n";
            } elseif ($model->shipping_type == '3') {
                $temp .= $prefix . 'ซื้อสินค้าไม่เกิน ' . number_format($range_start) . ' ชิ้น' . $suffix2 . "\n";
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_start, 2) . $suffix . "\n";
            } elseif ($model->shipping_type == '4') {
                $temp .= $prefix . 'น้ำหนักรวมไม่เกิน (' . $weight_type . ') ' . number_format($range_start) . $suffix2 . "\n";
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_start, 2) . $suffix . "\n";
            } elseif ($model->shipping_type == '5') {
                $temp .= $prefix . 'สินค้า ' . number_format($range_start) . ' ชิ้นแรก' . $suffix2 . "\n";
                $temp .= $prefix2 . ' ค่าจัดส่งชิ้นละ (' . $model->getCurrency()->symbol . ') ' . number_format($amount_start, 2) . $suffix . "\n";
            }
            $temp .= $closeTr . "\n";

            // Range
            for ($r = 0; $r < count($content['price']); $r++) {
                $row = explode(':', $content['price'][$r]);
                $rangePlus = 1;

                if (in_array($model->shipping_type, [2])) {
                    $rangePlus = 0.01;
                }

                if ($r == 0) {
                    $range_left = $range_start + $rangePlus;
                } else {
                    $range_left = $range_right + $rangePlus;
                }

                if (!in_array($model->shipping_type, [2])) {
                    $range_left = (int) $range_left;
                }

                $range_right = $row[0];
                $range_amount = $row[1];

                $range_end = $range_right;

                $temp .= $openTr . "\n";
                if ($model->shipping_type == '2') {
                    $temp .= $prefix . 'ซื้อสินค้าระหว่าง (' . $model->getCurrency()->symbol . ') ' . number_format($range_left, 2) . ' - (' . $model->getCurrency()->symbol . ') ' . number_format($range_right, 2) . $suffix2;
                    $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($range_amount, 2) . $suffix;
                } elseif ($model->shipping_type == '3') {
                    $temp .= $prefix . 'ซื้อสินค้าระหว่าง ' . number_format($range_left) . ' ชิ้น - ' . number_format($range_right) . ' ชิ้น ' . $suffix2;
                    $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($range_amount, 2) . $suffix;
                } elseif ($model->shipping_type == '4') {
                    $temp .= $prefix . 'น้ำหนักรวมระหว่าง (' . $weight_type . ') ' . number_format($range_left) . ' - (' . $weight_type . ') ' . number_format($range_right) . $suffix2;
                    $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($range_amount, 2) . $suffix;
                } elseif ($model->shipping_type == '5') {
                    $temp .= $prefix . 'สินค้าชิ้นที่ ' . number_format($range_left) . '  - ชิ้นที่ ' . number_format($range_right) . $suffix2;
                    $temp .= $prefix2 . ' ค่าจัดส่งชิ้นละ (' . $model->getCurrency()->symbol . ') ' . number_format($range_amount, 2) . $suffix;
                }
                $temp .= $closeTr . "\n";
            }

            // Range End
            $temp .= $openTr . "\n";
            if ($model->shipping_type == '2') {
                $temp .= $prefix . 'ซื้อสินค้ามากกว่า (' . $model->getCurrency()->symbol . ') ' . number_format($range_end, 2) . $suffix2;
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_end, 2) . $suffix;
            } elseif ($model->shipping_type == '3') {
                $temp .= $prefix . 'ซื้อสินค้ามากกว่า ' . number_format($range_end) . ' ชิ้น ' . $suffix2;
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_end, 2) . $suffix;
            } elseif ($model->shipping_type == '4') {
                $temp .= $prefix . 'น้ำหนักรวมมากกว่า (' . $weight_type . ') ' . number_format($range_end) . $suffix2;
                $temp .= $prefix2 . ' ค่าจัดส่ง (' . $model->getCurrency()->symbol . ') ' . number_format($amount_end, 2) . $suffix;
            } elseif ($model->shipping_type == '5') {
                $temp .= $prefix . number_format($range_end) . ' ชิ้นขึ้นไป ' . $suffix2;
                $temp .= $prefix2 . ' ค่าจัดส่งชิ้นละ (' . $model->getCurrency()->symbol . ') ' . number_format($amount_end, 2) . $suffix;
            }
            $temp .= $closeTr . "\n";
            $table .= $temp;
        }

        if ($isTable) {
            $table .= '</tbody></table>';
        } else {
            $table .= '';
        }

        return $table;
    }

}

if (!function_exists('pageToOffset')) {

    function pageToOffset($page, $limit) {
        $offset = ($page - 1) * $limit;
        return $offset;
    }

}

if (!function_exists('filterPhoneNumber')) {

    function filterPhoneNumber($phoneNumber) {
        return str_replace(['-', ' '], '', $phoneNumber);
    }

}

if (!function_exists('strposArray')) {

    function strposArray($haystack, $needles = [], $offset = 0, $encoding = 'UTF-8') {
        $chr = [];

        if (is_array($needles) && count($needles) > 0) {
            foreach ($needles as $needle) {
                $res = mb_strpos($haystack, $needle, $offset, $encoding);
                if ($res !== false)
                    $chr[$needle] = $res;
            }

            if (!empty($chr))
                return min($chr);
        }

        return false;
    }

}

if (!function_exists('inArrayArray')) {

    function inArrayArray($needles = [], $haystacks = []) {
        $chr = [];

        if (is_array($needles) && count($needles) > 0 &&
            is_array($haystacks) && count($haystacks) > 0) {
            foreach ($needles as $needle) {
                if (in_array($needle, $haystacks)) {
                    return true;
                }
            }
        }

        return false;
    }

}

if (!function_exists('getRangeDom')) {

    function getRangeDom($untilYear = 2011) {
        $rangeDom = [
            'month:1' => trans_choice('range_dom.range_month_dom', 1, ['month' => 1]),
            'month:3' => trans_choice('range_dom.range_month_dom', 3, ['month' => 3]),
            'month:6' => trans_choice('range_dom.range_month_dom', 6, ['month' => 6]),
            'month:12' => trans_choice('range_dom.range_month_dom', 12, ['month' => 12]),
        ];

        $currentYear = date('Y');
        for ($y = $currentYear; $y >= $untilYear; $y--) {
            $rangeDom['year:' . $y] = __('range_dom.range_year_dom', ['year' => $y]);
        }
        return $rangeDom;
    }

}

if (!function_exists('getDateFromRange')) {

    function getDateFromRange($rangeString) {
        list($rangeType, $rangeVal) = explode(':', $rangeString);
        switch ($rangeType) {
            case 'month':
            $dateStart = date('Y-m-d', strtotime('-' . ($rangeVal > 1 ? ($rangeVal . ' months') : ($rangeVal . ' month'))));
            $dateEnd = date('Y-m-d');
            break;
            case 'year':
            $dateStart = date('Y-m-d', strtotime($rangeVal . '-01-01'));
            $dateEnd = date('Y-m-d', strtotime($rangeVal . '-12-31'));
            break;
            default:
            throw new \Exception('Invalidd range type');
        }

        return collect([
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
        ]);
    }

}

if (!function_exists('getBrowser')) {

    function getBrowser($userAgent) {
        $ub = 'Unknown';
        if ((preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent)) || preg_match('/Edge/i', $userAgent) || preg_match('/Trident/i', $userAgent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $userAgent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $userAgent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $userAgent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $userAgent)) {
            $bname = 'Opera';
            $ub = "Opera";
        }

        return $ub;
    }

}

/**
 * Generates non-breaking space entities based on number supplied
 *
 * @access	public
 * @param	integer
 * @return	string
 */
if (!function_exists('nbs')) {

    function nbs($num = 1) {
        return str_repeat("&nbsp;", $num);
    }

}

if (!function_exists('extractString')) {

    function extractString($string, $start, $end) {
        $found = array();
        $pos = 0;
        while (true) {
            $pos = strpos($string, $start, $pos);
            if ($pos === false) { // Zero is not exactly equal to false...
                return $found;
            }
            $pos += strlen($start);
            $len = strpos($string, $end, $pos) - $pos;
            $found[] = substr($string, $pos, $len);
        }
    }

}

if (!function_exists('logPositionFormat')) {

    function logPositionFormat($file, $method, $line) {
        return 'File: ' . $file . "\n" . 'Method: ' . $method . "\n" . 'Line: ' . $line . "\n";
    }

}

if (!function_exists('changeDateTimeFormat')) {

    function changeDateTimeFormat($outputFormat, $nowDateTime = '', $time = '') {
        if ($nowDateTime == '') {
            $nowDateTime = date('Y-m-d H:i:s');
        }

        if ($time != '') {
            return date($outputFormat, strtotime($time, strtotime($nowDateTime)));
        } else {
            return date($outputFormat, strtotime($nowDateTime));
        }
    }

}

if (!function_exists('getAddressSeparateList')) {

	function getAddressSeparateList($address, $debug = false) {
        $originalAddress = $address;
        if ($debug) { dump('ที่อยู่เริ่มต้น: ' . $address); }

        $totalLength = mb_strlen($address, 'UTF-8');
        if ($debug) { dump('จำนวนเริ่มต้น: ' . $totalLength); }

        $positionList = [];
        $excludePositionList = [];
        $otherPositionList = [];

        $addressNoList = ['บ้านเลขที่ ', 'บ้านเลขที่', 'เลขที่ ', 'เลขที่'];
        $mooList = ['ม. ', 'ม.', 'หมู่ที่ ', 'หมู่ที่', 'หมู่ ', 'หมู่'];
        $mooBaanList = ['หมู่บ้าน ', 'หมู่บ้าน', 'มบ. ', 'มบ.', ' บ้าน ', ' บ้าน', 'บ้าน ', 'บ้าน', 'ชุมชน ', 'ชุมชน'];
        $buildingList = ['ธนาคาร', 'บริษัท ', 'บริษัท', 'บ. ', 'บ.', 'โรงเรียนบ้าน', 'โรงเรียน ', 'โรงเรียน', 'มหาวิทยาลัย ', 'มหาวิทยาลัย', 'วิทยาลัย ', 'วิทยาลัย', 'คณะ ', 'คณะ', 'สาขา ', 'สาขาถนน', 'สาขา', 'รร. ', 'รร.', 'โรงพยาบาล ', 'โรงพยาบาล', 'รพ. ' , 'รพ.', 'คลินิก ', 'คลินิก', 'วัด ', 'วัด', 'โรงแรม ', 'โรงแรม', 'ศูนย์การค้า ', 'ศูนย์การค้า', 'สำนักงาน ', 'สำนักงาน', 'ศาลากลางจังหวัด ', 'ศาลากลางจังหวัด', 'ศูนย์ราชการจังหวัด ', 'ศูนย์ราชการจังหวัด', 'ที่ว่าการ ', 'ที่ว่าการ', 'การไฟฟ้า', 'การประปา', 'เทศบาล ', 'เทศบาล', 'องค์การ', 'องค์กร', 'ตู้ ปณ.', 'สำนัก', 'บจก. ', 'บจก.', 'บมจ. ', 'บมจ.', 'บจ. ', 'บจ.', 'หจก. ', 'หจก.', 'หสน. ', 'หสน.', 'อบต. ', 'อบต.', 'สนง.', 'สพม.', 'กรม', 'โครงการ ', 'โครงการ', 'สถานี ', 'สถานี', 'อาคาร ', 'อาคาร', 'ตึก ', 'ตึก', 'ร้าน ', 'ร้าน'];
        $floorList = ['ชั้น ', 'ชั้น'];
        $roomList = ['ห้องเลขที่ ', 'ห้องเลขที่', 'เลขที่ห้อง ', 'เลขที่ห้อง', 'ห้อง ', 'ห้อง'];
        $soiList = ['ซ. ', 'ซ.', 'ซอย ', 'ซอย'];
        $roadList =  ['ถ. ', 'ถ.', 'กม. ', 'กม.', 'ถนน ', 'ถนน'];
        $excludeList = ['ตำบล ', 'ตำบล', 'ต. ', 'ต.', 'แขวง ', 'แขวง', 'อำเภอ ', 'อำเภอ', 'อ. ', 'อ.', 'เขต ', 'เขต', 'จังหวัด ', 'จังหวัด', 'จ. ', 'จ.', 'โทร', 'จัดส่ง'];
        $otherList = ['คอนโด', 'Condominium', 'Apartment', 'Apt', 'อพาร์ตเมนท์', 'อพาร์ทเมนท์', 'เพลส', 'Residence', 'Mansion', 'แมนชั่น', 'แฟลต', 'หอพัก', 'หอ', 'วิลล่า', 'Villa', 'เรือนจำ', 'เซเว่น', '7-11', 'ร้าน', 'โรง', 'เภสัช', 'รีสอร์ท', 'Resort', 'เดอะ', 'จำกัด', 'Co.,Ltd.', 'ศูนย์', 'Clinic'];

        $addressNoTxt = '';
        $mooTxt = '';
        $mooBaanTxt = '';
        $buildingTxt = '';
        $floorTxt = '';
        $roomTxt = '';
        $soiTxt = '';
        $roadTxt = '';
        $otherTxt = '';
        $excludeTxtList = [];

        //-------------------------------------------------- 

        // ลบ รหัสไปรษณีย์ ที่ลงท้ายออก 
        if (preg_match('/[0-9]{5}$/u', $address, $pregMatch)) { 
            $address = trim(preg_replace('/[0-9]{5}$/u', '', $address)); 
            $totalLength = mb_strlen($address, 'UTF-8'); 

            if ($debug) { 
                dump('ที่อยู่ (10): ' . $address); 
                dump('จำนวน (10): ' . $totalLength); 
            } 
        } 

        //-------------------------------------------------- 

        // ขึ้นต้นด้วยบ้านเลขที่ 
        if (empty($addressNoTxt)) { 
            if (preg_match('/^(?(?=7\-11)(*FAIL)|(?<name>([\d]+[\d\/\-]*)[\s]*))/u', $address, $pregMatch)) { 
                $addressNoTxt = trim($pregMatch[0]); 
                if ($debug) { dump('addressNoTxt: ' . $addressNoTxt); } 

                $address = trim(preg_replace('/^(?(?=7\-11)(*FAIL)|(?<name>([\d]+[\d\/\-]*)[\s]*))/u', ' ', $address)); 
                $totalLength = mb_strlen($address, 'UTF-8'); 

                if ($debug) { 
                    dump('ที่อยู่ (1): ' . $address); 
                    dump('จำนวน (1): ' . $totalLength); 
                } 
            } 
        } 

        // ขึ้นต้นด้วยบ้านเลขที่จนถึงเว้นวรรคอันแรก 
        if (empty($addressNoTxt)) { 
            $startPos = 0; 
            $endPos = (mb_strpos($address, ' ', 0, 'UTF-8') !== false ? mb_strpos($address, ' ', 0, 'UTF-8') : $totalLength); 
            $addressNo = extractWord($address, $startPos, $endPos); 
            // ใช่เลขอารบิคหรือเลขไทยหรือไม่ 
            if (! preg_match('/[^0-9๑๒๓๔๕๖๗๘๙\/\.]/', $addressNo)) { 
                $addressNoTxt = str_replace(['.'], [''], $addressNo); 
                if ($debug) { dump('addressNoTxt: ' . $addressNoTxt); } 

                $address = trim(mb_substr($address, $endPos)); 
                $totalLength = mb_strlen($address, 'UTF-8'); 

                if ($debug) { 
                    dump('ที่อยู่ (2): ' . $address); 
                    dump('จำนวน (2): ' . $totalLength); 
                } 
            } 
        }

        if (empty($addressNoTxt)) {
        	// ถ้ามีนำหน้าด้วย $addressNoList
        	if ($debug) { 
                dump('preg_match 1 (/(' . listToRegexPattern($addressNoList) . ')+[[:blank:]]?(?<name>[\d]+[\d\/\-]*)[\s]*/u): ', preg_match('/(' . listToRegexPattern($addressNoList) . ')+[[:blank:]]?(?<name>[\d]+[\d\/\-]*)[\s]*/u', $address, $pregMatch1), $pregMatch1);
            }

            if (preg_match('/(' . listToRegexPattern($addressNoList) . ')+[[:blank:]]?(?<name>[\d]+[\d\/\-]*)[\s]*/u', $address, $pregMatch1)) {
            	$addressNoTxt = trim($pregMatch1[0]);

            	if ($debug) { dump('pregMatch1: ' . $addressNoTxt); }

            	$address = trim(str_replace($addressNoTxt, '', $address)); 
                $totalLength = mb_strlen($address, 'UTF-8'); 

                if ($debug) { 
                    dump('ดึง pregMatch1 สำเร็จ'); 
                    dump('ที่อยู่ (3): ' . $address); 
                    dump('จำนวน (3): ' . $totalLength); 
                }
            }
        }

        if (empty($addressNoTxt)) {
            // อยู่ระหว่างที่อยู่ 
            if ($debug) { 
                dump('preg_match 2 (/(' . listToRegexPattern(array_merge($mooList, $floorList, $roomList, $soiList, $roadList, ['แยก', 'โทร', 'เบอร์', 'ตู้ ปณ.'])) . ')(.*)[\d]+.*(*SKIP)(*F)|[[:blank:]]?(?<name>[\d]{5}(*SKIP)(*F)|[\d]{1,}(?:[\d\/\-])+)[[:blank:]]?/u): ',! preg_match('/(' . listToRegexPattern(array_merge($mooList, $floorList, $roomList, $soiList, $roadList, ['แยก', 'โทร', 'เบอร์', 'ตู้ ปณ.'])) . ')(.*)[\d]+.*(*SKIP)(*F)|[[:blank:]]?(?<name>[\d]{5}(*SKIP)(*F)|[\d]{1,}(?:[\d\/\-])+)[[:blank:]]?/u', $address, $pregMatch2), $pregMatch2);
            }

            if (preg_match('/(' . listToRegexPattern(array_merge($mooList, $floorList, $roomList, $soiList, $roadList, ['แยก', 'โทร', 'เบอร์', 'ตู้ ปณ.'])) . ')(.*)[\d]+.*(*SKIP)(*F)|[[:blank:]]?(?<name>[\d]{5}(*SKIP)(*F)|[\d]{1,}(?:[\d\/\-])+)[[:blank:]]?/u', $address, $pregMatch2))  {
            	$addressNoTxt = trim($pregMatch2[0]);

            	if ($debug) { dump('pregMatch2: ' . $addressNoTxt); }

            	$address = trim(str_replace($addressNoTxt, '', $address)); 
                $totalLength = mb_strlen($address, 'UTF-8'); 

                if ($debug) { 
                    dump('ดึง pregMatch2 สำเร็จ'); 
                    dump('ที่อยู่ (4): ' . $address); 
                    dump('จำนวน (4): ' . $totalLength); 
                }
            }
        }

        //-------------------------------------------------- 

        // ลบคำที่ไม่ถูกต้องออก 
        foreach ($excludeList as $exclude) { 
            if (mb_strpos($address, ' ' . $exclude, 0, 'UTF-8') !== false) { 
                $excludePositionList[] = [ 
                    'position' => mb_strpos($address, ' ' . $exclude, 0, 'UTF-8'), 
                    'prefix' => $exclude, 
                    'type' => 'exclude', 
                ]; 
            } 
        } 

        // เรียงลำดับของคำที่ต้องค้นหา 
        $excludePositionList = collect($excludePositionList);
        $sortExcludePositionList = $excludePositionList->sortBy('position');
        $newExcludePositionList = $sortExcludePositionList->values()->all();

        if ($debug) { dump('newExcludePositionList:', $newExcludePositionList); } 

        if (count($newExcludePositionList) > 0) { 
            for ($i=0; $i<count($newExcludePositionList); $i++) { 
                if (isset($newExcludePositionList[$i + 1])) { 
                    $loopStartPos = $newExcludePositionList[$i]['position']; 
                    $loopEndPos = $newExcludePositionList[$i + 1]['position']; 
                } else { 
                    $loopStartPos = $newExcludePositionList[$i]['position']; 
                    if (mb_strpos($address, ' ', ($loopStartPos + 1), 'UTF-8') !== false) { 
                        $loopEndPos = mb_strpos($address, ' ', ($loopStartPos + 1), 'UTF-8'); 
                    } else { 
                        $loopEndPos = $totalLength; 
                    } 
                } 

                $excludeTxtList[] = extractWord($address, $loopStartPos, $loopEndPos, 'UTF-8'); 
            } 

            if ($debug) { dump('excludeTxtList:', $excludeTxtList); } 

            if (count($excludeTxtList) > 0) { 
                $address = trim(str_replace($excludeTxtList, [''], $address)); 
                $totalLength = mb_strlen($address, 'UTF-8'); 
            } 
        } 

        if ($debug) { 
            dump('ที่อยู่ (ลบคำที่ไม่ถูกต้องออก): ' . $address); 
            dump('จำนวน (ลบคำที่ไม่ถูกต้องออก): ' . $totalLength); 
        } 

        //-------------------------------------------------- 

        // ค้นหาตำแหน่ง 
        $allList = [ 
            'addressNo' => $addressNoList, 
            'moo' => $mooList, 
            'mooBaan' => $mooBaanList, 
            'building' => $buildingList, 
            'floor' => $floorList, 
            'room' => $roomList, 
            'soi' => $soiList, 
            'road' => $roadList, 
        ]; 

        foreach ($allList as $key => $all) {
            foreach ($all as $word) {
                if (mb_strpos($address, $word, 0, 'UTF-8') !== false) {
                    $positionList[] = [
                        'position' => mb_strpos($address, $word, 0, 'UTF-8'),
                        'prefix' => $word,
                        'type' => $key,
                    ];

                    //if (! in_array($key, ['moo', 'building', 'room', 'road'])) { break; }
                }
            }
        }

        if ($debug) { dump('positionList (ค้นหาตำแหน่ง):', $positionList); } 

        // กรองคำตำแหน่งเดียวกันออก 
        $positionList = collect($positionList); 
        $sortPositionList = $positionList->sortByDesc('prefix');
        if ($debug) { dump('sortPositionList:', $sortPositionList); } 
        $sortPositionList = $sortPositionList->unique('position'); 

        if ($debug) { dump('newPositionList (กรองคำตำแหน่งเดียวกันออก):', $sortPositionList); }


        // เรียงลำดับของคำที่ต้องค้นหา 
        $sortPositionList = $sortPositionList->sortBy('position'); 
        $newPositionList = $sortPositionList->values()->all(); 

        if ($debug) { dump('newPositionList (เรียงลำดับของคำที่ต้องค้นหา):', $newPositionList); } 

        // กรองคำย่อที่ใกล้เคียงกันออก 
        if (count($newPositionList) > 0) { 
            $loopPass = true; 
            while ($loopPass) { 
                for ($i=0; $i<count($newPositionList); $i++) { 
                    if (isset($newPositionList[$i + 1])) { 
                        $loopStartPos = $newPositionList[$i]['position']; 
                        $loopStartLength = mb_strlen($newPositionList[$i]['prefix'], 'UTF-8'); 
                        $loopEndPos = $newPositionList[$i + 1]['position']; 

                        if (($loopStartPos + $loopStartLength) >= $loopEndPos) { 
                            unset($newPositionList[$i + 1]); 
                            $newPositionList = array_values($newPositionList); 
                            break; 
                        } 
                    } else { 
                        $loopPass = false; 
                    } 
                } 
            } 
        } 

        if ($debug) { dump('newPositionList (กรองคำย่อที่ใกล้เคียงกันออก):', $newPositionList); } 

        // ดึงคำ 
        $loopStartPos = 0; 
        $loopEndPos = 0; 

        if (count($newPositionList) > 0) { 
            for ($i=0; $i<count($newPositionList); $i++) { 
                if ($i == 0 && $newPositionList[$i]['position'] > 0) { 
                    $otherTxt .= haveTextAddSymbol($otherTxt) . extractWord($address, 0, $newPositionList[$i]['position'], 'UTF-8'); 
                } 

                if (isset($newPositionList[$i + 1])) { 
                    $loopStartPos = $newPositionList[$i]['position']; 
                    $loopEndPos = $newPositionList[$i + 1]['position']; 
                } else { 
                    $loopStartPos = $newPositionList[$i]['position']; 
                    $loopEndPos = $totalLength; 
                } 

                ${$newPositionList[$i]['type'] . 'Txt'} .= haveTextAddSymbol(${$newPositionList[$i]['type'] . 'Txt'}) . extractWord($address, $loopStartPos, $loopEndPos, 'UTF-8'); 
            } 
        } else { 
            if (! empty($address)) { 
                $otherTxt = haveTextAddSymbol($otherTxt) . extractWord($address, 0, $totalLength, 'UTF-8'); 
            } 
        } 

        if ($debug) { 
            dump('ดึงคำ:', $newPositionList, [ 
                'addressNoTxt' => $addressNoTxt, 
                'mooTxt' => $mooTxt, 
                'mooBaanTxt' =>  $mooBaanTxt, 
                'buildingTxt' => $buildingTxt, 
                'floorTxt' => $floorTxt, 
                'roomTxt' => $roomTxt, 
                'soiTxt' => $soiTxt, 
                'roadTxt' => $roadTxt, 
                'otherTxt' => $otherTxt, 
            ]); 
        } 

        //-------------------------------------------------- 

        // กรองข้างหลังหมู่ออก 
        if (! empty($mooTxt)) { 
            $extractMooTxt = explode(' ', $mooTxt); 

            if (in_array($extractMooTxt[0], ['ม.', 'หมู่ที่', 'หมู่'])) { 
                $moo1 = array_shift($extractMooTxt); 
                $moo2 = array_shift($extractMooTxt); 

                if (! preg_match('/[^0-9๑๒๓๔๕๖๗๘๙]/', $moo2)) { 
                    if (in_array($moo1, ['หมู่ที่', 'หมู่'])) { 
                        $mooTxt = implode(' ', [$moo1, $moo2]); 
                    } else { 
                        $mooTxt = implode('', [$moo1, $moo2]); 
                    } 
                } else { 
                    $buildingTxt = implode('', [$moo1, $moo2]); 
                    $mooTxt = ''; 
                } 

                if (count($extractMooTxt) > 0) { 
                    $otherTxt .= haveTextAddSymbol($otherTxt) . implode(' ', $extractMooTxt); 
                } 
            } else { 
                $moo1 = array_shift($extractMooTxt); 

                // ตรวจสอบ ม. ว่าเป็น มหาลัย หรือเปล่า? 
                $extractMooTxt2 = explode('.', $moo1); 
                if ($extractMooTxt2[0] == 'ม' && ! is_numeric($extractMooTxt2[1])) { 
                    array_unshift($extractMooTxt, $moo1); 
                    $buildingTxt .= haveTextAddSymbol($buildingTxt) . implode(' ', $extractMooTxt); 
                    $mooTxt = ''; 
                } else { 
                    $mooTxt = $moo1; 
                    $otherTxt .= haveTextAddSymbol($otherTxt) . implode(' ', $extractMooTxt); 
                } 
            } 
        } 

        if ($debug) { 
            dump('กรองข้างหลังหมู่ออก:', [ 
                'addressNoTxt' => $addressNoTxt, 
                'mooTxt' => $mooTxt, 
                'mooBaanTxt' =>  $mooBaanTxt, 
                'buildingTxt' => $buildingTxt, 
                'floorTxt' => $floorTxt, 
                'roomTxt' => $roomTxt, 
                'soiTxt' => $soiTxt, 
                'roadTxt' => $roadTxt, 
                'otherTxt' => $otherTxt, 
            ]); 
        } 

        //-------------------------------------------------- 

        // จัดการ otherTxt 
        // ค้นหาตำแหน่ง 
        $allList = [ 
            'other' => $otherList 
        ]; 

        foreach ($allList as $key => $all) { 
            foreach ($all as $word) { 
                if (mb_strpos($address, $word, 0, 'UTF-8') !== false) { 
                    $otherPositionList[] = [ 
                        'position' => mb_strpos($address, $word, 0, 'UTF-8'), 
                        'prefix' => $word, 
                        'type' => 'building', 
                    ]; 
                    break; 
                } 
            } 
        } 

        if ($debug) { dump('otherPositionList (จัดการ otherTxt):', $otherPositionList); } 

        if (count($otherPositionList) > 0) { 
            $buildingTxt .= haveTextAddSymbol($buildingTxt) . $otherTxt; 
            $otherTxt = ''; 
        } else { 
            // ถ้าบ้านเลขที่ไม่มี และ otherTxt เป็นเลขทั้งหมด 
            if (! preg_match('/[^0-9๑๒๓๔๕๖๗๘๙]/', $otherTxt) && empty($addressNoTxt)) { 
                $addressNoTxt .= $otherTxt; 
                $otherTxt = ''; 
            } 
        } 

        if ($debug) { 
            dump('ดึงคำ (otherTxt):', $otherPositionList, [ 
                'addressNoTxt' => $addressNoTxt, 
                'mooTxt' => $mooTxt, 
                'mooBaanTxt' =>  $mooBaanTxt, 
                'buildingTxt' => $buildingTxt, 
                'floorTxt' => $floorTxt, 
                'roomTxt' => $roomTxt, 
                'soiTxt' => $soiTxt, 
                'roadTxt' => $roadTxt, 
                'otherTxt' => $otherTxt, 
            ]); 
        } 

        //-------------------------------------------------- 

        // กรองคำที่ไม่ต้องการออก
        //$addressNoTxt = trim(str_replace($addressNoList, '', $addressNoTxt));
        //$mooTxt = trim(str_replace($mooList, '', $mooTxt));

        return [ 
            'addressNoTxt' => removeOverWhiteSpace($addressNoTxt), 
            'mooTxt' => removeOverWhiteSpace($mooTxt), 
            'mooBaanTxt' =>  removeOverWhiteSpace($mooBaanTxt), 
            'buildingTxt' => removeOverWhiteSpace($buildingTxt), 
            'floorTxt' => removeOverWhiteSpace($floorTxt), 
            'roomTxt' => removeOverWhiteSpace($roomTxt), 
            'soiTxt' => removeOverWhiteSpace($soiTxt), 
            'roadTxt' => removeOverWhiteSpace($roadTxt), 
            'otherTxt' => removeOverWhiteSpace($otherTxt), 
            'excludeTxtList' => $excludeTxtList, 
        ]; 
    }

}

if (!function_exists('extractWord')) {

	function extractWord($text, $startPos, $endPos) {
        $length = ($endPos - $startPos);

        return trim(mb_substr($text, $startPos, $length));
    }

}

if (!function_exists('haveTextAddSymbol')) {

    function haveTextAddSymbol($text, $symbol = ' ') {
        return (empty($text) ? '' : $symbol);
    }

}

if (!function_exists('removeOverWhiteSpace')) {

	function removeOverWhiteSpace($text) {
        return preg_replace('/[[:space:]]+/', ' ', trim($text));
    }

}

if (!function_exists('listToRegexPattern')) {

    function listToRegexPattern($list) {
        $listTxt = (implode('|', $list));
        $listTxt = str_replace(['.'], ['\.'], $listTxt);

        return $listTxt;
    }

}

if (!function_exists('getLang')) {

    function getLang() {
    	return app()->getLocale();
    }

}

if (!function_exists('getISO639_1Lang')) {

    function getISO639_1Lang() {
    	$lang = getLang();
    	switch(getLang()) {
    		case 'cn':
         return 'zh';
         case 'jp':
         return 'ja';
         default:
         return $lang;
     }
 }

}

if (!function_exists('validCartBeforeSubmit')) {

    function validCartBeforeSubmit($cartPrepareData) {
        // dd($cartPrepareData);
        if(empty($cartPrepareData)) {
            return ['status' => 0, 'message' => 'ข้อมูลไม่ถูกต้อง กรุณาลองอีกครั้ง'];
        }

        $message = 'กรุณาเลือก';
        if(!isset($cartPrepareData['shipping_guest_address']) || empty($cartPrepareData['shipping_guest_address'])) {
            $message .= 'ที่อยู่ ก่อนทำรายการ';
            return ['status' => 0, 'message' => $message];
        }

        if(!isset($cartPrepareData['shipping_method']) || empty($cartPrepareData['shipping_method'])) {
            $message .= 'วิธีการจัดส่ง ก่อนทำรายการ';
            return ['status' => 0, 'message' => $message];

        }

        if(!isset($cartPrepareData['payment_method']) || empty($cartPrepareData['payment_method'])) {
            $message .= 'วิธีการรับชำระเงิน ก่อนทำรายการ';
            return ['status' => 0, 'message' => $message];

        }

        if(!isset($cartPrepareData['cart']['total_items']) || empty($cartPrepareData['cart']['total_items'])) {
            $message .= 'สินค้าที่ต้องการ ก่อนทำรายการ';
            return ['status' => 0, 'message' => $message];

        }

        return ['status' => 1];
    }

    if (!function_exists('phoneReplaceZero')) {
        function phoneReplaceZero($phoneNumber = '') {
            if(!empty($phoneNumber)) {
                return str_replace("+66", "0", $phoneNumber);
            } else {
                return null;
            }
        }
    }

    if (!function_exists('productItemForJsRender')) {
        function productItemForJsRender($productItem) {
            $productItemTmp = [];
            if(isset($productItem))
                foreach ($productItem as $index => $item) {
                    if(is_array($item) && strlen($index) == 32) {
                        $productItemTmp[] = [
                            'product_variant_id' => $item['product_variant_id'],
                            'name' => $item['name'],
                            'price' => number_format($item['price'], 2),
                        ];

                    }           
                }
                return $productItemTmp;
            }
        }

    }


    if (!function_exists('moneyFormat')) {

    function moneyFormat($number) {
        if(is_numeric($number)) {
            return number_format($number, 2);
        }

        return 0;
    }

}

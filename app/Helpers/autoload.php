<?php

$dir = realpath('./../app/Helpers');
$files = glob("$dir/*Helper.php");
foreach ($files as $file) {
	$filename = (string) $file;

	if (strpos($filename, 'Helper.php') !== false) {
		require_once $filename;
	}
}
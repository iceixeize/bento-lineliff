<?php

if(!function_exists('getHttpProtocol')) {
	function getHttpProtocol() {
		$http = 'http';

		if (isset($_SERVER['HTTP_X_FORWARDED_PROTO'])) {
			if ($_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
				$http = 'https';
			} else {
				$http = 'http';
			}
		}

		/*apache + variants specific way of checking for https*/
		if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] == 1)) {
			$http = 'https';
		}

		/*nginx way of checking for https*/
		if (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] === '443')) {
			$http = 'https';
		}

		return $http;
	}
}


if (!function_exists('isSsl')) {

	function isSsl() {
		return (getHttpProtocol() == 'https');
	}

}

if (!function_exists('paymentMethodDomForPos')) {
	function paymentMethodDomForPos($paymentMethodList, $shipping = null) {
		$paymentMethodDom = [];

      //dump($shipping, (($shipping instanceof \App\Models\StoreShipping) || ($shipping instanceof \App\Models\StoreShippingThirdParty)));



		if (isset($paymentMethodList['has_filtered']) && $paymentMethodList['has_filtered'] === true) {

		} else {
			$paymentMethodDom[] = [
				'id' => 'cash',
				'name' => 'ชำระด้วยเงินสด',
				// 'logo' => static::getPaymentLogo('cash', '3x1', 'svg'),
				'checked' => 0
			];
		}

		if (isset($paymentMethodList['payment_key'], $paymentMethodList['payment_value'], $paymentMethodList['payment_image'])) {
			$payment_key = $paymentMethodList['payment_key'];
			$payment_value = $paymentMethodList['payment_value'];
			$payment_image = $paymentMethodList['payment_image'];

			foreach ($payment_key as $paymentIndex => $paymentValue) {
				$paymentMethodDom[] = [
					'id' => $paymentValue,
					'name' => $payment_value[$paymentIndex],
					// 'logo' => static::getPaymentLogo($paymentValue, '3x1', 'svg'),
					'checked' => 0
				];
			}
		}

        //dd($paymentMethodDom);

		return $paymentMethodDom;
	}
}

if (!function_exists('paymentMethodDomForJsRender')) {
	function paymentMethodDomForJsRender($paymentMethodList) {
		$paymentMethodDom = [];
      //dump($paymentMethodList);

		if (isset($paymentMethodList['payment_key'], $paymentMethodList['payment_value'], $paymentMethodList['payment_image'])) {
			$payment_key = $paymentMethodList['payment_key'];
			$payment_value = $paymentMethodList['payment_value'];
			$payment_image = $paymentMethodList['payment_image'];

			foreach ($payment_key as $paymentIndex => $paymentValue) {
				$paymentMethodDom[] = [
					'id' => $paymentValue,
					'name' => $payment_value[$paymentIndex],
					// 'logo' => static::getPaymentLogo($paymentValue, '3x1', 'svg'),
				];
			}
		}

		return $paymentMethodDom;
	}
}

if (!function_exists('cartItemForJsRender')) {
	function cartItemForJsRender($cartCal) {
		$cartItemTmp = [];
		if(isset($cartCal))
		foreach ($cartCal as $index => $item) {
			if(is_array($item) && strlen($index) == 32) {
				$cartItemTmp[] = [
					'item_code' => $item['item_code'],
					'name' => $item['name'],
					'image' => $item['image'],
					'pv_name' => $item['pv_name'],
					'sku' => $item['sku'],
					'has_wholesale' => $item['has_wholesale'],
					'qty' => $item['qty'],
					'subtotal' => number_format($item['subtotal'], 2),
					'rowid' => $item['rowid'],
					'product_variant_id' => $item['product_variant_id'],
				];

			}			
		}
		return $cartItemTmp;
	}
}

if (!function_exists('validBtnForJsRender')) {
	function validBtnForJsRender($cartAll) {
		$status = false;
		if(isset($cartAll['shipping_guest_address']) && !empty($cartAll['shipping_guest_address']) &&
			isset($cartAll['shipping_method']) && !empty($cartAll['shipping_method']) &&
			isset($cartAll['payment_method']) && !empty($cartAll['payment_method']) &&
			isset($cartAll['cart']['total_products']) && !empty($cartAll['cart']['total_products']) 

		) {
			$status = true;
		}


		return $status;
	}
}




if (!function_exists('shippingMethodDomForPos')) { 
	function shippingMethodDomForPos($shippingMethodList) {
		$shippingMethodDom = [];

		if (count($shippingMethodList) > 0) {
			foreach ($shippingMethodList as $shippingMethod) {
				$shippingName = '';
				if (isset($shippingMethod['ss_id'])) {
					$shippingMethodName = $shippingMethod['default_name_field'];
				} else if (isset($shippingMethod['sstp_id'])) {
					$shippingMethodName = $shippingMethod['default_name_field'];
				} else {
					$shippingMethodName = $shippingMethod['default_name_field'];
				}

				$shippingMethodDom[] = [
					'id' => $shippingMethod['shipping_key'],
					'name' => $shippingMethodName,
					'title' => strip_tags($shippingMethodName),
					'shipping_provider_id' => (isset($shippingMethod['shipping_provider_id']) ? $shippingMethod['shipping_provider_id'] : 0),
					'checked' => 0
				];
			}
		}

		return $shippingMethodDom;
	}
}

if (!function_exists('cartItemForInvoice')) {
	function cartItemForInvoice($cartCal, $data = []) {
		\Log::info(['cartCal' => $cartCal]);
		$cartItemTmp = [];
		if(isset($cartCal))
		foreach ($cartCal as $index => $item) {
			if(is_array($item) && strlen($index) == 32) {
				$cartItemTmp[] = [
					'name' => $item['name'],
					'image' => $item['image'],
					'pv_name' => $item['pv_name'],
					'sku' => !empty($item['sku']) ? $item['sku'] : '-',
					'qty' => $item['qty'],
					'subtotal' => number_format($item['subtotal'], 2),
					'price' => number_format($item['price'], 2),
					
				];

			}			
		}
		return $cartItemTmp;
	}
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'customer';
	protected $primaryKey = 'customer_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();
	}


}

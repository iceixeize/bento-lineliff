<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Social extends Model
{
	use SoftDeletes;
	
	protected $table = 'social';
	protected $primaryKey = 'social_id';

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected static function boot() 
	{
		parent::boot();
	}



	public function store() {
		return $this->belongsTo('App\Models\Store', 'store_id');
	}


}

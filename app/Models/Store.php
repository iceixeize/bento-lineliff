<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'store';
	protected $primaryKey = 'store_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();
	}



	public function storeSetting() {
		return $this->hasOne('App\Models\StoreSetting', 'store_id');
	}

	public function social() {
		return $this->hasMany('App\Models\Social', 'store_id');
	}

	public function product() {
		return $this->hasMany('App\Models\Product', 'store_id');
	}


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'category';
	protected $primaryKey = 'category_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() {
		parent::boot();
	}

	public function productCategory() {
		return $this->hasOne('App\Models\ProductCategory', 'category_id');
	}


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'product_variant';
	protected $primaryKey = 'product_variant_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();

		static::addGlobalScope(new App\Scopes\DeleteScope);
	}

	public function product() {
		return $this->belongsTo('App\Models\Product', 'product_id');
	}

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'product';
	protected $primaryKey = 'product_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();
	}



	public function store() {
		return $this->belongsTo('App\Models\Store', 'store_id');
	}

	public function productVariant() {
		return $this->hasMany('App\Models\ProductVariant', 'product_id');
	}

	public function productCategory() {
		return $this->hasMany('App\Models\productCategory', 'product_id');
	}
}

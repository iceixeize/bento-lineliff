<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreSetting extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'store_setting';
	protected $primaryKey = 'store_setting_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();
	}



	public function store() {
		return $this->belongsTo('App\Models\Store', 'store_id');
	}


}

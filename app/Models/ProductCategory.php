<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
	const CREATED_AT = 'date_created';
	const UPDATED_AT = 'date_modified';
	protected $table = 'product_category';
	protected $primaryKey = 'product_id';

	protected $dates = ['date_created', 'date_modified'];

	protected static function boot() 
	{
		parent::boot();
	}

	public function product() {
		return $this->belongsTo('App\Models\Product', 'product_id');
	}

	public function category() {
		return $this->belongsTo('App\Models\Category', 'category_id');
	}

	public function store() {
		return $this->belongsTo('App\Models\Store', 'store_id');
	}



}

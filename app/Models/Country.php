<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Country extends Model {

    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_modified';
    const THAILAND_ID = 5;

    protected $table = 'country';
    protected $primaryKey = 'country_id';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['date_created', 'date_modified'];

    protected $casts = [
        'country_id' => 'integer',
        'weight_limit' => 'integer',
        'zone_id' => 'integer',
        'active' => 'boolean',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot() {
        parent::boot();
    }


    public static function getAll() {
            return Country::orderBy('country_id', 'asc')->get();

        return $countryList;
    }

    
    /*
    |-------------------------------------------------
    | Function
    |-------------------------------------------------
    */

    /*
    |-------------------------------------------------
    | Get
    |-------------------------------------------------
    */

    public function getFlagUrlAttribute() {
    	return asset($this->attributes['flag']);
    }

    /*
    |--------------------------------------------------
    | Static
    |-------------------------------------------------
    */

    public static function isThailand($countryId) {
        return ($countryId == static::THAILAND_ID);
    }

    public static function getDom() {
    	$countryList = static::getAll();

    	return $countryList->pluck('country_name', 'country_id');
    }

    public static function getShortNameDom() {
    	$countryList = static::getAll();

    	return $countryList->pluck('country_short_name', 'country_id');
    }

    public static function getDomShortName() {
    	$countryList = static::getAll();

    	return $countryList->pluck('country_short_name', 'country_id');
    }
    
    public static function getBlankSelect($query, $customText = '') {
        if ($customText == '') {
            $customText = trans('content.country_placeholder');
        }
        
        $country = new static();
        $country->country_id = '';
        $country->country_name = $customText;
        $country->country_short_name = '';
        $country->flag = '';
        $country->weight_limit = '';
        $country->zone_id = '';
        $country->active = 1;
        
        return $country;
    }

    /*
	|--------------------------------------------------
	| Scope
	|--------------------------------------------------
	*/
    
}
